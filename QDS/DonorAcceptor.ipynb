{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ee87a130",
   "metadata": {},
   "source": [
    "Quantum dynamics of dissipative systems, WS 23/24\n",
    "\n",
    "# Coherent dynamics: Charge transfer\n",
    "\n",
    "Imagine a donor acceptor system where a charge transfer is initialized by absorption of a photon. The donor is initially in its ground state and gets excited by the photon. Think of this as an electron that is promoted to an excited state. Once the system is excited the charge can travel to neighboring sites (bridge states) which have a similar energy than the excited state of the donor. The acceptor itself again has a lower energy such that the charge would finally fall into the acceptor state. This is what we observe in nature all the time, for instance in photosynthetic complexes of plants or bacteria.\n",
    "\n",
    "\n",
    "<img src=\"DonorAcceptor.png\" alt=\"Donor-Bridge-Acceptor\" style=\"width: 400px;\"/>\n",
    "\n",
    "\n",
    "In order to reach the acceptor, however, the charge needs to lose energy. Otherwise it will never go into the acceptor state but stay in the high-energy states. Let's test this:\n",
    "\n",
    "In the following we will model the system after excitation, that is we explicitly model the excited donor state, the bridge states and the acceptor. We will label the donor, the bridges and the acceptor as $N$ 'sites', and just number them from left to right where site $i=0$ is the donor and site $i=N-1$ is the acceptor, all other sites are bridge states. The energy on the sites is then given by the matrix elements\n",
    "\n",
    "\\begin{align}\n",
    "\\left<i\\right| \\hat{H} \\left| i\\right> = E_i\n",
    "\\end{align}\n",
    "\n",
    "\n",
    "For simplicity we assume that the donor an bridge states have energy $E_i = E_0$ and the acceptor state has an energy $E_{\\rm acc}$. We also assume that all states are coupled only to their nearest neighbors and that the coupling matrix element\n",
    "\n",
    "\\begin{align}\n",
    "\\left<i\\right| \\hat{H} \\left| i+1\\right> = \\left<i+1\\right| \\hat{H} \\left| i\\right> = c_{ij} = {\\rm const}\n",
    "\\end{align}\n",
    "\n",
    "is the same along the chain of states. \n",
    "\n",
    "\n",
    "## Atomic units\n",
    "\n",
    "When studying quantum mechanics numerically it is beneficial to do so in units that are suitable for representation on a computer. For instance, treating the Planck constant $h = 6.62607544267\\times10^{-34}$ Js in SI units is bound to fail. We therefore switch to <i>atomic units</i> which have comprehensible numerical values: The basis units are\n",
    "\n",
    "|Unit   | Reference    | symbol | Numerical value a.u.   | Numerical value SI    |\n",
    "|---:   |:-------------|:-------------|:-----------|:------|\n",
    "|Action | Reduced Planck constant| $\\hbar$      | 1          | $1.055\\times10^{-34}$ Js |\n",
    "|Length | Bohr redius            | $a_0$        | 1          | $5.292\\times10^{-11}$ m  |\n",
    "|Charge | Elementary charge      | $e$          | 1          | $1.602\\times10^{-19}$ C  |\n",
    "|Mass   | Electron mass          | $m_e$        | 1          | $9.109\\times10^{-31}$ kg  |\n",
    "\n",
    "\n",
    "Derived units\n",
    "\n",
    "\n",
    "|Unit   | Relation    | symbol         | Numerical value a.u.   | Numerical value SI    |\n",
    "|---:   |:-------------|:-------------|:-----------|:------|\n",
    "|Energy |  $\\frac{\\hbar^2}{m_e a_0^2}$ | $E_h$     | 1          | $4.360\\times10^{-18}$ J = 27.211 eV |\n",
    "|Time   |  $\\frac{\\hbar}{E_h}$         | $-$ (a.u.)       | 1          | $2.419\\times10^{-17}$ s = 0.024 fs  |\n",
    "\n",
    "In the literature the use of atomic units is often indicated by the statement \"$\\hbar=1$\".\n",
    "\n",
    "## The propagator\n",
    "\n",
    "In the quantum-dynamics community, the time-evolution of a wavefunction is often termed the 'propagation' of the wavefunction and any method that takes an initial state at time $t$ and 'propagates' it to some later time $t+\\Delta t$ is called a 'propagator'. This could for instance just be a some ODE integration algorithm, like a Runge-Kutta integration. In a narrower sense, the term 'propagator' refers to (possibly some approximation of) the time-evolution-operator $\\mathcal{U}$.  Given an initial state $\\psi$ at initial time $t_0$, the time-dependent Schrödinger equation can be formally solved as\n",
    "\n",
    "\\begin{align}\n",
    "\\psi(x,t) & = \\exp\\left(-\\frac{i}{\\hbar}\\hat{H}(t-t_0)\\right) \\psi(x,t_0) \\\\[.4cm]\n",
    "          &= \\mathcal{U}(t,t_0) \\psi(x,t_0)\n",
    "\\end{align}\n",
    "\n",
    "where in the first line a __time-independent__ Hamiltonian is assumed. The operator-exponential (in our case a matrix-exponential) is defined by its Taylor-expansion ($t_0=0$):\n",
    "\n",
    "\\begin{align}\n",
    " \\exp\\left(-\\frac{i}{\\hbar}\\hat{H}t\\right) = 1 +  \\left(-\\frac{i}{\\hbar}\\hat{H}t\\right) + \\frac12 \\left(-\\frac{i}{\\hbar}\\hat{H}t\\right)^2  + \\cdots\n",
    "\\end{align}\n",
    "\n",
    "If $\\psi(x,t=0) = \\phi_n(x)$ is an eigenstate of $\\hat{H}$ it is easy to show that \n",
    "\n",
    "\\begin{align}\n",
    " \\exp\\left(-\\frac{i}{\\hbar}\\hat{H}t\\right)  \\phi_n(x) =  \\exp\\left(-\\frac{i}{\\hbar}E_n t\\right)    \\phi_n(x) \n",
    "\\end{align}\n",
    "\n",
    "If one can express $\\psi(x,t=0)$ in terms of eigenstates $\\phi_n$, i.e., \n",
    "\n",
    "\\begin{align}\n",
    "\\psi(x,t=0) = \\sum_n c_n  \\phi_n(x) \n",
    "\\end{align}\n",
    "\n",
    "one can immediately obtain the system state at any given time as \n",
    "\n",
    "\\begin{align}\n",
    "\\psi(x,t) = \\sum_n c_n  \\exp\\left(-\\frac{i}{\\hbar}E_n t\\right)   \\phi_n(x) .\n",
    "\\end{align}\n",
    "\n",
    "Furthermore, a unitary transformation that diagonalizes $\\hat{H}$ also diagonalizes $\\mathcal{U}$ (switching to a basis representation now so Operators are expressed as matrices and states as vectors):\n",
    "\n",
    "\n",
    "\\begin{align}\n",
    "\\hat{H} &= Q \\, {\\rm diag}\\left\\{ E_n \\right\\}\\, Q^\\dagger \\\\\n",
    "\\mathcal{U} &= Q \\, {\\rm diag}\\left\\{ e^{-\\frac{i}{\\hbar} E_n t } \\right\\}\\, Q^\\dagger \n",
    "\\end{align}\n",
    "\n",
    "One can therefore just diagonalize the Hamiltonian matrix which one may have for instance in a grid basis, replace the diagonal matrix elements with the exponentials and transform back to the grid basis. The time-evolution is then just performed by a matrix-vector product:\n",
    "\n",
    "\\begin{align}\n",
    " \\vec{\\psi}(t)  =  \\mathcal{U}\\vec{\\psi}(t=0) \n",
    "\\end{align}\n",
    "\n",
    "Note that any power $\\mathcal{U}^j$ propagates the state to $j\\cdot t$:\n",
    "\n",
    "\\begin{align}\n",
    " \\vec{\\psi}(j\\cdot t)  =  \\mathcal{U}^j \\vec{\\psi}(t=0) \n",
    "\\end{align}\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "0ebfe691",
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy import *\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.linalg import eigh\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "6c49e7df",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1.  0.1 0.  0.  0. ]\n",
      " [0.1 1.  0.1 0.  0. ]\n",
      " [0.  0.1 1.  0.1 0. ]\n",
      " [0.  0.  0.1 1.  0.1]\n",
      " [0.  0.  0.  0.1 0. ]]\n"
     ]
    }
   ],
   "source": [
    "# number of sites\n",
    "N = 5\n",
    "\n",
    "# label them\n",
    "sites = array(range(N))\n",
    "\n",
    "# donor and bridge energy\n",
    "E0 = 1.0\n",
    "# acceptor energy\n",
    "Eacc = 0.0\n",
    "# coupling between nearest neighbors\n",
    "cij = 0.1\n",
    "\n",
    "# Hamiltonian matrix\n",
    "H = zeros([N,N], dtype=double)\n",
    "\n",
    "for i in range(N-1):\n",
    "    H[i,i] = E0\n",
    "    H[i,i+1] = cij\n",
    "    H[i+1,i] = cij\n",
    "\n",
    "H[N-1,N-1] = Eacc\n",
    "\n",
    "print(H)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "0de066d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate eigenstates and eigenenergies\n",
    "Eval, Evec = eigh(H)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d22bbc6",
   "metadata": {},
   "source": [
    "# Time evolution\n",
    "\n",
    "We want to observe the time evolution of the system after photoexcitation. We hence prepare the initial state on the donor\n",
    "\n",
    "\\begin{align}\n",
    "\\left| \\Psi(t=0)\\right> = \\left|0\\right> \n",
    "\\end{align}\n",
    "\n",
    "\n",
    "\n",
    "Build the propagator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "fb50269a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# time step\n",
    "dt = 0.2\n",
    "# final time\n",
    "Tmax = 50.0\n",
    "# number of time steps\n",
    "nT = int(Tmax/dt)+1\n",
    "\n",
    "# time evolution operator\n",
    "propagator = Evec @ diag( exp(-1j * Eval * dt) ) @ Evec.T\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "ec91901f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# initial state\n",
    "psi = zeros(N, dtype=complex)\n",
    "\n",
    "# allocate some storeage for psi over t\n",
    "psi[0] = 1.0\n",
    "psit =  zeros((nT,N), dtype=complex)\n",
    "psit[0,:] = psi[:]\n",
    "\n",
    "t0 = 0\n",
    "time = zeros(nT, dtype=double)\n",
    "time[0] = t0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "45cea025",
   "metadata": {},
   "outputs": [],
   "source": [
    "# do the propagation\n",
    "for i in range(1,nT):\n",
    "    t = t0 + i*dt\n",
    "    psi = propagator @ psi\n",
    "    psit[i,:] = psi[:]\n",
    "    time[i] = t"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "7ec19de3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "183443fd452a46f0a2ebf4488ab6809f",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(IntSlider(value=0, description='n', max=251, step=2), Output()), _dom_classes=('widget-i…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function __main__.plot_psi(n=0)>"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# look at the population of the sites interactively\n",
    "\n",
    "def plot_psi(n=0):\n",
    "\n",
    "    mf = 16\n",
    "    fig, ax = plt.subplots(figsize=(12,6))\n",
    "\n",
    "    psi = psit[n,:]\n",
    "    \n",
    "    ax.bar(sites, abs(psi)**2, color='blue', label=r'$|\\Psi|^2$')\n",
    "    ax.set_ylabel('Population',fontsize=mf)\n",
    "    ax.set_xlabel('$sites$',fontsize=mf)\n",
    "    ax.set_ylim(0, 1.1)\n",
    "    plt.subplots_adjust(hspace = 0.0)\n",
    "\n",
    "    \n",
    "    ax.set_title('Population of sites at time: {:.2f}'.format(time[n]), fontsize=mf)\n",
    "\n",
    "\n",
    "    \n",
    "    plt.legend(loc='best', shadow=False, fontsize=mf, frameon=False,\n",
    "                   borderpad=0.1, labelspacing = 0, handlelength = 0.8)\n",
    "    plt.show()\n",
    "    \n",
    "interact(plot_psi, n=(0, nT, 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "537f4079",
   "metadata": {},
   "source": [
    "We observe that the acceptor never receives a substantial amount of population. \n",
    "### The charge is never transferred.\n",
    "What is needed is a mechanism for the charge to dissipate energy, for instance into an environment. This could for instance be vibrational degrees of freedom of the molecules that make up the bridge and acceptor states and even their environment, for instance a solvent or a protein matrix. \n",
    "\n",
    "Such processes we will be investigated within this course. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5b74ead",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
