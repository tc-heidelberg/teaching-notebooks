---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

+++

# Grid-based and Spectral methods

```{code-cell} ipython3
from numpy import *
from scipy import linalg as la
import matplotlib.pyplot as plt
prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["xtick.labelsize"] = 14
plt.rcParams["ytick.labelsize"] = 14
plt.rcParams["figure.figsize"] = (8,5)
```

# Quantum Particle in a Box on a grid
The Hamiltonian for this problem **inside** the box reads
\begin{align}
\hat{H} = \hat{T} + V = -\frac{\hbar^2}{2m} \frac{\partial^2}{\partial x^2}
\end{align}

## Time-independent Schrödinger Equation
\begin{align}
H \Psi(x) = E \Psi(x)
\end{align}


We know the analytical solution to this problem - the wave functions for the 1D box are ($n$ counts from 1, not 0)
\begin{align}
\Psi_n(x) =\sqrt{\frac{2}{L}} \sin\left(\frac{n \pi (x+L/2)}{L}\right)
\end{align}
and the corresponding energies are
\begin{align}
E_n =\frac{n^2 \pi^2 \hbar^2}{2mL^2}
\end{align}

Note that for the Time-independent Schrödinger Equation we can choose the states real and will do so in this notebook. 


## Matrix form of quantum mechanics

https://doi.org/10.1007/BF01328377 \
https://doi.org/10.1007/BF01379806 \
https://aapt.scitation.org/doi/10.1119/1.3009634

As quantum-mechanical systems exhibit discrete energies and their states can be described using functions (vectors), and operators like ${\hat{x}}$ and ${\hat{p}}$ are not commutative (cannot be determined simultaneously) this naturally leads to the emergence of __matrix mechanics__. 

One first chooses a (finite) orthonormal basis set $\{\phi_i\}$ and then expresses operators in this basis:

\begin{align}
\hat{x} \rightarrow x_{ij} = \left\lt \phi_i \right| \hat{x} \left| \phi_j\right\gt
\end{align}
\begin{align}
\hat{p} \rightarrow p_{ij} = \left\lt \phi_i \right| \hat{p} \left| \phi_j\right\gt
\end{align}

etc. One is completely free in choosing the basis $\{\phi_i\}$ but typically one uses a basis that can be treated analytically and already roughly resembles the properties of the system. Examples for basis functions could be particle in a box eigenfunctions, harmonic oscillator eigenfunctions, Legendre polynomials, spherical harmonics, etc.    

One also represents the wavefunction in this basis:
\begin{align}
\left|\Psi\right\gt & \approx \sum_j  \left|\Phi_j\right\gt \left\lt\Phi_j\right| \left. \Psi\right\gt \\
  & =  \sum_j c_j \left|\Phi_j \right\gt \\
\end{align}

What we really only store on the computer are the matrix elements of the operators and the coefficients of the wavefunction. 


In a computer representation, we need to resort to linear algebra, and all operations in quantum mechanics can be described by vector-vector and matrix-vector multiplication, and matrix diagonalization. 

In the following, we will look at the matrix representation of the quantum particle in a box.


### Hamiltonian Matrix

The matrix elements of the Hamiltonian: 
\begin{align}
 H_{i,j} = \left\lt \phi_i \right| H \left| \phi_j\right\gt
\end{align}

In the following we will be working in **coordinate representation**, that is, we will have wavefunctions $\psi(x)$ and we will just represent them on a number of equidistant points we get for instance from a `numpy.linspace`. 

### Normalization

We will for now use the Riemann sum for evaluating integrals. That is, we will evaluate integrals such as the norm of the wavefunction as: 
\begin{align}
  \int\limits_{-L/2}^{L/2}\left|\psi(x)\right|^2 dx  = \left\lt \psi(x) \right.\left| \psi(x)\right\gt = \delta x \sum_i \left|\psi(x_i)\right|^2 = 1
\end{align}

Note, that since $\delta x$ is just a constant we can simply multiply it into the wavefunction define new sampled wavefunction 

\begin{align}
 \Psi_i = \Psi(x_i) \rightarrow \sqrt{\delta x} \psi(x_i)
\end{align}

such that 

\begin{align}
  \sum_i \left|\Psi_i\right|^2 = 1
\end{align}

and we do not have to carry around the $\delta x$ all the time. With this we can use things like:
\begin{align}
\left\lt \Psi_1(x) \right.\left| \Psi_2(x)\right\gt &= \sum_i \Psi_{1,i}^*  \Psi_{2,i} \\
 & = \vec{\Psi}_1^* \vec{\Psi}_2
\end{align}
which is just a dot-product of two vectors. 


What are the ${\phi_i}$ in this case? They are just functions with all points from the `numpy.linspace` set to zero, except one, the $i$th, which is set to unity.  We have what is called **discrete delta property**
\begin{align}
\phi_j(x_i) = \phi_{j,i}  \delta_{ji}
\end{align}

and 

\begin{align}
 \left\lt \phi_j(x) \right.\left| \phi_j(x)\right\gt = \sum_i \left(\phi_{j,i}\right)^2 = 1
\end{align}

and 

\begin{align}
 \left\lt \phi_j(x) \right.\left| \phi_l(x)\right\gt = \sum_i \phi_{j,i} \phi_{l,i}  = \delta_{jl}
\end{align}

### Wavefunction in this basis

Expressing the wavefunction in a basis ($\approx$ because of finite sum):

\begin{align}
  \left|\Psi\right\gt & \approx \sum_j  \left|\phi_j\right\gt \left\lt\phi_j\right| \left. \Psi\right\gt \\
  & =  \sum_j c_j \left|\phi_j \right\gt \\
  & =  \sum_j c_j \phi_j(x)  
\end{align}
 
We really only store the coefficients in memory while keeping in mind what basis they refer to. For a grid based basis function we have  $\phi_j(x_i) = \delta_{j,i}$ and  $c_j = \Psi(x_i)\cdot\sqrt{\delta x}$


### Kinetic energy

The kinetic energy is proportional to the second derivative of the wavefunction. In coordinate representation, we can evaluate the second derivative using the three-point finite differences,
\begin{align}
\frac{d^2 \Psi}{dx^2} \approx \frac{\Psi(x_{j-1})-2\Psi(x_j)+\Psi(x_{j+1})}{\delta x^2}.
\end{align}
This corresponds to applying the **Laplacian** matrix $L$ to the wavefunction $\vec\Psi$ sampled on equidistant grid points $\lbrace x_j \rbrace$ from the `numpy.linspace`. The Laplacian is given by

\begin{align}
L = \frac{1}{\delta x^2}\left[
\begin{array}{cccccc}
-2 & 1 & & & & \\
1 & -2 & 1 & & & \\
& \ddots & \ddots & \ddots & & \\
& & 1 & -2 & 1 & \\
& & & \ddots & \ddots & \ddots \\
& & & & 1 & -2 \\
\end{array} 
\right]
\end{align}
 Thus, we can use the Laplacian matrix directly in the Hamiltonian,
\begin{align}
 T = -\frac{1}{2m} L_{ij}.
\end{align}

We have to be careful, however, with setting up the Hamiltonian with the Laplacian matrix: there is a problem with the edges of the grid (see lecture on differentiation). If we set the first and the last point of out interval to be the first and last sampling point we will not get the correct result. Luckily the wavefunction is zero on the edges anyways so we do not need to model these points explicitly. We can set up the Laplacian matrix only on the middle points. 



### Potential energy

For the time being we only look at the case where potential energy is zero in the area where there wavefunction resides. It will hence not enter the Hamiltonian matrix explicitly. 


## Atomic units

When studying quantum mechanics numerically it is beneficial to do so in units that are suitable for representation on a computer. For instance, treating the Planck constant $h = 6.62607544267\times10^{-34}$ Js in SI units is bound to fail. We therefore switch to <i>atomic units</i> which have comprehensible numerical values: The basis units are

|Unit   | Reference    | symbol | Numerical value a.u.   | Numerical value SI    |
|---:   |:-------------|:-------------|:-----------|:------|
|Action | Reduced Planck constant| $\hbar$      | 1          | $1.055\times10^{-34}$ Js |
|Length | Bohr redius            | $a_0$        | 1          | $5.292\times10^{-11}$ m  |
|Charge | Elementary charge      | $e$          | 1          | $1.602\times10^{-19}$ C  |
|Mass   | Electron mass          | $m_e$        | 1          | $9.109\times10^{-31}$ kg  |


Derived units


|Unit   | Relation    | symbol         | Numerical value a.u.   | Numerical value SI    |
|---:   |:-------------|:-------------|:-----------|:------|
|Energy |  $\frac{\hbar^2}{m_e a_0^2}$ | $E_h$     | 1          | $4.360\times10^{-18}$ J = 27.211 eV |
|Time   |  $\frac{\hbar}{E_h}$         | $-$ (a.u.)       | 1          | $2.419\times10^{-17}$ s = 0.024 fs  |

```{code-cell} ipython3
# atomic units
hbar = 1.0
m = 1.0

# set number of points for numerical approximation
steps = 50

# width of the box
L = 1

dx = L/(steps + 1)

# grid along x
L2 = L/2-dx
xgrid, h = linspace(-L2,L2,steps,retstep=True)

# get step size h for the numerical derivative
# create the second derivative matrix

Laplacian = (-2.0 * diag(ones(steps))+diag(ones(steps-1),1)+diag(ones(steps-1),-1))/(float)(dx**2)

# we can look at this matrix - it is a band matrix as only the j-1, j, and j+1 
# grid points are required for each second derivative - see lecture 5
# print(Laplacian)

#plt.rcParams["figure.figsize"] = (9,9)
plt.matshow(Laplacian)
plt.show()
```

# Matrix diagonalization

+++

By discretizing the problem using a grid basis, we have turned the Time-independent Schrödinger equation into a linear algebra problem: **matrix diagonalization**. 

The solutions to the Time-independent Schrödinger equation correspond to eigenvectors $\vec \Psi$ of the Hamiltonian matrix $\mathbf{H}$
\begin{align}
    \mathbf{H}\vec\Psi=E\vec\Psi,
\end{align}
with eigenvalue (eigenenergy) $E$.

Matrix diagonalization can be carried out using direct approaches, that directly decompose the matrix as a whole, and iterative routines like Davidson or Lanczos that operate on a subspace of the full matrix (Krylov space for Lanczos). Overall, matrix diagonalization is complicated business, and depending on the kind of values in the matrix, different routines are chosen. For us this means that have to distinguish between real and complex matrices, symmetric and nonsymmetric matrices, and we need to be aware that different algorithms are used for the solution. Most important for us will be routines for symmetric (or Hermitian)  and symmetric (Hermitian) tri-diagonal matrices in the future. 

For practical purposes we will rely on existing routines that are implemented in various libraries and just use them and do not bother with implementation details.

```{code-cell} ipython3
Laplacian_diag = -2.0 * ones(steps)/(dx**2)
Laplacian_subdiag = ones(steps-1)/(dx**2)


Hamiltonian_diag = ((-0.5*(hbar**2)/m)) * Laplacian_diag 
Hamiltonian_subdiag = ((-0.5*(hbar**2)/m)) * Laplacian_subdiag

# diagonalize the Hamiltonian (Hermitian/symmetric matrix, uses Lapack dsyevd routine)
# DOES NOT CHECK THAT YOUR MATRIX IS HERMITIAN/SYMMETRIC SO YOU NEED TO BE SURE ABOUT IT
eigenval, eigenvec = la.eigh_tridiagonal(Hamiltonian_diag, Hamiltonian_subdiag)

# print(eigenvec)
# print(eigenval)

# the sign of the eigenvectors is arbitrary. We just loop through then
# and fix the first element of each eigenvector to be positive
for n in range(steps):
    if eigenvec[0,n] < 0.0:
        eigenvec[:,n] = -eigenvec[:,n]

# Note that the eigenvectors are normalized and orthogonal:
B = eigenvec.T @ eigenvec
# -------------^ matmul

unit = diag(ones(steps))
allclose(B,unit)
```

We know the exact solutions for the particle in a box (see Introduction). This allows us to compare our numerical with analytical results.

```{code-cell} ipython3
# import some interactive tools
from ipywidgets import interact
```

```{code-cell} ipython3
# compare numerical to the analytical solution

def evec_box(xgrid,L):
    "create the analytical particle in a box functions"
    
    steps=len(xgrid)             # number of points
    dx=xgrid[1]-xgrid[0]         # dx
    psi_n = empty((steps,steps)) # space for all analytical solutions
    prefac=sqrt(2*dx/L)          # normalization: sqrt(dx) multiplied into wavefunction
    
    for n in range(1,steps+1):  # n starts at 1
        angle = n * pi * linspace(dx,L-dx,steps)/L
        psi_n[:,n-
              1] = sin(angle)*prefac
    return psi_n

def energy_box(N,L,m):
    energy=empty(N)
    for n in range(1,N+1):
        energy[n-1] = n**2 * pi**2 / (m*2*L**2) 
    return energy

wavef = evec_box(xgrid,L)
ene = energy_box(len(xgrid),L,m)

# dynamically create figure
def plot_psi_ex(n=1):

    # we have eigenstates starting at n=1
    # but the numpy arrays start at index zero
    # so we just rename the n to n-1 in 
    # order to get the correct vectors and energies
    nm1 = n-1
    mf = 16
    
    fig, ax = plt.subplots(3,figsize=(6,3*3),sharex=True)
    
    ax[0].plot(xgrid,eigenvec[:,nm1])
    ax[0].plot(xgrid,wavef[:,nm1],color='black',linestyle = '--')
    ax[0].set_ylabel(r'$\Psi$',fontsize=mf)
    ax[0].text(-L2,max(eigenvec[:,nm1]*0.9),'Wave function',fontsize=mf)

    # plot 2
    ax[1].plot(xgrid,eigenvec[:,nm1]**2)
    ax[1].plot(xgrid,wavef[nm1]**2,color='black',linestyle = '--')
    ax[1].set_ylabel(r'$|\Psi|^2$',fontsize=mf)
    ax[1].text(-L2,max(eigenvec[:,nm1]**2*0.9),'Density',fontsize=mf)


    ax[2].hlines(eigenval[nm1],-L2,L2, label="calculated")
    ax[2].hlines(ene[nm1],-L2,L2,color='black',linestyle = '--', label="exact")
    ax[2].set_xlabel('x (a.u.)',fontsize=mf)
    ax[2].set_ylabel('E (E$_h$)',fontsize=mf)
    ax[2].text(-L2,eigenval[nm1],'Energy',fontsize=mf)
    ax[2].set_ylim(0,ene[0:10].max()+10)
    for i in range(3):
        ax[i].xaxis.set_tick_params(labelsize=mf)
        ax[i].yaxis.set_tick_params(labelsize=mf)
        ax[i].set_xlim(-L/2, L/2)
    l = plt.legend(loc='upper right', fontsize=mf, frameon=False)
    #plt.subplots_adjust(hspace = 0.0)
    plt.show()

interact(plot_psi_ex, n=(1, 10, 1))
```

## Questions
* How many energy eigenstates do you obtain upon diagonalization? Why is that so? 
* How do the analytical and numerical solution to the particle in a box compare in terms of accuracy for the first ten states? How can you influence this?
* Adapt the code to obtain the eigenstates of a harmonic oscillator potential 
\begin{align}
V(x) = \frac12 m \omega^2 x^2
\end{align}
and Hamiltonian
\begin{align}
H = -  \frac1{2m}\frac{\partial^2}{\partial x^2} + \frac12 m \omega^2 x^2.
\end{align}
You can use the following parameters for grid and potential.

```{code-cell} ipython3
# atomic units
hbar = 1.0
m = 2.0
omega = 2.0

# set number of points for numerical approximation
steps = 250

# width of the box
L = 15

dx = L/(steps + 1)

# grid along x
L2 = L/2-dx
xgrid = linspace(-L2,L2,steps)

V = 0.5*m*omega**2*xgrid**2
```

# Perturbed Quantum Particle in a Box with Spectral Method

```{code-cell} ipython3
import scipy.integrate as spi
```

Instead of just sampling the wavefunction $\left|\psi\right\rangle$ on a grid one can also use a complete set of  basis functions $\{\left|\phi_j\right\rangle\}_{j=0}^{N-1}$ to represent the wavefunction. In practice, only a finite set of basis functions can be employed which requires *trunctation* of the basis after $N$ basis functions. To this end one defines the projector  
\begin{align}
P = \sum_{j=0}^{N-1} \left|\phi_j\right\rangle\left\langle\phi_j\right| 
\end{align}
such that 

\begin{align}
\left|\psi\right\rangle \rightarrow P \left|\psi\right\rangle = \sum_j^{N-1}  \left|\phi_j\right\rangle\left\langle\phi_j\right| \left|\psi\right\rangle = \sum_j^{N-1} a_j \left|\phi_j\right\rangle
\end{align}

Any operator $\hat{A}$ is then replaced by 
\begin{align}
 \hat{A} \rightarrow P\hat{A}P = \sum_{jk}^{N-1} \left|\phi_j\right\rangle\left\langle\phi_j\right| \hat{A}  \left|\phi_k\right\rangle\left\langle\phi_k\right| =  \sum_{jk}^{N-1}\left|\phi_j\right\rangle A_{jk} \left\langle\phi_k\right| 
\end{align}

What we really only store then are the coefficients $a_j$ and the matrix elements $A_{jk}$

Also the Hamiltonian operator $\hat{H}$ will be represented in this basis:

\begin{align}
 \hat{H} \rightarrow P\hat{H}P &=  P\hat{T}P +  P\hat{V}P \\
  &= \sum_{jk}^{N-1}  \sum_{jk}^{N-1}\left|\phi_j\right\rangle T_{jk} \left\langle\phi_k\right| +   \sum_{jk}^{N-1}  \sum_{jk}^{N-1}\left|\phi_j\right\rangle V_{jk} \left\langle\phi_k\right|
\end{align}

we will demand that the matrix elements $T_{jk}$ are known analytically and $V_{jk} = \int dx \phi_j^*(x) V(x)\phi_k(x)$ must be evaluated by using numerical or analytical integration (which may be problematic). 

In the following we will use the analytically known eigenfunctions of the particle in a box as our basis functions (note: index counts from $j=1$):

\begin{align}
 \phi_j(x) = \sqrt{\frac{2}{L}} \sin\left(\frac{j\pi}{L}(x-x_0)\right)
\end{align}

with 
\begin{align}
 \left\langle\phi_j\right|\left.\phi_k\right\rangle = \delta_{jk}
\end{align}

and 

\begin{align}
 \left\langle\phi_j\right|\hat{T} \left|\phi_k\right\rangle = \frac{j^2 \pi^2}{2mL^2} \delta_{jk}
\end{align}

```{code-cell} ipython3
L     = 1.0    # Box length
m     = 100.0  # particle's mass
Q     = -1.0   # particles's charge
Np    = 150    # Number of real space points in segment
Nb    = 10     # Number of basis functions (pbox solutions)
steps = 100
A0    = 0.5   
E0    = 0.1   # Amplitude of constant electric field

dx = L/(steps + 1)
L2 = L/2-dx
xgrid = linspace(-L2,L2,steps)
```

```{code-cell} ipython3
def evec_box(xgrid,L,N):
    "create the analytical particle in a box functions"
    
    steps=len(xgrid)             # number of points
    dx=xgrid[1]-xgrid[0]         # dx
    psi_n = empty((steps,N))     # space for all analytical solutions
   
    for n in range(1, N+1):  # n starts at 1
        angle = n * pi * linspace(dx,L-dx,steps)/L
        psi_n[:,n-1] = sin(angle)
    return psi_n

def Vfunc(x):
    #return 0
    return A0*exp(-0.5*x**2/0.01) - Q*x*E0
    #return 

def matrix_element_V(V,B,i,j):
    """
    Return matrix element of the potential for indices i, j
    The integration uses a simple Simpson integrator
    
    V  : potential energy function evaluated on xgrid
    B  : array of primitive basis functions sampled on xgrid
    i,j: quantum numbers of the basis states (lowest index = 0, not 1!)
    """
    return spi.simpson(B[:,i]*V*B[:,j],x=xgrid)
```

Let us use this approach to obtain the eigenstates of a particle in a perturbed box. The perturbing potential has the form
\begin{align}
 V(x) = A_0\exp\left(-\frac{x^2}{2\sigma}\right) - qxE_0.
\end{align}

```{code-cell} ipython3
V = Vfunc(xgrid)
psi_n = evec_box(xgrid,L,Nb)

mf = 14
plt.plot(xgrid,V)
plt.xlabel('x',fontsize=mf)
plt.ylabel('V(x)',fontsize=mf)
plt.show()
```

Represent $\hat H$ in the basis of the first $N$ particle in a box eigenfunctions. This gives a $N \times N$ matrix $\mathbf{H}$, which we store in a two-dimensional `numpy.array`.
The wavefunction is represented as a coefficient vector,
\begin{align}
  \Psi(x) = \sum_{j=1}^N c_j \phi_j(x)\\
  \Psi(x) \rightarrow \vec {\Psi} = \vec c.
\end{align}
To convert this to coordinate space, we have to multiply from the left with the transformation matrix 
\begin{align}
\mathbf A=\left( \phi_1(x), \phi_2(x),\dots, \phi_N(x)\right),
\end{align}
where the $j$-th column of $\mathbf{A}$ corresponds to the $j$-th basis function sampled on the grid points.

```{code-cell} ipython3
Top = zeros((Nb,Nb),float)
for j in range(Nb):
    Top[j,j] = j**2*pi**2/(2*m*L**2)
    
Vop = zeros((Nb,Nb),float)
for i in range(Nb):
    for j in range(Nb):
        Vop[i,j] = matrix_element_V(V,psi_n,i,j)
        
H = Top + Vop
```

```{code-cell} ipython3
evals,evecs = linalg.eigh(H)
psiX = psi_n  @ evecs

plt.plot(abs(psiX[:,0])**2)
plt.xlabel('x',fontsize=mf)
plt.ylabel(r'$|\psi(x)|^2$',fontsize=mf)
plt.show()
```

```{code-cell} ipython3
def setup_H(Nb):
    psi_n = evec_box(xgrid,L,Nb)
    Top = zeros((Nb,Nb),float)
    for j in range(Nb):
        Top[j,j] = j**2*pi**2/(2*m*L**2)

    Vop = zeros((Nb,Nb),float)
    for i in range(Nb):
        for j in range(Nb):
            Vop[i,j] = matrix_element_V(V,psi_n,i,j)

    H = Top + Vop
    return H
```

```{code-cell} ipython3
evalsGS = []
for n in arange(2,20):
    H = setup_H(n)
    evals,evecs = linalg.eigh(H)
    
    evalsGS.append(evals[0])
    
plt.scatter(arange(2,20),evalsGS)
plt.plot(arange(2,20),evalsGS)
plt.xlabel('$N$',fontsize=mf)
plt.ylabel('$E_{GS}$ (au)',fontsize=mf)
plt.show()
```

```{code-cell} ipython3

```
