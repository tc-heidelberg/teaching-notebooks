# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Numerical approximation to a Dirac $\delta$-function

# %%
import numpy as np
import scipy.integrate as spi
import matplotlib.pyplot as plt
from matplotlib import rc
#plt.rc('text', usetex=False)
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

# %% [markdown]
# Copyright (C) 2020, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>
# <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# %% [markdown] editable=true slideshow={"slide_type": ""}
# ### Approximation as an integral over complex exponentials
#
# We illustrate numerically and graphically how the usual definition of a $\delta$-function
#
# $$
# 2\pi\, \delta(x-x^\prime) = \int_{-\infty}^{\infty} e^{i(x-x^\prime)k} dk
# $$
#
# converges to the expected result, namely a spike of inifinite height and area 1.
#
# The exponential on the RHS of the equartion above can be written as $e^{ikx}/e^{ikx^\prime}$. Intuitively, if the integral on the RHS of the equation is performed from $-\infty$ to $\infty$, even the slightest dephasing of the exponentials leads to a complete cancellation of the integral. Only when $x = x^\prime$ does the integral not vanish. 

# %%
# Discretization of the x (e.g. position) coordinate
xmin = -np.pi
xmax = np.pi
nx = 2600
X = np.linspace(xmin, xmax, nx)


# %% [markdown]
# We approximate the integral over the infinite $k$-space as a discrete sum over the finite interval
# $[-N_k\Delta_k, N_k\Delta_k]$
#
# $$
# \delta(x-x^\prime) \approx \frac{1}{2\pi} \sum_{l=-N_k}^{N_k} e^{i(x-x^\prime)l \Delta_k} \Delta_k
# $$
#
# where the discrete values of the $k$ variable are $k_l = l \Delta_k$ and the differential $dk$ has been turned into the separation of the discrete values in $k$-space.

# %%
# This function returns the approximation of the delta function based on a discrete sampling of the k-space
# represented in the (also discrete) x-space.
def deltaFunc(x, klim, Nk):
    """
    Return the approximation of the delta function based on a discrete
    sampling of the k-space represented in the (also discrete) x-space.
    x -- array with discrete x-space values
    klim -- absolute value of the k-space limits
    Nk -- number of discrete k-space values in the positive (negative) semi-space
    """
    K = np.linspace(-klim, klim, 2*Nk + 1)
    Dk = K[1]-K[0]
    delta = np.zeros(len(x), complex)
    for kvalue in K:
        dtmp = np.exp(1j*x*kvalue)*Dk/2.0/np.pi
        delta = delta + dtmp
    return delta


# %%
plt.xlabel('x')
Nk = 400
klim = 50
plt.plot(X, np.real(deltaFunc(X, klim, Nk)), label='Klim='+str(klim))
plt.legend()
#plt.savefig('delta.pdf')
plt.show()


# %% [markdown]
# The cell below lets you interact with two convergence parameters. $K_\textrm{lim}$ determines the largest and smallest momentum included in the representation of the $\delta$-function. The larger $K_\textrm{lim}$, the wider the integration limit, and the more momenta are available to "synthesize" the delta peak, which it becomes a better approximation to the single spike. $N_k$ controls the accuracy of the numerical integration and should simply be set sufficiently high.

# %%
def plotDelta(Klim, Nk):
    plt.plot(X, np.real(deltaFunc(X, Klim, Nk)), label='Klim='+str(klim))
interact(plotDelta, Klim=(1,100,1), Nk=(1,500,10))


# %% [markdown]
# As can be checked below, the area under the delta function is very close to 1 and becomes closer to unity as the absolute value of the limits of the momentum integration is made larger.

# %%
def Area(x, delta):
    A = np.real(spi.simpson(delta, x=x))
    return A


# %%
Area(X, deltaFunc(X, 200, 500))


# %% [markdown]
# ### Approximation as a Sinc-square function

# %% [markdown]
# Another useful approximation to a $\delta$-function is via the sinc-square $(\sin(x)/x)^2$ function. In general, one can make use of the identity
#
# \begin{align}
#     \frac{1}{\pi b}\int_{-\infty}^{+\infty} \left( \frac{\sin(x-a)\,b}{x-a} \right)^2 dx = 1
# \end{align}

# %%
def sinc2Func(x, a, b):
    # return a sinc-square function normalized to 1
    sinc = (1.0/np.pi/b)*(np.sin((x - a)*b)/(x - a))**2
    return sinc


# %%
def plotDeltaSinc(a, b):
    sinc = sinc2Func(X, a, b)
    plt.ylim(0, 2.5)
    plt.plot(X, sinc)
interact(plotDeltaSinc, a=(-2, 2, 1), b=(1, 50, 1))

# %%
Area(X, sinc2Func(X, 0, 45))
