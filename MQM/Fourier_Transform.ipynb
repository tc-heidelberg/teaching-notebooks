{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "917cb838",
   "metadata": {},
   "source": [
    "Mathematics for Quantum Mechanics WS 23/24\n",
    "\n",
    "# The Fourier transform\n",
    "\n",
    "In this notebook we will use the symmetric definition of the Fourier transform:\n",
    "\n",
    "\\begin{align}\n",
    "\\tilde f(k) & = \\frac1{\\sqrt{2\\pi}}\\int\\limits_{-\\infty}^{\\infty} f(x)e^{-ikx} dx\\\\\n",
    "f(x) & = \\frac1{\\sqrt{2\\pi}}\\int\\limits_{-\\infty}^{\\infty} \\tilde f(k)e^{ikx} dk\n",
    "\\end{align}\n",
    "\n",
    "The following demonstrates a number of features of the Fourier transform. For numerical purposes we will need to limit ourselves to finite intervals in $x$ and $k$ instead of the infinite ones as in the formulas above. This also means we will have to restrict the functions we use to ones that vanish outside the intervals we define. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f037cdef",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import ipywidgets as widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46039f6a",
   "metadata": {},
   "source": [
    "## Examples of the Fourier transform\n",
    "\n",
    "Let us start with a Gaussian function\n",
    "\n",
    "\\begin{align}\n",
    "f(x) & = e^{-\\frac{x^2}{2\\sigma^2}}\n",
    "\\end{align}\n",
    "\n",
    "And calculate the Fourier transform. The functions below implement the Gaussian function and its first two derivatives, as well as a convenience function that samples a given function in some $x$-interval."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5c7b41e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def function1(x, sigma):\n",
    "    \"Gaussian function\"\n",
    "    return np.exp(-x**2/2/sigma**2)\n",
    "\n",
    "\n",
    "def dfunction1(x, sigma):\n",
    "    \"Derivative of function1 (analytic)\"\n",
    "    return np.exp(-x**2/2/sigma**2)*(-x/sigma**2)\n",
    "\n",
    "def ddfunction1(x, sigma):\n",
    "    \"Second derivative of function1 (analytic)\"\n",
    "    return np.exp(-x**2/2/sigma**2)*(-x/sigma**2)**2 - np.exp(-x**2/2/sigma**2)/sigma**2\n",
    "\n",
    "\n",
    "def sample_func(f, xmin, xmax, Nx, kwargs={}):\n",
    "    \"\"\"\n",
    "    Sample a function in coordinate domain in interval [xmin, xmax)\n",
    "     \n",
    "    f: the function\n",
    "    xmin: start if interval\n",
    "    xmax: end of interval (excluding xmax)\n",
    "    Nx: number of points in [xmin, xmax)\n",
    "    kwargs: any further keyword arguments f may require (just passed on)\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    x = np.linspace(xmin, xmax, Nx, endpoint=False)\n",
    "    fx = f(x, **kwargs)\n",
    "    return x, fx\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37e685b3",
   "metadata": {},
   "source": [
    "Below are implementations of the Fourier transform. \n",
    "\n",
    "The implementations below are numerically not very efficient. In numerics one would usually use an implementation of the so-called \"Fast Fourier transform\", FFT, but as all implementations of FFTs behave slightly different from the analytic Fourier transforms let's skip this here and stick to the analytic form. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c90597d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fourier_single_k(x,fx,k):\n",
    "    \"\"\"\n",
    "    integrate dx 1/sqrt(2pi) f(x)exp(-ikx) for a single k\n",
    "    \n",
    "    x: points in coordinate domain (array, equidistant)\n",
    "    fx: f evaluated on points x (array)\n",
    "\n",
    "    k: 'frequency' k\n",
    "    \"\"\"\n",
    "    dx = x[1]-x[0]\n",
    "    wave = np.exp(-1j * k * x)\n",
    "    fk = (fx*wave).sum()/np.sqrt(np.pi*2)*dx\n",
    "    return fk\n",
    "    \n",
    "def inverse_fourier_single_x(k,fk,x):\n",
    "    \"\"\"\n",
    "    integrate dk 1/sqrt(2pi)  f(k)exp(ikx) for a single x\n",
    "    \n",
    "    k: points in Fourier domain (array, equidistant)\n",
    "    fk: f(k) evaluated on points k (array)\n",
    "    x: 'coordinate' x\n",
    "    \"\"\"\n",
    "    \n",
    "    dk = k[1]-k[0]\n",
    "    wave = np.exp(1j * k * x)\n",
    "    fx = (fk*wave).sum()/np.sqrt(np.pi*2)*dk\n",
    "    return fx\n",
    "    \n",
    "def fourier(x, fx, kmin, kmax, Nk):\n",
    "    \"\"\"\n",
    "    integrate dx 1/sqrt(2pi) f(x)exp(-ikx) for k\n",
    "    in interval [kmin, kmax)\n",
    "    \n",
    "    x: points in coordinate domain (array, equidistant)\n",
    "    fx: f evaluated on points x (array)\n",
    "    kmin: start if k-interval\n",
    "    kmax: end of k-interval (excluding kmax)\n",
    "    NK: number of points in [kmin, kmax)\n",
    "    \n",
    "    returns \n",
    "    \n",
    "    k: points in k-domain (array, equidistant)\n",
    "    fk: f(k) evaluated on points k (array)\n",
    "    \"\"\"\n",
    "    \n",
    "    k = np.linspace(kmin,kmax,Nk,endpoint=False,retstep=False)\n",
    "    fk = np.zeros(len(k), dtype=complex)\n",
    "    \n",
    "    for n,kk in enumerate(k):\n",
    "        fk[n] = fourier_single_k(x,fx,kk)\n",
    "    \n",
    "    return k, fk\n",
    "    \n",
    "\n",
    "def inverse_fourier(k,fk, xmin, xmax, Nx):\n",
    "    \"\"\"\n",
    "    integrate dk 1/sqrt(2pi) f(k)exp(ikx) for x\n",
    "    in interval [xmin, xmax)\n",
    "    \n",
    "    k: points in k domain (array, equidistant)\n",
    "    fk: f(k) evaluated on points k (array)\n",
    "    xmin: start if x-interval\n",
    "    xmax: end of x-interval (excluding kmax)\n",
    "    NX: number of points in [xmin, xmax)\n",
    "    \n",
    "    returns \n",
    "    \n",
    "    x: points in x-domain (array, equidistant)\n",
    "    fx: f(x) evaluated on points x (array)\n",
    "    \"\"\"\n",
    "    \n",
    "    x = np.linspace(xmin,xmax,Nx,endpoint=False,retstep=False)\n",
    "    fx = np.zeros(len(x), dtype=complex)\n",
    "    \n",
    "    for n,xx in enumerate(x):\n",
    "        fx[n] = inverse_fourier_single_x(k,fk,xx)\n",
    "    \n",
    "    return x, fx\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d409970",
   "metadata": {},
   "source": [
    "### Fourier transform of a Gaussian.\n",
    "\n",
    "Let us sample the Gaussian defined above and Fourier-transform it. Change the `sigma` variable to see what happens when the Gaussian has a different width. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab707329",
   "metadata": {},
   "outputs": [],
   "source": [
    "xmin = -10  # start of x-interval\n",
    "xmax = 10   # end of x-interval (excluding xmax)\n",
    "kmin = -10  # start of k-interval\n",
    "kmax = 10   # end of k-interval (excluding kmax)\n",
    "Nx = 100    # number of points in [xmin, xmax)\n",
    "Nk = 100    # number of points in [kmin, kmax)\n",
    "\n",
    "# sample a Gaussian and Fourier transform:\n",
    "sigma = 3\n",
    "x, fx = sample_func(function1, xmin, xmax, Nx, {\"sigma\":sigma})\n",
    "k, fk = fourier(x, fx, kmin, kmax, Nk)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3db5c2f",
   "metadata": {},
   "source": [
    "And plot the result. Shown in lower panel of the plot below is also the analytic result (orange) of the Fourier transform."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1f4afac",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "fig, ax = plt.subplots(2, figsize=(10,6))    \n",
    "fig.tight_layout(pad=5.0) \n",
    "\n",
    "mf = 16\n",
    "ax[0].plot(x,fx, color=\"black\", marker=\".\", linewidth=0, label=r'$e^{\\frac{-x^2}{2\\sigma^2}}$')\n",
    "\n",
    "    \n",
    "ax[0].set_xlabel(r'$x$', fontsize=mf)\n",
    "ax[0].set_ylabel(r'$f(x)$', fontsize=mf)\n",
    "ax[0].legend(fontsize=mf)\n",
    "\n",
    "\n",
    "ax[1].plot(k,fk.real, color=\"black\", marker=\".\", linewidth=0, label=r'$\\mathcal{F}\\mathrm{~} e^{\\frac{-x^2}{2\\sigma^2}}$')\n",
    "\n",
    "ax[1].plot(k,np.exp(-k**2*sigma**2/2)*sigma, label=r'$\\sigma e^{\\frac{-k^2\\sigma^2}{2}}$', color='orange')\n",
    "    \n",
    "ax[1].set_xlabel(r'$k$', fontsize=mf)\n",
    "ax[1].set_ylabel(r'$f(k)$', fontsize=mf)\n",
    "ax[1].legend(fontsize=mf)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4d1eb33",
   "metadata": {},
   "source": [
    "## Derivatives with Fourier transforms\n",
    "\n",
    "We have seen in the lecture that we can calculate the derivative of a function using the Fourier transform as\n",
    "\n",
    "\\begin{align}\n",
    "f'(x) & = \\frac1{\\sqrt{2\\pi}}\\int\\limits_{-\\infty}^{\\infty}  (ik) f(k)e^{ikx} dk\n",
    "\\end{align}\n",
    "\n",
    "or more general for the $n$-th derivative:\n",
    "\n",
    "\\begin{align}\n",
    "f^{(n)}(x) & = \\frac1{\\sqrt{2\\pi}}\\int\\limits_{-\\infty}^{\\infty}  (ik)^n f(k)e^{ikx} dk\n",
    "\\end{align}\n",
    "\n",
    "Let us test this with the Gaussian from above. The function below calculates the $n$-th derivative of a function.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93331398",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fourier_derivative(x, fx, kmin, kmax, Nk, n):\n",
    "    \"\"\"\n",
    "    Calculate the derivative of a function f(x) using \n",
    "    a Fourier transform, i.e., calculate \n",
    "    \n",
    "    F^-1 (ik)^n F f(x)\n",
    "    \n",
    "    x: points in coordinate domain (array, equidistant)\n",
    "    fx: f evaluated on points x (array)\n",
    "    kmin: start if k-interval\n",
    "    kmax: end of k-interval (excluding kmax)\n",
    "    NK: number of points in [kmin, kmax)\n",
    "    n: n-th derivative\n",
    "    \n",
    "    returns \n",
    "    \n",
    "    x: points in x-domain (array, equidistant, same as input)\n",
    "    fxn: f^(n)(x) the n-th derivative on points x\n",
    "    \"\"\"\n",
    "    \n",
    "    k, fk = fourier(x, fx, kmin, kmax, Nk)\n",
    "    fkn = (1j*k)**n*fk\n",
    "    dx = x[1] - x[0]\n",
    "    Nx = len(x)\n",
    "    x, fxn = inverse_fourier(k,fkn, x[0], x[-1] + dx, Nx)\n",
    "    return x, fxn\n",
    "        \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cedd665",
   "metadata": {},
   "source": [
    "Let us calculate the first two derivatives of the Gaussian: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f824217c",
   "metadata": {},
   "outputs": [],
   "source": [
    "sigma=2\n",
    "x, fx0 = sample_func(function1,xmin, xmax, Nx, {\"sigma\":sigma})\n",
    "x, fx1 = fourier_derivative(x, fx0 ,kmin, kmax, Nk, n=1)\n",
    "x, fx2 = fourier_derivative(x, fx0 ,kmin, kmax, Nk, n=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cbc47c99",
   "metadata": {},
   "source": [
    "And plot. The dotted lines are the numerical results, the solid lines are the analytical results from the functions above.   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eca296f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(1, figsize=(8,5))    \n",
    "       \n",
    "mf = 16\n",
    "ax.plot(x,fx0.real, color=\"blue\", marker=\".\", linewidth=0, \n",
    "        label=r'$e^{\\frac{-x^2}{2\\sigma}}$  using $\\mathcal{F}$')\n",
    "ax.plot(x,fx1.real, color=\"red\", marker=\".\", linewidth=0, \n",
    "        label=r'$\\frac{\\partial}{\\partial x}e^{\\frac{-x^2}{2\\sigma}}$ using $\\mathcal{F}$')\n",
    "ax.plot(x,fx2.real, color=\"black\", marker=\".\", linewidth=0, \n",
    "        label=r'$\\frac{\\partial^2}{\\partial x^2}e^{\\frac{-x^2}{2\\sigma}}$ using $\\mathcal{F}$')\n",
    "\n",
    "ax.plot(x,function1(x, sigma), color=\"blue\", marker=\"\", linewidth=1, label=r'$e^{\\frac{-x^2}{2\\sigma}}$ analytic')\n",
    "ax.plot(x,dfunction1(x, sigma), color=\"red\", marker=\"\", linewidth=1, label=r'$\\frac{\\partial}{\\partial x}e^{\\frac{-x^2}{2\\sigma}}$  analytic')\n",
    "ax.plot(x,ddfunction1(x, sigma), color=\"black\", marker=\"\", linewidth=1, label=r'$\\frac{\\partial}{\\partial x}e^{\\frac{-x^2}{2\\sigma}}$  analytic')\n",
    "\n",
    "\n",
    "ax.set_xlabel(r'$k$', fontsize=mf)\n",
    "ax.set_ylabel(r'$f(k)$', fontsize=mf)\n",
    "ax.legend(fontsize=mf, bbox_to_anchor=(1.1, 1.00))\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d7940faa",
   "metadata": {},
   "source": [
    "## Spectral analysis with Fourier transform\n",
    "\n",
    "Often a signal in time-domain may look very complicated but really consists of a only a few plane waves, with various frequencies and different amplitudes (for instance, a shaped laser pulse, or a dipole-dipole correlation function that one obtains in time-dependent spectroscopy). Knowing the frequencies and amplitudes the signal is composed of may hence be very insightful. \n",
    "\n",
    "The code below will create a signal in time-domain with $N_f$ random frequencies $\\omega_n$ (the frequencies will change every time you run the code). It will also generate a random coefficient $d_n$ and create a signal of the form \n",
    "\n",
    "$$ f(t) = \\sin\\left(\\frac{\\pi t}{T}\\right)\\sum_{n=1}^{N_f} d_n e^{i \\omega_n t} $$\n",
    "\n",
    "for $0\\le t < T$ and $f(t) = 0$ elsewhere. \n",
    "\n",
    "The Fourier transform should extract is exactly the $d_n$ and $\\omega_n$. (If the signal was infinitely log we would obtain delta functions at $\\omega_n$, but since the signal has a $\\sin$ envelope we will obtain more or less sharp peaks at $\\omega_n$ where the width of the peaks is correlates to $1/T$.)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "511dbe3b",
   "metadata": {},
   "outputs": [],
   "source": [
    "def function2(t, T, d, omega):\n",
    "    f = 0.0*t\n",
    "    for n, dd in enumerate(d):\n",
    "      f = f + dd*np.exp(1j*omega[n]*t) \n",
    "    f = f * np.sin(t*np.pi/T)**2\n",
    "    return f\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bd280cd",
   "metadata": {},
   "source": [
    "Set up time domain and sampling, calculate Fourier Transform:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca2738fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 500     # interval: [0,T)\n",
    "Nt = 10000   # no. of points in [0,T)\n",
    "Nf = 10      # no. of frequencies\n",
    "wmin = 0.0   # min frequency\n",
    "wmax = 5.0   # max frequency \n",
    "Nw = 10000   # no. points in [wmin,wmax)\n",
    "\n",
    "# random coefficients\n",
    "d = np.random.random(Nf)\n",
    "#random frerquencies \n",
    "omega = np.random.random(Nf)*(wmax-0.1)\n",
    "\n",
    "# sample in time-domain\n",
    "t, ft = sample_func(function2, 0, T, Nt, {\"T\":T, \"d\":d, \"omega\":omega})\n",
    "# Fourier transform\n",
    "w, fw = fourier(t, ft, wmin, wmax, Nw)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e89083cc",
   "metadata": {},
   "source": [
    "And plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4227a8da",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2, figsize=(10,7))    \n",
    "fig.tight_layout(pad=5.0)\n",
    "\n",
    "mf = 16\n",
    "ax[0].plot(t,ft.real, color=\"black\", marker=\"\", linewidth=1, label=r'$f(t)$')\n",
    "\n",
    "\n",
    "ax[0].set_xlabel(r'$t$', fontsize=mf)\n",
    "ax[0].set_ylabel(r'$f(t)$', fontsize=mf)\n",
    "ax[0].legend(fontsize=mf)\n",
    "ax[0].set_title('time domain')\n",
    "ax[1].plot(omega, d/np.absolute(d).max(), color=\"red\", marker=\"o\",linewidth=0, label=\"$(\\omega_n,d_n)$\")\n",
    "ax[1].plot(w, np.absolute(fw)/np.absolute(fw).max(), color=\"black\", marker=\"\", linewidth=1, label=r'$\\mathcal{F}\\mathrm{~} f(t)$')\n",
    "\n",
    "ax[1].set_title('Frequency domain (scaled: max=1)')\n",
    "ax[1].set_xlabel(r'$\\omega$', fontsize=mf)\n",
    "ax[1].set_ylabel(r'$f(\\omega)$', fontsize=mf)\n",
    "ax[1].legend(fontsize=mf)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f47a8d4c",
   "metadata": {},
   "source": [
    "# The delta distribution\n",
    "\n",
    "\n",
    "The delta distribution can be obtained as the limiting case of\n",
    "\n",
    "$$\\delta(x) = \\lim_{\\epsilon\\to 0} \\frac{\\epsilon}{x^2 + \\epsilon}$$\n",
    "\n",
    "Let us test this numerically. The code below shows the function \n",
    "\n",
    "$$f_\\epsilon(x) = \\frac{\\epsilon}{x^2 + \\epsilon}$$\n",
    "\n",
    "which parametrically depends on epsilon. Use the slider to plot $f_\\epsilon(x)$ for different $\\epsilon$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b4f7117",
   "metadata": {},
   "outputs": [],
   "source": [
    "def delta_epsilon(eps, xmin, xmax, Nx):\n",
    "    \"\"\"\n",
    "    Sample  epsilon/(x^2 + epsilon^2) in coordinate domain\n",
    "    in interval [xmin, xmax]\n",
    "    \n",
    "    eps  : epsilon\n",
    "    xmin : start if interval\n",
    "    xmax : end of interval\n",
    "    Nx   : number of points in [xmin, xmax]\n",
    "    \"\"\"\n",
    "    \n",
    "    \n",
    "    x = np.linspace(xmin, xmax, Nx, endpoint=True)\n",
    "    delta = eps/(x**2 + eps**2)\n",
    "    return x, delta\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "346e6bab",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_delta(eps, xmin, xmax, Nx):\n",
    "    \"\"\"\n",
    "    Plot  epsilon/(x^2 + epsilon^2) in coordinate domain\n",
    "    in interval [xmin, xmax]\n",
    "    \n",
    "    eps  : epsilon\n",
    "    xmin : start if interval\n",
    "    xmax : end of interval\n",
    "    Nx   : number of points in [xmin, xmax]\n",
    "    \"\"\"\n",
    "    \n",
    "    x, delta = delta_epsilon(2**eps, xmin, xmax, Nx)\n",
    "    \n",
    "    \n",
    "    mf = 16\n",
    "    fig, ax = plt.subplots(1,1, figsize=(12,5))    \n",
    "    \n",
    "    ax.plot(x, delta, color=\"red\", marker=\".\", linewidth=1, label=r\"$\\frac{\\epsilon}{x^2 + \\epsilon^2}$\")\n",
    "    \n",
    "    ax.set_title(r'$\\epsilon=$' + '2^{:d}'.format(eps))\n",
    "    ax.set_xlabel(r'y', fontsize=mf)\n",
    "    ax.set_ylabel(r'x', fontsize=mf)\n",
    "    ax.legend(fontsize=20)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d748f7bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "xmin = -1.0  # start of x-interval \n",
    "xmax = 1.0   # end of x-interval\n",
    "Nx = 10001   # number of points in  x-interval. use odd number to get the point at x=0\n",
    "\n",
    "widgets.interact(plot_delta, eps=widgets.IntSlider(min=-20, max=0, step=1, value=0),\n",
    "         xmin=widgets.fixed(xmin), \n",
    "         xmax=widgets.fixed(xmax),\n",
    "         Nx=widgets.fixed(Nx))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce0fc3f8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
