# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Autorrelation function and power spectrum of the wavefunction

# %%
import numpy as np
import scipy.integrate as spi

import matplotlib.pyplot as plt
from matplotlib import rc

# %% [markdown]
# ## Theoretical background
#
#  We start introducing the exponential representation of the delta function,
# 
#  $$
#     \delta(x-x^\prime) = \frac{1}{2\pi} \int_{-\infty}^{\infty} e^{i k (x-x^\prime)} dk
#  $$
# 
# and the Fourier Transform pairs,
#
# - Forward Fourier transform:
#
#  \begin{align*}
#     f(t) = \int_{-\infty}^{\infty} F(\omega) e^{-i \omega t} d\omega
#  \end{align*} 
# 
# - Inverse Fourier transform:
#
#  \begin{align*}
#     F(\omega) = \frac{1}{2\pi} \int_{-\infty}^{\infty} f(t) e^{i \omega t} dt
#  \end{align*} 
# 
# We now consider the wavapacket:
#
# \begin{align*}
#     |\Psi(t)\rangle = \sum_l C_l(0) \exp{(-i\, \omega_l\, t)}\; |\Psi_l\rangle
# \end{align*}
#
# with $\hat{H}|\Psi_l\rangle = E_l |\Psi_l\rangle$ and $\omega_l = E_l/\hbar$.
#
# The autocorrelation function of the wavepacket,
# $a(t) = \langle \Psi(0) | \Psi(t) \rangle $ takes the following form as a function of the angular frequencies:
#
# \begin{align*}
#     a(t) & = \langle \Psi(0) | \Psi(t) \rangle \\
#          & = \sum_l |C_l(0)|^2 \exp{-i \, \omega_l \, t}.
# \end{align*}
#
# The power spectrum of the wavepacket is defined as the (inverse) Fourier
# transform of $a(t)$,
# namely the population of each state at the correpsonding angular frequency,
# 
# \begin{align*}
# \sigma(\omega) & = \frac{1}{2\pi} \int_{-\infty}^{\infty} e^{i \omega t} a(t) dt \\
#                & = \sum_l |C_l(0)|^2 \delta(\omega - \omega_l)
# \end{align*}

# %%
omegas = np.array([1, 2, 3], complex)

def normalize(c0):
    return c0/np.sqrt(np.vdot(c0, c0))

C0 = np.array([1, 1, 1], complex)   # Raw amplitudes 
nC0 = normalize(C0)                 # Normalized amplitudes
pC0 = np.conj(nC0)*nC0              # Normalized populations

# %% [markdown]
# Introduce a discrete time-grid:

# %%
t0 = 0           # Initial time
tf = 40          # Final time
Nt = 400          # Number of time samples 
t = np.linspace(t0, tf, Nt)
dt = t[1] - t[0] # Discrete time-step


# %%
def genauto(omegas, populations, t):
    Nt = len(t)
    auto = np.zeros(Nt, complex)
    for l in range(len(omegas)):
        auto = auto + populations[l]*np.exp(-1j*omegas[l]*t)
    return auto

auto = genauto(omegas, pC0, t)

    # %%
plt.plot(t, np.real(auto))
plt.plot(t, np.imag(auto))
plt.plot(t, np.abs(auto))


# %% [markdown]
# Finally, starting from the power spectrum again
#
# \begin{align*}
# \sigma(\omega) & = \frac{1}{2\pi} \int_{-\infty}^{\infty} e^{i \omega t} a(t) dt
# \end{align*}
#
# and using $a(0)\in \Re$, and hence $a(t)=a^*(-t)$, we see
# that we need to consider only the autocorrelation at
# positive times:
#
# \begin{align*}
#     \sigma(\omega) = \frac{1}{\pi}\Re\{ \int_0^\infty a(t) \exp{(i\,\omega \, t)}\; dt \}
# \end{align*}

# %%
def sigma(omg, a, t, dt):
    s = spi.trapezoid(a*np.exp(1j*omg*t), t, dt)
    return np.real(s)/np.pi


# %% [markdown]
# We introduce a discrete frequency grid where the power spectrum is computed. Since time and frequency are a Fourier pair, the maximum frequency contained in the time time signal is given by the inverse of the time-step,
#
# $$
# \omega_\text{max}=\frac{2\pi}{\Delta t}.
# $$
#
# The sampled frequency interval is correspondingly given by the inverse of the maximum propagation time,
#
# $$
# \Delta \omega=\frac{2\pi}{t_f}.
# $$
#
# Hence, if high-frequency resolution is required in the spectrum, one needs to propagate for a longer time than than if a low-frequency spectrum suffices.
#
# Often $\omega_\text{max}$ and $\Delta\omega$ are chosen to get an adequate graphical representation of the spectrum, but it is important to realize that these parameters can only be meaningfully chosen in the ranges given by the above expressions.

# %%
def power_spectrum(auto, t, w0, wf, dw):
    dt = t[1] - t[0]
    w = np.arange(w0, wf, dw)    # Generate frequency grid
    s = np.zeros(len(w), float)  # sigmas
    for l, iw in enumerate(w):
        s[l] = sigma(iw, auto, t, dt)
    return w, s


# %%
w0 = 0.0
#wf = 2*np.pi/dt
wf = 6.0
#dw = 2*np.pi/tf
dw = 0.02

w, s = power_spectrum(auto, t, t0, wf, dw)

# %%
plt.plot(w, s)
plt.vlines(omegas, 0, np.real(pC0), color='r')

# %%
# Check that the power spectrum integrates to one:
S = spi.trapezoid(s, w, w[1]-w[0])
print(S)

# %% [markdown]
# ## Example: vibronic progression

# %%
omegas = np.arange(0.5, 10, 1)
t0 = 0           # Initial time
tf = 40          # Final time
Nt = 400          # Number of time samples 
t = np.linspace(t0, tf, Nt)
dt = t[1] - t[0] # Discrete time-step

# %%
C0 = np.exp(-0.1*(omegas[3] - omegas)**2)
nC0 = normalize(C0)
pC0 = np.conj(nC0)*nC0
plt.vlines(omegas, 0, np.real(pC0), color='r')

# %%
auto = genauto(omegas, pC0, t)#*np.cos(t*np.pi/(2*tf))
plt.plot(t, np.abs(auto))

# %%
w, s = power_spectrum(auto, t, t0, 10.0, 0.01)
plt.plot(w, s)
plt.vlines(omegas, 0, np.real(pC0)*10, color='r')


# %% [markdown]
# The highly oscillatory signal is caused by the abrupt end of the
# autocorrelation function and is known as Gibbs phenomenon. It can be mitigated
# by multiplying the autocorrelation by a filter function that reaches zero at
# the end of the time domain, for example the cosine filter
#
# $$
# g(t) = \cos(\frac{t\, \pi}{t_f\, 2})^n
# $$
#
# where $n=0,1,2,\ldots$.

# %%
auto1 = genauto(omegas, pC0, t)*np.cos(t*np.pi/(2*tf))
auto2 = genauto(omegas, pC0, t)*np.cos(t*np.pi/(2*tf))**2

plt.plot(t, np.abs(auto))
plt.plot(t, np.abs(auto1))
plt.plot(t, np.abs(auto2))

# %%
w, s1 = power_spectrum(auto1, t, t0, 10.0, 0.01)
w, s2 = power_spectrum(auto2, t, t0, 10.0, 0.01)

plt.plot(w, s)
plt.plot(w, s1)
plt.plot(w, s2)

# %% [markdown]
# ## Example: dense finite random spectrum
#
# We generate the spectrum of a system with $N$ states with random energies
# (frequencies) in the range 0 to $w_\text{cut}$

# %%
N = 10000
wc = 10
omegas = np.random.random(N)*wc
C0 = np.random.random(N) + np.random.random(N)*1j
nC0 = normalize(C0)
pC0 = np.conj(nC0)*nC0

t0 = 0           # Initial time
tf = 20         # Final time
Nt = 500        # Number of time samples 
t = np.linspace(t0, tf, Nt)
dt = t[1] - t[0] # Discrete time-step

# %%
plt.vlines(omegas, 0, np.real(pC0), color='r')

# %% [markdown]
# The autocorrelation now falls very quickly, roughly in the time scale of the
# inverse energy range of the spectrum, and it is not periodic (or the period is
# astronomically long)

# %%
auto = genauto(omegas, pC0, t)*np.cos(t*np.pi/(2*tf))
plt.plot(t, np.abs(auto))

# %%
w, s = power_spectrum(auto, t, t0, 10.0, 0.01)
plt.plot(w, s)
plt.vlines(omegas, 0, np.real(pC0)*10, color='r')

# %% [markdown]
# ## Example: dissipative or irreversible system, e.g. dissociation
#
# In these cases the autocorrelation function decays to zero due to the very
# high density of states

# %%
omegas = np.arange(0.5, 10, 1)
t0 = 0           # Initial time
tf = 80          # Final time
Nt = 400          # Number of time samples 
t = np.linspace(t0, tf, Nt)
dt = t[1] - t[0] # Discrete time-step

# %%
C0 = np.exp(-0.1*(omegas[3] - omegas)**2)
nC0 = normalize(C0)
pC0 = np.conj(nC0)*nC0

# %%
auto = genauto(omegas, pC0, t)
tau = 8
decay = np.exp(-t/tau)
autodec = auto*decay

# %%
plt.plot(t, np.abs(auto))
plt.plot(t, decay)
plt.plot(t, np.abs(autodec))

# %%
w, s = power_spectrum(auto, t, t0, 10.0, 0.01)
w, sd = power_spectrum(autodec, t, t0, 10.0, 0.01)
plt.plot(w, sd)

# %% [markdown]
# In this case, when the autocorrelation truly terminates, the spectrum does not
# change with an increased propagation time. Hence, although the number of
# eigenstates under the spectrum is in principle infinite, a finite time
# propagation delivers all information about the power spectrum.
