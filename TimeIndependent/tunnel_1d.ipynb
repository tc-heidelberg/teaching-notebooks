{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2da5f41c",
   "metadata": {},
   "source": [
    "# Tunnel through a 1D barrier"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d51bd8b7",
   "metadata": {},
   "source": [
    "Copyright (C) 2022, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/4.0/80x15.png\" /></a><br />This work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 International License</a>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a11f4eb9",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import rc\n",
    "plt.rcParams['figure.figsize'] = [10, 6]\n",
    "plt.rcParams['figure.dpi'] = 100\n",
    "MEDIUM_SIZE = 10\n",
    "plt.rc('axes', labelsize=MEDIUM_SIZE)\n",
    "\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual\n",
    "import ipywidgets as widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0737745d",
   "metadata": {},
   "source": [
    "## Time-Independent Schrödinger Equation for Unbound States in 1D\n",
    "\n",
    "We consider the TISE for the case of a particle colliding with a square potential energy barrier of height $V$ and width $L$. The particle, for example an electron, has total energy $E$ and arrives at the barrier propagating from left to right, i.e. with a positive momentum.\n",
    "\n",
    "The wavefunction before (region I), in (region II), and after (region III) the barrier takes the form:\n",
    "\n",
    "\\begin{align}\n",
    "    \\psi_{I} & = A e^{i k x} + B e^{-i k x}        & \\\\\n",
    "    \\psi_{II} & = C e^{i \\beta x} + D e^{-i \\beta x} & \\textrm{for}~E>V \\\\\n",
    "    \\psi_{II} & = C e^{\\beta x} + D e^{-\\beta x}     & \\textrm{for}~V>E \\\\\n",
    "    \\psi_{III} & = F e^{i k x} + G e^{-i k x}        &\n",
    "\\end{align}\n",
    "\n",
    "The conditions of continuity of the wavefunction and of its first derivative at $x=0$ (start of the barrier)\n",
    "and $x=L$ (end of the barrier)\n",
    "\n",
    "\\begin{align}\n",
    "    A + B      & = C + D \\\\\n",
    "    ikA - ikB  & = \\beta C - \\beta D\n",
    "\\end{align}\n",
    "\n",
    "can be written in matrix form:\n",
    "$$\n",
    "    \\begin{pmatrix}\n",
    "    1 & 1 \\\\\n",
    "    ik & -ik\n",
    "    \\end{pmatrix}\n",
    "    \\begin{pmatrix}\n",
    "    A \\\\ B\n",
    "    \\end{pmatrix}\n",
    "    =\n",
    "    \\begin{pmatrix}\n",
    "    1 & 1 \\\\\n",
    "    \\beta & -\\beta\n",
    "    \\end{pmatrix}\n",
    "    \\begin{pmatrix}\n",
    "    C \\\\ D\n",
    "    \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "and correpondingly for the conditions at the end of the barrier (x=L):\n",
    "$$\n",
    "    \\begin{pmatrix}\n",
    "    e^{\\beta L}  & e^{-\\beta L}  \\\\\n",
    "    \\beta e^{\\beta L} & -\\beta e^{-\\beta L}\n",
    "    \\end{pmatrix}\n",
    "    \\begin{pmatrix}\n",
    "    C \\\\ D\n",
    "    \\end{pmatrix}\n",
    "    =\n",
    "    \\begin{pmatrix}\n",
    "    e^{ikL} &  e^{-ikL} \\\\\n",
    "    ike^{ikL} & -ike^{-ikL}\n",
    "    \\end{pmatrix}\n",
    "    \\begin{pmatrix}\n",
    "    F \\\\ G\n",
    "    \\end{pmatrix}.\n",
    "$$\n",
    "\n",
    "The parameters $k$ and $\\beta$ depend on the energy of the particle and height of the barrier\n",
    "\n",
    "\\begin{align}\n",
    "    k & = \\frac{\\sqrt{2 m E}}{\\hbar} \\\\\n",
    "    \\beta & = \\frac{\\sqrt{2 m (V-E)}}{\\hbar}\n",
    "\\end{align}\n",
    "\n",
    "The incoming amplitude $A$ can always be set to 1. The physically relevant parameters are\n",
    "\\begin{align}\n",
    "    T & = \\frac{|F|^2}{|A|^2} & \\text{Transmission coefficient} \\\\\n",
    "    R & = \\frac{|B|^2}{|A|^2} & \\text{Reflection coefficient}\n",
    "\\end{align}\n",
    "where $T + R = 1$.\n",
    "\n",
    "Defining the matrices above as\n",
    "\n",
    "$$\n",
    "    \\begin{pmatrix}\n",
    "    1 & 1 \\\\\n",
    "    ik & -ik\n",
    "    \\end{pmatrix} = \\mathbf{M}_a ;\n",
    "    \\begin{pmatrix}\n",
    "    1 & 1 \\\\\n",
    "    \\beta & -\\beta\n",
    "    \\end{pmatrix} = \\mathbf{M}_b ;\n",
    "    \\begin{pmatrix}\n",
    "    e^{\\beta L}  & e^{-\\beta L}  \\\\\n",
    "    \\beta e^{\\beta L} & -\\beta e^{-\\beta L}\n",
    "    \\end{pmatrix} = \\mathbf{M}_c ;\n",
    "    \\begin{pmatrix}\n",
    "    e^{ikL} &  e^{-ikL} \\\\\n",
    "    ike^{ikL} & -ike^{-ikL}\n",
    "    \\end{pmatrix} = \\mathbf{M}_d\n",
    "$$\n",
    "\n",
    "one can write a relation between the left ($A$, $B$) and right ($F$, $G$) coefficients:\n",
    "\n",
    "\\begin{align}\n",
    "    \\begin{pmatrix}\n",
    "    A \\\\ B\n",
    "    \\end{pmatrix} & =\n",
    "    \\mathbf{M}_a^{-1} \\mathbf{M}_b \\mathbf{M}_c^{-1} \\mathbf{M}_d\n",
    "    \\begin{pmatrix}\n",
    "    F \\\\ G\n",
    "    \\end{pmatrix} \\\\\n",
    "    \\begin{pmatrix}\n",
    "    A \\\\ B\n",
    "    \\end{pmatrix} & =\n",
    "    \\mathbf{T}\n",
    "    \\begin{pmatrix}\n",
    "    F \\\\ G\n",
    "    \\end{pmatrix}\n",
    "\\end{align}\n",
    "\n",
    "The matrix $\\mathbf{T}$ can be numerically evaluated since all parameters of all matrices are known.\n",
    "\n",
    "$G=0$ because there are no particles propagating towards the barrier from the right."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0f1be8da",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tunnel(E, V=1.0, L=3.0, m=1.0):\n",
    "    \"\"\"\n",
    "    Calculate transmission and reflection probability in a 1D square barrier\n",
    "    \n",
    "    E - particle's energy\n",
    "    V - barrier height\n",
    "    L - barrier width\n",
    "    m - particle's mass\n",
    "    \n",
    "    All parameters in atomic units\n",
    "    \"\"\"\n",
    "    kappa = 1j*np.sqrt(2*m*E)\n",
    "    if E > V:\n",
    "        beta = 1j*np.sqrt(2*m*(E-V))\n",
    "    else:\n",
    "        beta = np.sqrt(2*m*(V-E))\n",
    "    expb = np.exp(beta*L)\n",
    "    expk = np.exp(kappa*L)\n",
    "    Ma = np.array([[1, 1],[kappa, -kappa]])\n",
    "    Mb = np.array([[1, 1],[beta, -beta]])\n",
    "    Mc = np.array([[expb, 1/expb],[beta*expb, -beta/expb]])\n",
    "    Md = np.array([[expk, 1/expk], [kappa*expk, -kappa/expk]])\n",
    "    iMa = np.linalg.inv(Ma)\n",
    "    iMc = np.linalg.inv(Mc)\n",
    "    t1 = np.dot(iMa, Mb)\n",
    "    t2 = np.dot(iMc, Md)\n",
    "    T = np.dot(t1, t2)\n",
    "    Trans = np.abs(1/T[0, 0])**2\n",
    "    Reflx = np.abs(T[1, 0]/T[0, 0])**2\n",
    "    return Trans, Reflx"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "487f7bcc",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_transmission(V=1.0, L=3.0, m=1.0):\n",
    "    Np = 2002\n",
    "    Elist = np.linspace(0.02, 7, Np)\n",
    "    Tlist = np.zeros(Np, float)\n",
    "    for p, e in enumerate(Elist):\n",
    "        T, R = tunnel(e, V=V, L=L, m=m)\n",
    "        Tlist[p] = T\n",
    "        plt.xlabel('E')\n",
    "        plt.ylabel('T')\n",
    "    plt.plot(Elist, Tlist)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "507050d7",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA04AAAINCAYAAAAJGy/3AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjguMCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy81sbWrAAAACXBIWXMAAA9hAAAPYQGoP6dpAABLu0lEQVR4nO3deXyU9bn///fsWSchKwmEXZBFUaAqIK24oOjx1HN6Kq1WtGpPObX1IG1PS/22p3o85ZwulvZYqNSttrbya6vWWqpGq2hBqyC4sCh7WBKyQDKTbSYzc//+mMwkIUASMjP3ZOb1fDzmkZk7s1whBOad6/O5bothGIYAAAAAAKdkNbsAAAAAAEh2BCcAAAAA6APBCQAAAAD6QHACAAAAgD4QnAAAAACgDwQnAAAAAOgDwQkAAAAA+kBwAgAAAIA+2M0uINFCoZCOHDmi3NxcWSwWs8sBAAAAYBLDMOT1elVeXi6r9fQ9pbQLTkeOHFFFRYXZZQAAAABIEgcPHtTIkSNPe5+0C065ubmSwn84brfb5GoAAAAAmMXj8aiioiKaEU4n7YJTZHme2+0mOAEAAADo1xYehkMAAAAAQB8ITgAAAADQB4ITAAAAAPSB4AQAAAAAfSA4AQAAAEAfCE4AAAAA0AeCEwAAAAD0geAEAAAAAH0gOAEAAABAHwhOAAAAANAHghMAAAAA9IHgBAAAAAB9IDgBAAAAQB9MDU6vvfaarr32WpWXl8tiseiZZ57p8zHr16/XzJkzlZGRoXHjxunnP/95/AsFAAAAkNZMDU4tLS2aPn26HnjggX7df9++fbr66qs1b948bdmyRd/61rd055136g9/+EOcKwUAAACQzuxmvvjChQu1cOHCft//5z//uUaNGqWVK1dKkiZPnqxNmzbphz/8oT71qU/FqUoAAAAA6c7U4DRQb7zxhhYsWNDj2JVXXqmHH35YHR0dcjgcJlUGnLn2jqDe3n9M44pzNCI/0+xyUlogGNKRxnYdbmxTe0dQHcGQcjLsGpbl1KiCLGW7htQ/iUgChmGo1R9UU1uH2jqC8nWE5AsE5Q+E5Ou8SJJFktUqWWSRxSJZLBY5bBZlOmzKdNrCH7tdt9vYgozUZRiGAiFD/kBIHcFQ9OfFHwzf7ggYChmGgoYhwzAUDEkhI3ws1Hk98rlQSNHr6vz5slossnZ+1Am3LQr//FktXR+tVoscVqvstvDPpcNmld1mlcNqkd0WPu60WWW3WmSzWmSxWMz+I4RJhtS7hJqaGpWWlvY4VlpaqkAgoPr6epWVlfV6jM/nk8/ni972eDxxrxPor1Z/QJ9Z86beO9Qkp82qNYtn6pJJJWaXlTIMw9B7h5r04vYabdzToPcPNSkQMk55/xH5mZpc5tZF4wo0Z3yRJpfl8h9kmvIHQjrc2KbqpjYd9bSruqldNZ2X+mafGts65GnrUGNrx2n/Tp0pp92q/EyH8rMcys90yh297lBepkP52U4V57hUnOuKfsx02mJeB9KXYRhq6wj/UqCxtUNNbR1qbg+otSOoNn9Arf6gWv1BtXV+bO1+rCMQ/VxbR/gXCdFLMHwxYv9jkzDRcGWNhKzwR6fdKpfd1vmx6xI9brPK5bB2+2g74bZVLoet22Mil67nPPE17AS5hBpSwUlSr78cRudP3qn+0qxYsUL33HNP3OsCzsRPXtql9w41SZL8wZC+8tstWv/1+SrIdppc2dDWEQzpd5sO6fE39mtnjbfH55x2q0bmZyrLZZPdalWzL6CGZp+Ot3bocGObDje26aUdRyWFg9TV5wzXNeeWa/rIPP5zSkGNrX5tr/ZoT12L9tW1aF99s/bVt+jg8TYFBxCIHDaLspz2k765sSj8f5Wh8G/KDUMKGeG/p+0dXW8u2zqC0TeT/kBItV6far2+071sDzkuu4pynCrOdanohFBV4napOCdDJW6XCrOddLTSiC8QDj+eto4eISjy8cRLY6tfTW0BNbX51RFMTLqxWsL/Njts4Z8fh80a7hJZJZvFEu4UWSSbNXLdIptV0evR7pK6fsYMw1DIkAx1dakinw91fj78sxjuXgWChjqChgKhkDoCIXWEDAWCIZ3sn4GOoKGOYDAhfzZ9sVgkpy0cuiJ/hk77Cddtls6P3Y+d/P7hP39L5+dt4eud97d3hkWr1RLtvkUu9s7vjd3W7brVKqtVslutve5ri94n/HxDxZAKTsOHD1dNTU2PY7W1tbLb7SosLDzpY5YvX65ly5ZFb3s8HlVUVMS1TqA/WnwB/eatKknS6htn6P/+ulvbqz1a/epu3X3NFJOrG5oMw9Bz71XrBy98qKpjrZIkl92qBVOH6+NnFemicYUakZ950n+kj7f49dFRr7YebNTGPQ16a98xHW5s0y9e36dfvL5Pk0pzdeNFo3Td+SPkzmBZ8FBU09Sudw81atsRj7Yf8WhHtUeHG9tOef9Mh01l+Rkqy8vQcHemhue5NDwvU8U5rnD3J6uz+5PpVIbDOuhgbRiGfIFwmPK2B7q9ke1QY5s/fLs1fPtYq1/1zT7VecMXXyCkZl9Azb6A9je0nvZ1LBapMLszUHVeotfdGd1uZ9DFShLBkNEVfLoHnVZ/j78nkc93D0ltHYN7g++wWZSX6ZA706HcDIeyHDZlOcPLSrOcNmU57eHrjsgxe/Tz2U67Mp3hzooz8oY88osFm00OuyX6hjxZhUKGOkKhcKgKhrqFq/DxcOA6YclhdOlhePmuPxjq9jEoX4/bXcd73O6+5PeE+3QPtIahrmXB/f89S9J46ktzNGPUMLPL6LchFZxmz56tP/3pTz2Ovfjii5o1a9Yp9ze5XC65XK5ElAcMyF931srbHtDowixdOXW4XA6rbn1sk9a+fVBfXTBJGQ7esAxEradd33r6g2i3qCjHpX+7ZLz+ZeZI5WX2HXSGZTt14bhCXTiuUF/8xHi1dwT16oe1eu69ar2046g+POrVd/64TSvW7dQ/zRihf503TmOKsuP9ZeEMBYIh7aj2avOBY9pc1ah3Dhw/ZUgaVZCls0pyNK44W2OLcjS2KFvjirNVkutKaJfRYrEow2FThsOm/Cyn+vsrPsMw1OwLqM7rU32zvzNMtXddb/ap1tuuWo9P9c0+hQypvjl8fUf16Z8712VXsTvcuYqEqu7BqqTzc/lZDjqyfQiFDHl9gWio8ZwQgiLBJ3zc3+OYtz0wqNe2WCR3RlfY737peczZ61iW05bW31ur1SKX1aZk2gIbChnRkOULBjv3inXtGYuEt0iY6+hcHhldKtntc/7Ox514f18w3HmLPi4QUtAwFAyFu3PBkNF1OxRSKCQFQqHw8VB4D1vkY6jz48lYh9jfLVP/GjQ3N2v37t3R2/v27dPWrVtVUFCgUaNGafny5Tp8+LAef/xxSdKSJUv0wAMPaNmyZfrCF76gN954Qw8//LB++9vfmvUlAGfs1Q/rJElXTh0uq9WiSyaWaOSwTB063qa/fFCtfzp/pMkVDh0bd9frjt+8o+OtHXLYLLpj/gT968fHKct55v/EZThsumpama6aVqamtg49/c4hPfH3Ku2qbdZv/l6lJ9+q0sJpZVryifE6Z2ReDL8anIlgyND2Ix5t2FOvDbvrtWn/8V6/abdapEnD3ZpW7taUcremlLk1udw95DuIFotFuRnhbsC44tPfNxgydKzFHw5S3q6OVa2n63Z4iWC72jtC8voC8tYFtLeu5bTP67RZw0sDO5cJdn/jHbnujtzO7LrtSOJOQ3fBkNFjD0+LL3y92RcJO90CUXv4duR6JAx5fYFB7+vJdoZDdfjP0q78bkHHfcKfd+RzeZkO5WbYh9RyKJye1WpRhtXW+QvWofHvV2TpZFewCsmQlDXEfklsMQzztue9+uqrmj9/fq/jN998sx577DHdcsst2r9/v1599dXo59avX6+77rpL27ZtU3l5ub7xjW9oyZIl/X5Nj8ejvLw8NTU1ye12x+LLAAbMMAxd8L2XVef16Te3X6g5E4okSfdXfqSfvrxLC6aUas3iWSZXOTT8cuN+3fvcdgVDhqaWu/Wj66fr7OHx+dk2DEN/33dMa17bq7/urI0ev3hCkb50yXjNHl+Y1r+ZTbT99S362+56bdxTr417GtTY2tHj8+4Mu2aMHqaZo4Zp5uhhml6Rz+TEfop0sWq9PtV6wkGqrluwqusMV7VeX68/94GITBbMctqV5epc+uUIL/3KdoXfGDqsVjns4f0SDlt4ypmj28Qzh80S3tOizr0tUo/bka8nEDK6fsseiCyHOvG376HosIMWf0CtvvDH9o5QbP5gFV4+7O4MkN1DZf5Juj15WV1hcygFTWAoGUg2MDU4mYHghGRw8Fir5n3/FdmtFn1wz5XRZXnbj3h09U9fV4bDqne+fcWgOiapzjAM/fDFD/WzV/ZIkv7p/BFa8c/nJGyJ484ajx5cv1fPvnskOkTg/FH5+vL8Cbr07BICVBwEQ4beqTqul7YfVeWOo726IDkuuy4aV6C5E4o0e3yhJpbk8lv2BPAFgqpv9ke7Vg3NncvM2vzRyYPdl6NFui9DkdUiZbvsyu4MetlOezTkuDPtcmeEA4470yF3hj0aitwZXZ0flmEDyWUg2YB3ZYAJIpP0zi7L7fGf6OSyXFUUZOrgsTa99lGdrprWe8Q+wqHpv/+8Qw/9bZ8k6etXTtKXLhmf0LBy9nC3frzoPC27YqIeen2vnnz7oLZUNeq2X27S5DK37pg/XgunlcnGG/dBafEF9PquOlVur9UrH9bqWIs/+jmHzaIZo4Zp7oQizZ1QpOkj85J6k3mqctltGpGfOaDz0AWC4WEWrSeMsm6LdHq6jbKObMjvCIaiXaNAZIN+54Z9S+f5eWTpdq4sqfNj10S2yNSwU00Vc9mtynDYegSjLKdN2a7wR5d98ENAAAxdBCfABO8dapQkTR+Z3+O4xWLRFZOH65EN+7Se4HRKP3zxw2houveTU7V49hjTaqkoyNI9n5ymOy6doIdf36dfv3lAO6o9+vJvtmhc8Uf6t0+M13Xnj2CJzQDUNLXr5Z1H9dL2o9qwp0H+QNcyqbxMhy49u0SXTy7VxycWKXeI709KV3abVflZTuVnmV0JAPQfwQkwwY7OcwtNG9F7qMDFZxXqkQ37tGF3Q6LLGhJ+9cb+6PK8+66bps9dNNrkisJKcjO0/OrJ+rdLxuvRDfv12Mb92lvXoq///j2tfGmXlnxinD49q4JlOidhGIZ2VHv10o6jemnH0WhHNmJUQZaumFKqyyeXataYYYRQAIApCE6ACfbUNkuSxhfn9PrcBWMLZbdaVHWsVQePtaqigF/JRry0/ai+8+w2SdKyKyYmTWjqLj/LqbuumKgvfHycfv3mAT30+j4dbmzTt/+4TT/96259Yd5Y3Xjh6LQfUuAPhPTWvmOq3F6jl3bU9hgVbrFI51Xk6/LJpVowpVQTSnJYHgUAMF16/88NmKC9I6gjTeE3ieOKe58HKMdl13kV+dp04Lg27K7XZy4YlegSk9LeumbdtXarDEO64cJR+sqlE8wu6bRyXHYt+cR43TJnjNa+fVAPrt+jI03t+t66nVr16h59fs5Y3TJnjPKy0mepWWOrX69+WKeXdhzV+g/regwIyHBYdfGEYl0xpUTzzy5RSW6GiZUCANAbwQlIsH31LTKM8F6NwmznSe8zZ0JRODjtaSA4SWr1B7Tk15vl9QX0sTHDdM8/Th0yHYgMh003zxmjz14wSs9sOazV6/doX32LfvzSR/rF63v12QsqdOOFo1P2ZLp76pr18o6jemlHrTYfOB6dQChJRTlOXXZ2qS6fUqqLJxQp08kyRgBA8iI4AQkWGaE8rjj7lG/+544v1E9f3qU39jTIMIwhExLiwTAMLX/qfX10tFnFuS797IYZQ3KPi9Nu1fUfq9CnZo7Un9+v1qpXdmtnjVe/eH2ffvH6Ps2dUKgbLhitK6aUymkfel9fREcwpE37j+vlHUf18s5a7avvOTJ8YmmOLptcqiumlOq8kfmMCwcADBkEJyDB9taF9zeNK+q9vyliekW+nDar6pt9qjrWqtGFqdmN6I8/bj2iP249IpvVop/dMEMl7qG9hMtmtegfp5frH84p0ysf1upXbx7Q+o/qtGF3gzbsblBhtlNXn1OmfzyvXDNHDUv6YGEYhnbXNuv1XfX62+56/X1vg1r8wejnHTaLLhpXqMvOLtFlk0vZswcAGLIITkCC7a3v6jidSobDpmkj3HqnqlGb9h9P2+B0pLFN3/7jB5KkOy89SxeMLTC5otixWi26bHKpLptcqoPHWrX27YNau+mg6rw+/erNA/rVmwdUlpehhdPKdMmkYl0wtiApJvKFQob21jdr0/7jemv/MW3YXa+jHl+P+xRkO3XJpGJdPrlU885iZDgAIDUQnIAEqzrWKkka00cYmjWmIBycDhzXp2aOTERpSSUUMvT1378rb3tA0yvydcf88WaXFDcVBVn62pWT9O+Xn6UNu+v1p3er9eK2GlU3teuRDfv0yIZ9ynBYNXtcoeZOKNL5o4Zpark77kHKMAzVeNq1s9qrbUeatPnAcb1T1aimto4e93PZrbpgbIEunlCki88q0uTh7qTvlAEAMFAEJyDBqjvHLpfnn37J2YxRwyRJ7xw4HveaktETb1Vpw+4GZTis+vH102UfgvuaBsphs+qSSSW6ZFKJ2jumaf1Hdfrrjlqt/6hONZ52vfJhnV75sK7zvhZNLnNrYmmuJpTkaEJxjkYVZqk0N0PuTHu/98WFQoaa2jp0uLFNh463do7Bb9OuWq921njV2NrR6zEZDqumj8zXzNHDNHdCkWaOHpYU3TAAAOKJ4AQkUCAYUo2nXZJUnp952vvOHB0OTh/VetXU1qG8zPRZ7lTradf3/7JTkvSNq87WuJOc7yrVZThsunLqcF05dbgMw9CHR71a/2GdNh04ri1Vx1Xf7Nd7h5p6nSxWCg+iKM5xKcdlV4bTpkyHVTarRR1BQ4FgSIGQoeb2gI63+tXU1qFug+56sVktGl+crbOHu3VeRTgsTSl3D8kBHQAADAbBCUigWq9PISPcLSjOcZ32vsW5Lo0pzNL+hla9U3Vc8yeVJKhK893z3HZ5fQFNH5mnxbPHmF2O6SwWi84e7tbZw936osJL6A4db9O7hxq1p7ZFu+uatbu2WUca29TU1iF/INTjhLL9UZTj1MhhWRpVkKWKgkyNKczW5DK3JpTk0E0CAEAEJyChqjtPfFvqzujXHpCZowu0v6FVm/enT3B65cNa/fm9alkt0n//0zmysVemF4vFooqCrJNOqGvvCKrO61Nds09t/qDa/EG1dgQVChmy2yyyW61y2CzKdtk1LMupYdkO5Wc6h/QIdAAAEoHgBCTQ4cbOZXp5p1+mFzFrzDD94Z1D2pwm+5za/EF9p3OK3ufnjtW0EXkmVzT0ZDhspwxVAADgzPErRiCB+jsYIiKyz2nrwUZ1BENxqytZ/OL1vTp4rE1leRladsVEs8sBAACIIjgBCXSkMziV9TEYImJCcY7cGXa1dQS1s9obz9JMd9TTrtWv7pEkLb96srJdNMQBAEDyIDgBCXSkKbJUr38dJ6vVovMjY8mrUnu53o9e/FBtHUGdPypf155bZnY5AAAAPRCcgASq6QxOw/u5x0nqWq6XyvucPjjcpN9tPiRJ+vY/TOn3OYgAAAASheAEJFB9s0+SVJJ7+lHk3c1I8Y6TYRj67z/vkGFI104vj369AAAAyYTgBCSIYRjR4FQ0gOA0vSJPVot06HibajtPnptK/rqzVm/sbZDLbtU3rppkdjkAAAAnRXACEqSprUMdQUNS+GSj/ZWb4dDE0lxJqdd1MgxD91d+JCk8fnzkMEZoAwCA5ERwAhIk0m1yZ9jlstsG9NhU3ef00o5abTviUbbTpn/9+DizywEAADglghOQILXegS/Ti4js+0ml4GQYhla+FO423TxnjAqy+9+FAwAASDSCE5Ag9c1+SVJxzsCDU6Tj9MFhj3yBYEzrMkv3btPt8+g2AQCA5EZwAhKkfhAdp9GFWSrIdsofDOmDw55Yl5ZwdJsAAMBQQ3ACEqSuc4/TmXScLBZLdLnelhQYEEG3CQAADDUEJyBBIh2n4jPoOEmpMyCCbhMAABiKCE5AgtQPouMkSTNG5UsKjyQ3DCNWZSUc3SYAADAUEZyABKmLnvz2zDos547Ml91q0VGPT4cb22JZWsLQbQIAAEMVwQlIkOMtHZKkguwz6zhlOm2aUu6WNHSX69FtAgAAQxXBCUiQ463hceTDshxn/BxdAyIaY1FSQtFtAgAAQxnBCUgAXyCoVn/4/Ev5WWceGGYM4QERdJsAAMBQRnACEqCxNbxMz2a1yJ1hP+PniUzW217tUas/EJPaEoFuEwAAGOoITkACHGsJL9PLz3TIYrGc8fOU52Wo1O1SMGTovUNNsSov7l7u7DZl0W0CAABDFMEJSIDI/qb8QexvksInwo10nd4ZIifCNQxDK1+m2wQAAIY2ghOQAJGlesMGsb8pIjIg4p0hss/p5R21+uBwuNv0BbpNAABgiCI4AQkQnagXg25L9wERoVBynwiXbhMAAEgVBCcgAbo6ToNbqidJ08rzlOW06Xhrhz486h3088UT3SYAAJAqCE5AAhxviZzDafAdF6fdqo+NKZAkbdzTMOjnixe6TQAAIJUQnIAEON7ZcRrMOZy6mz2+UJL0xp76mDxfPNBtAgAAqYTgBCRAY2SPUwyW6knSnM7g9Pe9xxQIhmLynLFEtwkAAKQaghOQAF3jyGMTIKaW5yk3wy6vL6BtRzwxec5YotsEAABSDcEJSIDjMRwOIUk2q0UXjg13nZJtnxPdJgAAkIoITkACxHIceURkud4be5MrONFtAgAAqYjgBMRZMGSoqS0yHCI2HSepa0DE2/uOyRcIxux5B4NuEwAASFUEJyDOPG0dMjrPU5ufGbsgMak0V8W5LrV1BPX2vuMxe97BqNx+lG4TAABISQQnIM4iy/RyXHY57bH7kbNaLbpkYrEk6ZUPa2P2vGcqFDJ0f2W42/T5uXSbAABAaiE4AXHWdQ6n2C3Ti5h/domk5AhOz2+r0c4ar3JddrpNAAAg5RCcgDjztMcvOF18VpHsVov21rXoQENLzJ+/v4IhQz/u7DbdevHYmI1dBwAASBYEJyDOPJ2DIdwZsQ9O7gyHZo0ZJkl6Zad5Xafn3juiXbXNcmfYddu8sabVAQAAEC8EJyDOPO0BSVJuhj0uzz9/Uni53ssmBadAMKSVL+2SJP3rx8fFJSACAACYjeAExFk8O06SdNnkUknSG3sa1Ng5iCKRnt5yWPvqWzQsy6Fb5tJtAgAAqYngBMSZt7Pj5M6MT3CaUJKjs4fnKhAy9OL2o3F5jVNp7whG9zYt+cR45bji01UDAAAwG8EJiLPIcIh4LmG75pwySdKf36uO22uczKMb9utIU7vK8zJ085wxCX1tAACARCI4AXEWXaqXGb9uzNXnhoPTht31CVuud6zFr1Wv7JYkfXXBJGU4bAl5XQAAADMQnIA4iwyHiGfHaXxxt+V62xKzXO+nL++S1xfQlDK3/un8EQl5TQAAALMQnIA4i3Sc4jVVL+La6eWSpN+/cyiuryNJ++tb9MTfD0iSvnX1ZFmtlri/JgAAgJkITkCceSN7nOI0HCLin2eMkNUivbXvmPbUNcftdQzD0L3PbVdH0NAnJhbr4rOK4vZaAAAAyYLgBMRZIpbqSVJZXmb0nE5r3z4Yt9ep3H5Uf91ZK4fNom//w5S4vQ4AAEAyITgBcZaI4RARn7lglCTpD5sPyR8Ixfz52/xB3fOn7ZKk2+eN04SSnJi/BgAAQDIiOAFx1N4RlK8zwMR7qZ4kzZ9UrFK3Sw0tfq17P/ajyR94ZZcON7ZpRH6mvnLphJg/PwAAQLIiOAFxFDn5rcUi5Tjj33Gy26y66aLRkqSfr98jwzBi9tzvH2rSg+v3SpK+/Q9TlJWArwcAACBZEJyAOIqc/DbHZU/Y5LmbLhqjbKdNO2u8evWjupg8Z3tHUF/93VYFQoauPme4rpxaGpPnBQAAGCoITkAceRM0GKK7vCyHbuzsOq18aZdCocF3nX5c+ZE+Otqsohyn/uuT02SxMH4cAACkF4ITEEddgyESF5wk6fZ5Y5XttOndg43603tHBvVcf915VA++Fl6it+Kfz1VhjisWJQIAAAwpBCcgjiJL9dxxPvntiUpyM/Rvl4yXJH3/+Q/V6g+c0fNUNbRq6ZNbJUmfu2iUrpjCEj0AAJCeCE5AHHnaOpfqJbjjJIXHhY/Iz9ThxjatWLdzwI9vbPXrtl++LU97QOePytd3/mFqHKoEAAAYGghOQBx1dZwSH5wyHDb976fOlST96s0DennH0X4/ttUf0Ocfe1u7aptV6nZp1Y0z5LTzzwUAAEhfvBMC4iiyxyk3wUv1Ii4+q0i3zBkjSfrKb7fog8NNfT7mWItfNz70d22palRepkO/uu1CleVlxrlSAACA5EZwAuIoOlXPhKV6Ed+6erIunlCkVn9Qn3v473pr37FT3vedquO67mcboqHpsc9/TBNLcxNYLQAAQHIiOAFxZNZwiO6cdqtWf26Gplfkq7G1Q59Z84a+++w27a1rlmEYCoUMvX+oScv+v636l9UbVXWsVSPyM/WHf5ut80cNM61uAACAZGLeuzkgDZg1jvxEuRkOPfmFi/Stp9/X01sO67GN+/XYxv3KctoUCBnyB0LR+153Xrnu+eQ05ZlcMwAAQDIhOAFx5DHhBLinkum06ceLztM/zxihNa/t1Zt7G9TqD0qSspw2zZ9Uon/9+DhNr8g3t1AAAIAkZHpwWrVqlX7wgx+ourpaU6dO1cqVKzVv3rxT3v+JJ57Q97//fe3atUt5eXm66qqr9MMf/lCFhYUJrBron66Ok+k/alHzzirWvLOK1d4RVE1Tu2xWi8ryMmS3sXIXAADgVEx9p7R27VotXbpUd999t7Zs2aJ58+Zp4cKFqqqqOun9//a3v2nx4sW67bbbtG3bNv3ud7/T22+/rdtvvz3BlQP9402ijtOJMhw2jSnKVkVBFqEJAACgD6a+W7r//vt122236fbbb9fkyZO1cuVKVVRUaPXq1Se9/5tvvqkxY8bozjvv1NixY3XxxRfri1/8ojZt2pTgyoH+MfM8TgAAAIgd04KT3+/X5s2btWDBgh7HFyxYoI0bN570MXPmzNGhQ4e0bt06GYaho0eP6ve//72uueaaU76Oz+eTx+PpcQESIRAMRfcQmXUeJwAAAMSGacGpvr5ewWBQpaWlPY6XlpaqpqbmpI+ZM2eOnnjiCS1atEhOp1PDhw9Xfn6+/u///u+Ur7NixQrl5eVFLxUVFTH9OoBTafEFo9ezXQQnAACAocz0jQ0Wi6XHbcMweh2L2L59u+6880595zvf0ebNm/X8889r3759WrJkySmff/ny5WpqaopeDh48GNP6gVPx+sLL9Fx2q5x203/UAAAAMAim/Rq8qKhINputV3eptra2VxcqYsWKFZo7d66+/vWvS5LOPfdcZWdna968ebrvvvtUVlbW6zEul0sulyv2XwDQh2ZfeDAEy/QAAACGPtN+De50OjVz5kxVVlb2OF5ZWak5c+ac9DGtra2yWnuWbLPZJIU7VUAyaekMTizTAwAAGPpMXT+0bNkyPfTQQ3rkkUe0Y8cO3XXXXaqqqoouvVu+fLkWL14cvf+1116rp556SqtXr9bevXu1YcMG3XnnnbrgggtUXl5u1pcBnFRkFHkOwQkAAGDIM/Ud3aJFi9TQ0KB7771X1dXVmjZtmtatW6fRo0dLkqqrq3uc0+mWW26R1+vVAw88oK9+9avKz8/XpZdeqv/93/8160sATimyVI/gBAAAMPRZjDRb4+bxeJSXl6empia53W6zy0EKe/KtKn3zqfd1+eQSPXTzx8wuBwAAACcYSDZg1BcQJ83scQIAAEgZBCcgTliqBwAAkDoITkCcNEeGQzCOHAAAYMgjOAFxEj2PEx0nAACAIY/gBMSJlz1OAAAAKYPgBMRJC3ucAAAAUgbBCYiTyB6nXPY4AQAADHkEJyBOuqbqOUyuBAAAAINFcALixNse2eNkM7kSAAAADBbBCYiTFj9L9QAAAFIFwQmIA8Mwus7jxFI9AACAIY/gBMSBLxBSIGRI4gS4AAAAqYDgBMRBZH+TJGU52OMEAAAw1BGcgDjofg4nq9VicjUAAAAYLIITEAfNnPwWAAAgpRCcgDiILNVjfxMAAEBqIDgBcRDpOGXTcQIAAEgJBCcgDiJ7nHIJTgAAACmB4ATEgZc9TgAAACmF4ATEQeTktyzVAwAASA0EJyAOmn0dkqRchkMAAACkBIITEActvqAkluoBAACkCoITEAeMIwcAAEgtBCcgDiJL9djjBAAAkBoITkAcNDOOHAAAIKUQnIA4iEzVY48TAABAaiA4AXEQ6TixxwkAACA1EJyAOGjmBLgAAAApheAExAFL9QAAAFILwQmIsVDIUIu/8zxOLNUDAABICQQnIMZa/IHodTpOAAAAqYHgBMRYZH+T3WqRy86PGAAAQCrgXR0QYy2dwSnbZZfFYjG5GgAAAMQCwQmIMS+DIQAAAFIOwQmIsRZfeDBEtstmciUAAACIFYITEGPN3ZbqAQAAIDUQnIAYa+HktwAAACmH4ATEWGQcebaT4AQAAJAqCE5AjEWW6nHyWwAAgNRBcAJijKV6AAAAqYfgBMQYU/UAAABSD8EJiDGm6gEAAKQeghMQYyzVAwAASD0EJyDGoh0npuoBAACkDIITEGMtLNUDAABIOQQnIMYiwyFYqgcAAJA6CE5AjHUNh2CqHgAAQKogOAEx1sxwCAAAgJRDcAJiyDAM9jgBAACkIIITEEO+QEiBkCGJ4AQAAJBKCE5ADEW6TZKU7WSPEwAAQKogOAExFJmol+Gwym7jxwsAACBV8M4OiKGuwRAOkysBAABALBGcgBhq8UeCE8v0AAAAUgnBCYihZibqAQAApCSCExBDjCIHAABITQQnIIZaOPktAABASiI4ATHU3DlVj44TAABAaiE4ATHU1XFiOAQAAEAqITgBMRTd4+Sk4wQAAJBKCE5ADDFVDwAAIDURnIAYYjgEAABAaiI4ATFExwkAACA1EZyAGOoKTgyHAAAASCUEJyCGWjrHkbNUDwAAILUQnIAYamGpHgAAQEoiOAEx1MxwCAAAgJREcAJiiKl6AAAAqYngBMRIKGSoxR/e48RSPQAAgNRCcAJipLUjGL1OxwkAACC1EJyAGIks07NapAwHP1oAAACphHd3QIx0P/mtxWIxuRoAAADEEsEJiBEGQwAAAKQu04PTqlWrNHbsWGVkZGjmzJl6/fXXT3t/n8+nu+++W6NHj5bL5dL48eP1yCOPJKha4NSaOYcTAABAyjL1Hd7atWu1dOlSrVq1SnPnztWDDz6ohQsXavv27Ro1atRJH3P99dfr6NGjevjhhzVhwgTV1tYqEAgkuHKgtxYfE/UAAABSlanv8O6//37ddtttuv322yVJK1eu1AsvvKDVq1drxYoVve7//PPPa/369dq7d68KCgokSWPGjElkycApdS3Vs5lcCQAAAGLNtKV6fr9fmzdv1oIFC3ocX7BggTZu3HjSxzz77LOaNWuWvv/972vEiBGaOHGivva1r6mtre2Ur+Pz+eTxeHpcgHjwRpbqOek4AQAApBrT3uHV19crGAyqtLS0x/HS0lLV1NSc9DF79+7V3/72N2VkZOjpp59WfX29vvSlL+nYsWOn3Oe0YsUK3XPPPTGvHzgRwyEAAABSl+nDIU4c22wYxilHOYdCIVksFj3xxBO64IILdPXVV+v+++/XY489dsqu0/Lly9XU1BS9HDx4MOZfAyB1BSf2OAEAAKQe097hFRUVyWaz9eou1dbW9upCRZSVlWnEiBHKy8uLHps8ebIMw9ChQ4d01lln9XqMy+WSy+WKbfHASTBVDwAAIHWZ1nFyOp2aOXOmKisrexyvrKzUnDlzTvqYuXPn6siRI2pubo4e++ijj2S1WjVy5Mi41gv0heEQAAAAqcvUpXrLli3TQw89pEceeUQ7duzQXXfdpaqqKi1ZskRSeJnd4sWLo/e/4YYbVFhYqM9//vPavn27XnvtNX3961/XrbfeqszMTLO+DEBS1zhy9jgBAACkHlPf4S1atEgNDQ269957VV1drWnTpmndunUaPXq0JKm6ulpVVVXR++fk5KiyslJf+cpXNGvWLBUWFur666/XfffdZ9aXAESxVA8AACB1WQzDMMwuIpE8Ho/y8vLU1NQkt9ttdjlIIf+yeqM2HTiu1TfO0MJzyswuBwAAAH0YSDYwfaoekCroOAEAAKQughMQIy1+ghMAAECqIjgBMcJwCAAAgNRFcAJipGupHuPIAQAAUg3BCYiBjmBI/kBIEh0nAACAVERwAmIgcvJbiT1OAAAAqYjgBMRAZJme026Vw8aPFQAAQKrhHR4QA5HgxDI9AACA1ERwAmKghcEQAAAAKY3gBMRAc+co8mwnHScAAIBURHACYqCFpXoAAAApjeAExEDXOZwITgAAAKmI4ATEQLTjlEFwAgAASEUEJyAGosGJPU4AAAApieAExEB0OARL9QAAAFISwQmIga7hEIwjBwAASEUEJyAGWhgOAQAAkNIITkAMMFUPAAAgtRGcgBho8XMeJwAAgFRGcAJigOEQAAAAqY3gBMRA1x4nhkMAAACkIoITEAPN7SzVAwAASGUEJyAGmKoHAACQ2ghOwCAZhsFwCAAAgBRHcAIGqdUfVMgIX8/NIDgBAACkIoITMEiRczhZLVKmg+EQAAAAqYjgBAySt9tgCIvFYnI1AAAAiAeCEzBIkY5TbobD5EoAAAAQLwQnYJAYRQ4AAJD6CE7AIDX7OiQxGAIAACCVEZyAQYrucSI4AQAApKx+B6dbb71VXq83nrUAQ5KXpXoAAAApr9/B6Ze//KXa2triWQswJHUNhyA4AQAApKp+ByfDMOJZBzBkRYITHScAAIDUNaA9TpyjBuita6ke48gBAABS1YB+RT5x4sQ+w9OxY8cGVRAw1LBUDwAAIPUN6J3ePffco7y8vHjVAgxJze3hceRM1QMAAEhdA3qn95nPfEYlJSXxqgUYkqIdJ/Y4AQAApKx+73FifxNwcpzHCQAAIPUxVQ8YJM7jBAAAkPr6/U4vFArFsw5gyGI4BAAAQOob0DhyAD0ZhtEtODGOHAAAIFURnIBBaO8IKRgKL2NlqR4AAEDqIjgBg+D1hUeRWyxSltNmcjUAAACIF4ITMAjN3QZDMHkSAAAgdRGcgEHgHE4AAADpgeAEDALncAIAAEgPBCdgECLBiYl6AAAAqY3gBAxCZKkeE/UAAABSG8EJGITm9vBUPZbqAQAApDaCEzAIDIcAAABIDwQnYBC8LNUDAABICwQnYBCamaoHAACQFghOwCB42+k4AQAApAOCEzAIkT1ObsaRAwAApDSCEzAILNUDAABIDwQnYBAYDgEAAJAeCE7AIDT7OI8TAABAOiA4AYMQWarHeZwAAABSG8EJOEOGYUSHQ9BxAgAASG0EJ+AM+QIhdQQNSVIuU/UAAABSGsEJOEORczhZLFKWw2ZyNQAAAIgnghNwhrztnYMhXHZZrRaTqwEAAEA8EZyAM+Rp5+S3AAAA6YLgBJwhT1u44+TOJDgBAACkOoITcIY8nUv13EzUAwAASHkEJ+AMedo6l+rRcQIAAEh5BCfgDHV1nAhOAAAAqY7gBJyhrj1OLNUDAABIdQQn4AxFOk6c/BYAACD1EZyAM+SNjiOn4wQAAJDqCE7AGWIcOQAAQPogOAFniBPgAgAApA/Tg9OqVas0duxYZWRkaObMmXr99df79bgNGzbIbrfrvPPOi2+BwCkwHAIAACB9mBqc1q5dq6VLl+ruu+/Wli1bNG/ePC1cuFBVVVWnfVxTU5MWL16syy67LEGVAr0xjhwAACB9mBqc7r//ft122226/fbbNXnyZK1cuVIVFRVavXr1aR/3xS9+UTfccINmz56doEqB3iInwM1jjxMAAEDKMy04+f1+bd68WQsWLOhxfMGCBdq4ceMpH/foo49qz549+s///M9+vY7P55PH4+lxAQbLHwiprSMoiY4TAABAOjAtONXX1ysYDKq0tLTH8dLSUtXU1Jz0Mbt27dI3v/lNPfHEE7Lb+7evZMWKFcrLy4teKioqBl074O1cpidJOYwjBwAASHmmD4ewWCw9bhuG0euYJAWDQd1www265557NHHixH4///Lly9XU1BS9HDx4cNA1A5GJejkuu2zW3n9fAQAAkFpM+1V5UVGRbDZbr+5SbW1try6UJHm9Xm3atElbtmzRl7/8ZUlSKBSSYRiy2+168cUXdemll/Z6nMvlksvlis8XgbTljQ6GoNsEAACQDkzrODmdTs2cOVOVlZU9jldWVmrOnDm97u92u/X+++9r69at0cuSJUs0adIkbd26VRdeeGGiSgeigyE4+S0AAEB6MPXX5cuWLdNNN92kWbNmafbs2VqzZo2qqqq0ZMkSSeFldocPH9bjjz8uq9WqadOm9Xh8SUmJMjIyeh0H4o1R5AAAAOnF1OC0aNEiNTQ06N5771V1dbWmTZumdevWafTo0ZKk6urqPs/pBJiBk98CAACkF4thGIbZRSSSx+NRXl6empqa5Ha7zS4HQ9Sa1/boe+t26p/PH6H7F51ndjkAAAA4AwPJBqZP1QOGosgep1yGQwAAAKQFghNwBqJ7nBgOAQAAkBYITsAZiO5xYjgEAABAWiA4AWfA2x4ZR85SPQAAgHRAcALOAOPIAQAA0gvBCTgDnAAXAAAgvRCcgDPQxB4nAACAtEJwAs5AY5tfkpSfRXACAABIBwQnYIDaO4Jq7whJIjgBAACkC4ITMECRZXo2q0U5LqbqAQAApAOCEzBAx1s7l+llOmSxWEyuBgAAAIlAcAIGqLE13HHKY5keAABA2iA4AQMUCU75jCIHAABIGwQnYICaohP1nCZXAgAAgEQhOAEDRMcJAAAg/RCcgAFq7JyqR8cJAAAgfRCcgAGKdpwYDgEAAJA2CE7AAHXtcSI4AQAApAuCEzBA0XHk7HECAABIGwQnYIC6luqxxwkAACBdEJyAAWpqY6oeAABAuiE4AQPU2MoeJwAAgHRDcAIGwB8IqcUflCTlZ7JUDwAAIF0QnIABaOycqGe1SLkZdpOrAQAAQKIQnIABaOo2Uc9qtZhcDQAAABKF4AQMQGMbE/UAAADSEcEJGADO4QQAAJCeCE7AADBRDwAAID0RnIAB4BxOAAAA6YngBAxAZKkee5wAAADSC8EJGIDIOHL2OAEAAKQXghMwAMdbwh2nYexxAgAASCsEJ2AAGlp8kqSCHJfJlQAAACCRCE7AABxrCS/VK8xmjxMAAEA6ITgBAxAJTgUEJwAAgLRCcAL6KRQydLxzqh7BCQAAIL0QnIB+8rR3KBgyJEnDGEcOAACQVghOQD81dC7Ty82wy2nnRwcAACCd8O4P6Cf2NwEAAKQvghPQTw3NBCcAAIB0RXAC+olR5AAAAOmL4AT00/FWOk4AAADpiuAE9FNkqd4wghMAAEDaITgB/XSsxSeJpXoAAADpiOAE9FNDdKqey+RKAAAAkGgEJ6CfusaRO0yuBAAAAIlGcAL66TgdJwAAgLRFcAL6wTCM6FI99jgBAACkH4IT0A8t/qB8gZAkxpEDAACkI4IT0A/13vBEvSynTdkuu8nVAAAAINEITkA/1DWHg1NRDvubAAAA0hHBCeiHus6OU3EuwQkAACAdEZyAfqjv7DgV03ECAABISwQnoB8iHaeiXAZDAAAApCOCE9AP0aV6ORkmVwIAAAAzEJyAfogu1WOPEwAAQFoiOAH9EF2ql8NSPQAAgHREcAL6gal6AAAA6Y3gBPTBMAzVN/slEZwAAADSFcEJ6IOnLSB/MCSJE+ACAACkK4IT0Ie65nZJUm6GXRkOm8nVAAAAwAwEJ6APdV6W6QEAAKQ7ghPQh7rIKHKW6QEAAKQtghPQh+gocjpOAAAAaYvgBPThqCe8x2m4O8PkSgAAAGAWghPQh5omghMAAEC6IzgBfYgEp9I8ghMAAEC6IjgBfahhqR4AAEDaIzgBp2EYRjQ4ldFxAgAASFsEJ+A0Gls75A+EJEklbqbqAQAApCuCE3AakW5TQbZTLrvN5GoAAABgFoITcBrRwRDsbwIAAEhrBCfgNNjfBAAAACkJgtOqVas0duxYZWRkaObMmXr99ddPed+nnnpKV1xxhYqLi+V2uzV79my98MILCawW6YaOEwAAACSTg9PatWu1dOlS3X333dqyZYvmzZunhQsXqqqq6qT3f+2113TFFVdo3bp12rx5s+bPn69rr71WW7ZsSXDlSBdHGUUOAAAASRbDMAyzXvzCCy/UjBkztHr16uixyZMn67rrrtOKFSv69RxTp07VokWL9J3vfKdf9/d4PMrLy1NTU5PcbvcZ1Y30ccujb+nVD+v0v586R4s+NsrscgAAABBDA8kGpnWc/H6/Nm/erAULFvQ4vmDBAm3cuLFfzxEKheT1elVQUHDK+/h8Pnk8nh4XoL9YqgcAAADJxOBUX1+vYDCo0tLSHsdLS0tVU1PTr+f40Y9+pJaWFl1//fWnvM+KFSuUl5cXvVRUVAyqbqSXw41tkqQR+ZkmVwIAAAAzmT4cwmKx9LhtGEavYyfz29/+Vt/97ne1du1alZSUnPJ+y5cvV1NTU/Ry8ODBQdeM9NDU1iFve0CSNGIYwQkAACCd2c164aKiItlstl7dpdra2l5dqBOtXbtWt912m373u9/p8ssvP+19XS6XXC7XoOtF+jl8PNxtKsh2Kstp2o8KAAAAkoBpHSen06mZM2eqsrKyx/HKykrNmTPnlI/77W9/q1tuuUW/+c1vdM0118S7TKSxQ8dbJUkj6TYBAACkPVN/jb5s2TLddNNNmjVrlmbPnq01a9aoqqpKS5YskRReZnf48GE9/vjjksKhafHixfrJT36iiy66KNqtyszMVF5enmlfB1IT+5sAAAAQYWpwWrRokRoaGnTvvfequrpa06ZN07p16zR69GhJUnV1dY9zOj344IMKBAK64447dMcdd0SP33zzzXrssccSXT5SXGSpHsEJAAAApp7HyQycxwn9teRXm/X8thp999opumXuWLPLAQAAQIwNifM4AckuulRvWJbJlQAAAMBsBCfgFBgOAQAAgAiCE3ASLb6Ajrd2SOIcTgAAACA4AScVWaaXm2GXO8NhcjUAAAAwG8EJOImDxyLL9NjfBAAAAIITcFL7G8LBaWwRwQkAAAAEJ+Ck9te3SJLGFGabXAkAAACSAcEJOIn9DQQnAAAAdCE4AScRDU5FBCcAAAAQnIBe/IGQDh8PT9UbU8geJwAAABCcgF4OHm9VyJCynDYV57rMLgcAAABJgOAEnOBA5zK90YXZslgsJlcDAACAZEBwAk6wr55R5AAAAOiJ4AScoHvHCQAAAJAITkAve+qaJUljmagHAACATgQn4AS7joaD01klOSZXAgAAgGRBcAK6aWrtUK3XJ0k6qzTX5GoAAACQLAhOQDe7ar2SpPK8DOW47CZXAwAAgGRBcAK62VUbXqY3gW4TAAAAuiE4Ad2wvwkAAAAnQ3ACuoks1ZtYSnACAABAF4IT0E2k4zShhKV6AAAA6EJwAjo1tXWoxtMuSZrAUj0AAAB0Q3ACOm0/4pEkjcjPVF6mw+RqAAAAkEwITkCnbUeaJElTy90mVwIAAIBkQ3ACOkU6TtNG5JlcCQAAAJINwQno9AEdJwAAAJwCwQmQ1N4R1J66FknS1HI6TgAAAOiJ4ARI2lnjVTBkqCjHqVK3y+xyAAAAkGQIToCkDw6Hl+lNKc+TxWIxuRoAAAAkG4ITIGnrwUZJ0rkMhgAAAMBJEJwASe8cOC5Jmjl6mMmVAAAAIBkRnJD2jrX4tbc+PBji/FH55hYDAACApERwQtrbUhXuNo0vzlZ+ltPkagAAAJCMCE5Ie+90BqcZo1imBwAAgJMjOCHtvXOgURL7mwAAAHBqBCekNV8gqC0HGQwBAACA0yM4Ia29c6BR7R0hFee6NKEkx+xyAAAAkKQITkhrG/fUS5LmjC/kxLcAAAA4JYIT0tqG3eHgNHd8kcmVAAAAIJkRnJC2vO0devdQkyRpzoRCk6sBAABAMiM4IW29ufeYgiFDowuzNHJYltnlAAAAIIkRnJC2Xt5xVJI0f1KJyZUAAAAg2RGckJZCIUMv7aiVJF0+udTkagAAAJDsCE5IS+8ealR9s0+5LrsuGFtgdjkAAABIcgQnpKWXO7tNH59ULKedHwMAAACcHu8YkXYMw9Cf36+WJC2YwjI9AAAA9I3ghLTz3qEm7atvUYbDyv4mAAAA9AvBCWnnma2HJUlXTBmubJfd5GoAAAAwFBCckFYCwZD+9G54md5155WbXA0AAACGCoIT0sorH9apvtmnYVkOzTur2OxyAAAAMEQQnJBWfvXmAUnS9bMqmKYHAACAfuOdI9LGgYYWvfZRnSTphgtHmVwNAAAAhhKCE9LGLzeGu02fmFis0YXZJlcDAACAoYTghLTQ0OzTb9+qkiTdevFYk6sBAADAUENwQlp4bON+tXUEdc6IPH38rCKzywEAAMAQQ3BCyjvW4tdjG/ZLku6YP14Wi8XcggAAADDkEJyQ8n768i55fQFNKXNrwZThZpcDAACAIYjghJS2p65Zv+4cQf7/rpksq5VuEwAAAAaO4ISUFQoZ+tZT7ysQMnTp2SWaM4G9TQAAADgzBCekrCffPqi/7zumTIdN9/zjVLPLAQAAwBBGcEJK2nXUq/96brsk6asLJqqiIMvkigAAADCUEZyQclr9AX3piXfU1hHUvLOK9Pm5nLcJAAAAg0NwQkoJhgwtfXKrdtU2qyTXpR8vOk82BkIAAABgkAhOSBmGYejbf/xAL24/KqfdqgdumKGiHJfZZQEAACAFEJyQEkIhQ//vmQ/0m79XyWKRfrLoPF0wtsDssgAAAJAi7GYXAAxWmz+or//+XT33XrUsFul//vkcLTynzOyyAAAAkEIIThjS9tW36N9+vVk7a7yyWy368aLzdO30crPLAgAAQIohOGFICoYM/eqN/frBCx+qxR9UUY5L//fZ8zV7fKHZpQEAACAFEZwwpBiGoTf2NOh/n9+pdw81SZIuGFOg/7vhfJW6M0yuDgAAAKmK4IQhIRgy9NquOv381T36+75jkqRcl13/sfBs3XjBKFkZOQ4AAIA4Ijghqe2u9eov79do7aaDOnS8TZLktFl1w4Wj9KVLxquELhMAAAASgOCEpNLU2qFNB47p7/uO6eUdR7WnriX6OXeGXf8ys0K3zxur8vxME6sEAABAujE9OK1atUo/+MEPVF1dralTp2rlypWaN2/eKe+/fv16LVu2TNu2bVN5ebn+4z/+Q0uWLElgxYiFQDCk6qZ27alr1s4ar3ZWe7Sj2quPar0yjK77OWwWzZ1QpGvPLdc155Ypw2Ezr2gAAACkLVOD09q1a7V06VKtWrVKc+fO1YMPPqiFCxdq+/btGjVqVK/779u3T1dffbW+8IUv6Ne//rU2bNigL33pSyouLtanPvUpE74CdOcPhNTqD8jbHtCxFr+Otfp1rNmv461+HWvx66jHp0PHW3XoeJtqPO0KhoyTPs+4omxdMLZAcyYU6ZJJxXJnOBL8lQAAAAA9WQzDOPm71wS48MILNWPGDK1evTp6bPLkybruuuu0YsWKXvf/xje+oWeffVY7duyIHluyZIneffddvfHGG/16TY/Ho7y8PDU1Ncntdg/+ixiEAw0t2n7EI0OSYUiGDIWM8OQ4dTtmGOHrIcOQIUndj+vEzxldz9d5/ZTPqfDQhY5gSIGgoY5Q+GMgGJK/82Og2+cDofBxX0dQrf6gWv0BtfqDavEF1NYRVEdwYH+VnDarRhVmaXKZW2cPz9XZw3N1zog89i0BAAAgIQaSDUzrOPn9fm3evFnf/OY3exxfsGCBNm7ceNLHvPHGG1qwYEGPY1deeaUefvhhdXR0yOHo3Znw+Xzy+XzR2x6PJwbVx8b6j+r0nT9uM7uMmMtwWFWY7dKwbIeGZTlVkB2+FOW4NHJYpkYOy1LFsEwV5biYhgcAAIAhwbTgVF9fr2AwqNLS0h7HS0tLVVNTc9LH1NTUnPT+gUBA9fX1Kisr6/WYFStW6J577old4TFUkpuhWaOHyWKRLLKEP57yukUWRY6dcLvH5/r/WFkku9Uiu80qR+SjzSqHzSK71Sq7zSKnLfyx+31cdqtyXHZlOm3KdtqV5bIpy2lTltOuLKdNDpvVvD9UAAAAIA5MHw5hsfTsOBiG0etYX/c/2fGI5cuXa9myZdHbHo9HFRUVZ1puTF01bbiumjbc7DIAAAAA9MG04FRUVCSbzdaru1RbW9urqxQxfPjwk97fbrersLDwpI9xuVxyuVyxKRoAAABAWjJtTZXT6dTMmTNVWVnZ43hlZaXmzJlz0sfMnj271/1ffPFFzZo166T7mwAAAAAgFkzdjLJs2TI99NBDeuSRR7Rjxw7dddddqqqqip6Xafny5Vq8eHH0/kuWLNGBAwe0bNky7dixQ4888ogefvhhfe1rXzPrSwAAAACQBkzd47Ro0SI1NDTo3nvvVXV1taZNm6Z169Zp9OjRkqTq6mpVVVVF7z927FitW7dOd911l372s5+pvLxcP/3pTzmHEwAAAIC4MvU8TmZIpvM4AQAAADDPQLIBc6MBAAAAoA8EJwAAAADoA8EJAAAAAPpAcAIAAACAPhCcAAAAAKAPBCcAAAAA6APBCQAAAAD6QHACAAAAgD4QnAAAAACgDwQnAAAAAOgDwQkAAAAA+kBwAgAAAIA+EJwAAAAAoA92swtINMMwJEkej8fkSgAAAACYKZIJIhnhdNIuOHm9XklSRUWFyZUAAAAASAZer1d5eXmnvY/F6E+8SiGhUEhHjhxRbm6uLBaLaXV4PB5VVFTo4MGDcrvdptWBLnxPkgvfj+TD9yT58D1JPnxPkg/fk+STTN8TwzDk9XpVXl4uq/X0u5jSruNktVo1cuRIs8uIcrvdpv+FQU98T5IL34/kw/ck+fA9ST58T5IP35Pkkyzfk746TREMhwAAAACAPhCcAAAAAKAPBCeTuFwu/ed//qdcLpfZpaAT35Pkwvcj+fA9ST58T5IP35Pkw/ck+QzV70naDYcAAAAAgIGi4wQAAAAAfSA4AQAAAEAfCE4AAAAA0AeCEwAAAAD0geBkglWrVmns2LHKyMjQzJkz9frrr5tdUlp77bXXdO2116q8vFwWi0XPPPOM2SWltRUrVuhjH/uYcnNzVVJSouuuu04ffvih2WWltdWrV+vcc8+Nnqhw9uzZ+stf/mJ2WehmxYoVslgsWrp0qdmlpK3vfve7slgsPS7Dhw83u6y0d/jwYX3uc59TYWGhsrKydN5552nz5s1ml5W2xowZ0+vnxGKx6I477jC7tH4hOCXY2rVrtXTpUt19993asmWL5s2bp4ULF6qqqsrs0tJWS0uLpk+frgceeMDsUiBp/fr1uuOOO/Tmm2+qsrJSgUBACxYsUEtLi9mlpa2RI0fqf/7nf7Rp0yZt2rRJl156qT75yU9q27ZtZpcGSW+//bbWrFmjc8891+xS0t7UqVNVXV0dvbz//vtml5TWjh8/rrlz58rhcOgvf/mLtm/frh/96EfKz883u7S09fbbb/f4GamsrJQkffrTnza5sv5hHHmCXXjhhZoxY4ZWr14dPTZ58mRdd911WrFihYmVQZIsFouefvppXXfddWaXgk51dXUqKSnR+vXr9fGPf9zsctCpoKBAP/jBD3TbbbeZXUpaa25u1owZM7Rq1Srdd999Ou+887Ry5Uqzy0pL3/3ud/XMM89o69atZpeCTt/85je1YcMGVvYksaVLl+q5557Trl27ZLFYzC6nT3ScEsjv92vz5s1asGBBj+MLFizQxo0bTaoKSG5NTU2Swm/UYb5gMKgnn3xSLS0tmj17ttnlpL077rhD11xzjS6//HKzS4GkXbt2qby8XGPHjtVnPvMZ7d271+yS0tqzzz6rWbNm6dOf/rRKSkp0/vnn6xe/+IXZZaGT3+/Xr3/9a916661DIjRJBKeEqq+vVzAYVGlpaY/jpaWlqqmpMakqIHkZhqFly5bp4osv1rRp08wuJ629//77ysnJkcvl0pIlS/T0009rypQpZpeV1p588km98847rFZIEhdeeKEef/xxvfDCC/rFL36hmpoazZkzRw0NDWaXlrb27t2r1atX66yzztILL7ygJUuW6M4779Tjjz9udmmQ9Mwzz6ixsVG33HKL2aX0m93sAtLRianaMIwhk7SBRPryl7+s9957T3/729/MLiXtTZo0SVu3blVjY6P+8Ic/6Oabb9b69esJTyY5ePCg/v3f/10vvviiMjIyzC4HkhYuXBi9fs4552j27NkaP368fvnLX2rZsmUmVpa+QqGQZs2ape9973uSpPPPP1/btm3T6tWrtXjxYpOrw8MPP6yFCxeqvLzc7FL6jY5TAhUVFclms/XqLtXW1vbqQgHp7itf+YqeffZZvfLKKxo5cqTZ5aQ9p9OpCRMmaNasWVqxYoWmT5+un/zkJ2aXlbY2b96s2tpazZw5U3a7XXa7XevXr9dPf/pT2e12BYNBs0tMe9nZ2TrnnHO0a9cus0tJW2VlZb1+uTN58mQGciWBAwcO6KWXXtLtt99udikDQnBKIKfTqZkzZ0YniERUVlZqzpw5JlUFJBfDMPTlL39ZTz31lP76179q7NixZpeEkzAMQz6fz+wy0tZll12m999/X1u3bo1eZs2apRtvvFFbt26VzWYzu8S05/P5tGPHDpWVlZldStqaO3dur9NZfPTRRxo9erRJFSHi0UcfVUlJia655hqzSxkQluol2LJly3TTTTdp1qxZmj17ttasWaOqqiotWbLE7NLSVnNzs3bv3h29vW/fPm3dulUFBQUaNWqUiZWlpzvuuEO/+c1v9Mc//lG5ubnRDm1eXp4yMzNNri49fetb39LChQtVUVEhr9erJ598Uq+++qqef/55s0tLW7m5ub32/WVnZ6uwsJD9gCb52te+pmuvvVajRo1SbW2t7rvvPnk8Ht18881ml5a27rrrLs2ZM0ff+973dP311+utt97SmjVrtGbNGrNLS2uhUEiPPvqobr75ZtntQyuKDK1qU8CiRYvU0NCge++9V9XV1Zo2bZrWrVvHbz9MtGnTJs2fPz96O7IW/eabb9Zjjz1mUlXpKzKq/5JLLulx/NFHHx1SG0hTydGjR3XTTTepurpaeXl5Ovfcc/X888/riiuuMLs0IGkcOnRIn/3sZ1VfX6/i4mJddNFFevPNN/n/3UQf+9jH9PTTT2v58uW69957NXbsWK1cuVI33nij2aWltZdeeklVVVW69dZbzS5lwDiPEwAAAAD0gT1OAAAAANAHghMAAAAA9IHgBAAAAAB9IDgBAAAAQB8ITgAAAADQB4ITAAAAAPSB4AQAAAAAfSA4AQAAAEAfCE4AgLRyyy23yGKx9LpcddVVZpcGAEhidrMLAAAg0a666io9+uijPY65XC6TqgEADAUEJwBA2nG5XBo+fLjZZQAAhhCW6gEAAABAHwhOAIC089xzzyknJ6fH5b/+67/MLgsAkMRYqgcASDvz58/X6tWrexwrKCgwqRoAwFBAcAIApJ3s7GxNmDDB7DIAAEMIS/UAAAAAoA90nAAAacfn86mmpqbHMbvdrqKiIpMqAgAkO4ITACDtPP/88yorK+txbNKkSdq5c6dJFQEAkp3FMAzD7CIAAAAAIJmxxwkAAAAA+kBwAgAAAIA+EJwAAAAAoA8EJwAAAADoA8EJAAAAAPpAcAIAAACAPhCcAAAAAKAPBCcAAAAA6APBCQAAAAD6QHACAAAAgD4QnAAAAACgDwQnAAAAAOjD/w8TRaiExa6zWQAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 1000x600 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plot_transmission(L=4.0, m=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "64de33ea",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "32777ae4fde04b8b9546679884833fde",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=1.0, description='V', max=3.0, min=0.5, step=0.3), FloatSlider(value=3…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function __main__.plot_transmission(V=1.0, L=3.0, m=1.0)>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "interact(plot_transmission,\n",
    "         L=(1.0, 15.0, 0.5),\n",
    "         V=(0.5, 3, 0.3),\n",
    "         m=(1, 500, 50)\n",
    "        )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15fc2b98",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
