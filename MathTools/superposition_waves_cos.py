# %% [markdown]
# # Superposition of cosine waves

# %%
import numpy as np
import scipy.integrate as spi
import matplotlib.pyplot as plt
from matplotlib import rc
#plt.rc('text', usetex=False)
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

# %% [markdown]
# Copyright (C) 2024-, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>
# <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.


# %%
# Discretization of the x (e.g. position) coordinate
xmin = -np.pi
xmax = np.pi
nx = 2600
X = np.linspace(xmin, xmax, nx)

# %%
def superposition(klist, X, vel=0.0, time=0.0):
    nx = len(X)
    func = np.zeros(nx, float)
    for k in klist:
        func = func + np.cos(k*(X - vel*time))
    func = func / len(klist)
    return func


# %% [markdown]
# \begin{align}
#   f & = \sum_j^{N_k} \cos(k_j (x - vt)) \\
#   k_j & = [1,2,\dots]
# \end{align}

# %%
def plotSuper(Nk, time):
    klist = np.arange(1, Nk+1)
    f = superposition(klist, X, 0.25, time)
    plt.xlabel('x')
    plt.plot(X, f)

w1 = widgets.IntSlider(value=1, min=1, max=10)
w2 = widgets.IntSlider(value=0, min=0, max=5)
interact(plotSuper, Nk=w1, time=w2)
