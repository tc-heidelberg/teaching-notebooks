# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Two-level system under constant perturbation: Fermi's golden rule

# %%
import numpy as np
import scipy.integrate as spi
import matplotlib.pyplot as plt
from matplotlib import rc
plt.rc('text', usetex=False)
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets

fs2au = 41.341373206755
ev2au = 0.036749308824762
ic2au = 4.5563352527708e-06


# %% [markdown]
# Copyright (C) 2021-22, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>
# <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# %% [markdown]
# ### Functions for time propagation

# %%
def Propagate(t, C0, H):
    """
    Propagate the TDSE for Hamiltonian matrix H and initial state vector C0
    
    t - propagation time
    C0 - state vector for t=0
    H - Hamiltonian matrix
    """
    # diagonalize Hamiltonian matrix
    evals,evecs = np.linalg.eig(H)
    # Sort eigenvalues and eigenvactors from lowest to highest energy
    evecs[:] = evecs.take(evals.argsort(),axis=1) # axis=1: eigenvectors are stored as the columns of the evecs 2D array
    evals[:] = evals.take(evals.argsort())
    # Propagator U = PHP^T
    phases = np.exp(-1.0j*evals*t)
    iE = np.diag(phases)
    U = np.dot(evecs, np.dot(iE, evecs.T))
    return np.dot(U, C0)


# %%
# Exact time integration
def Pop_Exact(t, Eb, Vab):
    H = np.zeros((2,2), complex)
    H[1,1] = Eb
    H[0,1] = Vab
    H[1,0] = H[0,1]
    C = np.zeros(2, complex)
    C[0] = 1.0
    Ct = Propagate(t, C, H)
    return np.abs(Ct[1])**2


# %%
# Approximate (first order perturbation theory) time integration
def Pop_TDPT(t, Eb, Vab):
    Eb2 = Eb*Eb
    fac = 4.0*Vab**2/Eb2
    s2 = np.sin(Eb*t/2.0)**2
    return fac*s2


# %% [markdown]
# ### Time evolution of the $|b\rangle$ state as a function of time
#
# We compare here the time evolution of the system with Hamiltonian matrix
#
# $$
# \begin{pmatrix}
#   0       &   V_{ab} \\
#   V_{ab}  &   E_b
# \end{pmatrix}
# $$
#
# both exactly and using time-dependent first-order perturbation theory.
#
# You can experiment with the detuning ($E_b$) and the coupling constant ($V_{ab}$) to see when perturbation theory breaks down.

# %%
N = 500
tf = 100
Eb = 0.1
Vab = 0.05
plt.xlabel('t')
trange = np.linspace(0, tf, N)
pop_Ex = np.zeros(N, float)
pop_PT = np.zeros(N, float)
for j, t in enumerate(trange):
    pop_Ex[j] = Pop_Exact(t, Eb, Vab)
    pop_PT[j] = Pop_TDPT(t, Eb, Vab)
plt.plot(trange, pop_Ex, label='Pop. Ex')
plt.plot(trange, pop_PT, label='Pop. PT')
plt.legend()
plt.show()

# %% [markdown]
# ### Population of $|b\rangle$ as a function of the detuning for a fixed final time
#
# Here we explore graphically the population of the state $|b\rangle$ as a function of its detuning ($E_b-E_a$), whereby $E_a=0$. The perturbation theory result is shown inverted for easier comparison but you can easily change that if you want to plot both curves in the positive semiplane.
#
# You can experiment by increasing the final time $t_f$ to see how, eventually, the perturbation theory and the exact results will end up diverging. The perturbation theory result is also broken by increasing the interaction strength $V_{ab}$.
#
# By increasing $t_f$ you can also observe how the transition probability increases on resonance ($E_b\approx 0$) and is strongly suppressed everywhere else.

# %%
N = 4096 + 1         # Energy discretization
tf = 500*fs2au       # Final time
Vab = 10*ic2au       # Interaction potential
Emin = -250.0*ic2au  # Energy range around zero detuning
Emax = 260*ic2au
plt.xlabel('$E_b$ [cm-1]')
#plt.ylim(0,1.0)
Erange = np.linspace(Emin, Emax, N)
pop_Ex = np.zeros(N, float)
pop_PT = np.zeros(N, float)
for j, E in enumerate(Erange):
    pop_Ex[j] = Pop_Exact(tf, E, Vab)
    pop_PT[j] = Pop_TDPT(tf, E, Vab)
plt.plot(Erange/ic2au, pop_Ex, label='Pop. Ex')
plt.plot(Erange/ic2au, pop_PT, label='Pop. PT')
plt.legend()
plt.show()


# %% [markdown]
# ### Fermi's Golden Rule
#
# We integrate now the transition probability of the $|b\rangle$ state over the $E_b$ energy axis. Under the assumption that there is a continuum of $|b\rangle$-like states coupled to $|a\rangle$, this provides the total transition probability to this continuum:
#
# $$
# P_{a\to b}(t) = \int_{-\infty}^\infty |C_b(t; E_b)|^2 \rho(E_b) dE_b
# $$
#
# The density of states is assumed to be constant, $\rho(E_b)\to\rho$, and can be taken out of the integral.
#
# $$
# P_{a\to b}(t) = \rho \int_{-\infty}^\infty |C_b(t; E_b)|^2  dE_b
# $$
#
#
# Under first order perturbation theory one arrives at the FGR expression (see script):
#
# \begin{align}
# P_{a\to b}(t) & =  4 \int_{-\infty}^\infty \frac{|V_{ab}|^2  }{E_b^2}
#                   \sin^2
#                      \left( \frac{E_b t}{2\hbar}
#                      \right) \rho(E_b) dE_b \\
#               & \approx \frac{2\pi}{\hbar} |V_{ab}|^2 \rho \; t = K_{a\to b}\; t.
# \end{align}
#
# where $E_a=0$ and we
# assume that $V_{ab}$ is independent of the detuning energy (as usual, because only transitions close to zero detuning are relevant, see above).
#
# In the plots below it is just set to
# $\rho=1$ for simplicity, since it is irrelevant for the conclusions.
#
# In the plots below one can see how the integral of the transition probability integrated over the detuning energy grows linearly with time, instead of quadratically as one might have expected. This linear behaviour makes it meaningful to introduce a first order rate constant $K_{a\to b}$ to characterize the kinetics of the process.

# %%
def AreaCb2(Vab, tf, k, Emin, Emax, popFunc):
    N = 2**k + 1
    Erange = np.linspace(-0.11, 0.1, N)
    pop = np.zeros(N, float)
    for j, E in enumerate(Erange):
        pop[j] = popFunc(tf, E, Vab)
    intgr = spi.romb(pop, dx=Erange[1]-Erange[0], show=False)
    return intgr


# %%
tf = 1000*fs2au  # Final time in fs
N = 100         # Time discretization
Vab = 10*ic2au  # Coupling potential in cm-1

k = 12         # Energy discretization for integration over energy range (2**k + 1)
Emin = -300.0*ic2au  # Energy range around zero detuning
Emax = 310*ic2au

trange = np.linspace(0, tf, N)
AEx = np.zeros(N, float)
APT = np.zeros(N, float)
for j, t in enumerate(trange):
    AEx[j] = AreaCb2(Vab, t, k, Emin, Emax, Pop_Exact)
    APT[j] = AreaCb2(Vab, t, k, Emin, Emax, Pop_TDPT)

# %%
plt.xlabel('Time [fs]')
plt.ylabel('Integrated Population')
plt.plot(trange/fs2au, AEx, label='Exact')
plt.plot(trange/fs2au, APT, label='FGR')
plt.legend()
plt.show()

# %%
