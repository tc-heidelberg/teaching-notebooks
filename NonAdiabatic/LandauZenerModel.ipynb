{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical Integration of the Landau-Zener Model for Non-Adiabatic Transitions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Copyright (C) 2021, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/4.0/80x15.png\" /></a><br />This work is licensed under a <a rel=\"license\" href=\"http://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 International License</a>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy as sp\n",
    "import scipy.integrate as spi\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "#plt.rc('text', usetex=True)\n",
    "\n",
    "from ipywidgets import interact, interactive, fixed, interact_manual\n",
    "import ipywidgets as widgets"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Landau-Zener model consists of a two-level system represented by the real-symmetric Hamiltonian matrix\n",
    "\\begin{align}\n",
    "    \\mathbf{W}(x) =\n",
    "    \\begin{pmatrix}\n",
    "        W_{11}(x) & W_{12} \\\\\n",
    "        W_{12} & W_{22}(x)\n",
    "    \\end{pmatrix}\n",
    "\\end{align}\n",
    "depending on the parameter $x$, which in our case is assumed to represent an atomic displacement in a molecule.\n",
    "The diagonal elements correspond to the energy of two diabatic electronic states, which are assumed\n",
    "to have a constant slope with respect to $x$ and to cross at the origin:\n",
    "\\begin{align}\n",
    "    W_{11}(x) & =\\frac{1}{2}F_{12}x \\\\\n",
    "    W_{22}(x) & =-\\frac{1}{2}F_{12}x.\n",
    "\\end{align}\n",
    "where $\\frac{d}{dx}(W_{11}-W_{22})=F_{12}$ is the slope of the energy difference.\n",
    "\n",
    "One assumes as well that the coupling term $W_{12}$ is constant.\n",
    "\n",
    "Now, further assuming that the parameter $x$ varies with contant velocity, $x=vt$, one arrives at the final form for the time-dependent Hamiltonian:\n",
    "\\begin{align}\n",
    "    \\mathbf{W}(t) =\n",
    "    \\begin{pmatrix}\n",
    "        \\frac{1}{2}F_{12} v t & W_{12} \\\\\n",
    "        W_{12}                & - \\frac{1}{2}F_{12} v t\n",
    "    \\end{pmatrix}\n",
    "    =\n",
    "    \\begin{pmatrix}\n",
    "        \\frac{1}{2} \\alpha t & W_{12} \\\\\n",
    "        W_{12}                & - \\frac{1}{2} \\alpha t\n",
    "    \\end{pmatrix}\n",
    "\\end{align}\n",
    "\n",
    "Finally, the model depends on two parameters. $\\alpha$ determines the speed of change of the energy gap. $W_{12}$ is the\n",
    "coupling matrix element between both diabatic states."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The analytical expression for the asymptotic probability transfer reads\n",
    "\\begin{align}\n",
    "    P_{tr} = e^{-\\frac{2\\pi}{\\hbar}\\frac{|W_{12}|^2}{\\alpha}}\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Unit transformations\n",
    "au2ev = 27.21139613178773\n",
    "ev2au = 1.0/au2ev\n",
    "au2an = 0.529177249\n",
    "an2au = 1.0/au2an\n",
    "au2fs = 0.024188843341\n",
    "fs2au = 1.0/au2fs\n",
    "\n",
    "# Model Parameters\n",
    "alpha = 0.5*ev2au/fs2au # Energy gap change-rate in eV/fs\n",
    "G0 = 0.1*ev2au          # Gap at the avoided crossing in eV ; W12 = G0/2.0\n",
    "\n",
    "# Integration parameters\n",
    "T0 = 15.0*fs2au  # the TDSE is numerically integrated in the range [-T0, T0]\n",
    "DT = 0.1*fs2au "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Ptr(a, w):\n",
    "    return np.exp(-2.0*np.pi*w**2/a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We integrate now the TDSE\n",
    "\\begin{align}\n",
    "    \\begin{pmatrix}\n",
    "        \\dot{c}_1(t)\\\\\n",
    "        \\dot{c}_2(t)\n",
    "    \\end{pmatrix}\n",
    "    =\n",
    "    \\frac{-i}{\\hbar}\n",
    "    \\begin{pmatrix}\n",
    "        \\frac{1}{2} \\alpha t & W_{12} \\\\\n",
    "        W_{12}                & - \\frac{1}{2} \\alpha t\n",
    "    \\end{pmatrix}\n",
    "    \\begin{pmatrix}\n",
    "        c_1(t)\\\\\n",
    "        c_2(t)\n",
    "    \\end{pmatrix}\n",
    "\\end{align}\n",
    "numerically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def integrateLZ(g=G0, a=alpha, t0=T0, dt=DT):\n",
    "    # Hamiltonian matrix\n",
    "    W = np.zeros((2, 2), complex)\n",
    "    W[0, 1] = g/2.0\n",
    "    W[1, 0] = g/2.0\n",
    "    def Wt(t):\n",
    "        W[0, 0] = 0.5*a*t\n",
    "        W[1, 1] = -W[0, 0]\n",
    "        return W\n",
    "    \n",
    "    # Time derivative of coefficients\n",
    "    def dtpsi(t, c):\n",
    "        c = np.asarray(c, complex)\n",
    "        return -1j*np.dot(Wt(t), c)\n",
    "    \n",
    "    # Integrate\n",
    "    c0 = np.zeros(2, complex)\n",
    "    c0[1] = 1.0\n",
    "    r = spi.ode(dtpsi)\n",
    "    r.set_integrator('zvode',nsteps=100000)\n",
    "    r.set_initial_value(c0, -t0)\n",
    "    tlst = []\n",
    "    clst= []\n",
    "    while r.successful() and r.t < t0:\n",
    "        r.integrate(r.t+dt)\n",
    "        tlst.append(r.t)\n",
    "        clst.append(r.y.copy())\n",
    "    tlst = np.array(tlst)\n",
    "    clst = np.array(clst)\n",
    "    return tlst, clst"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(g=G0, a=alpha, t0=T0, dt=DT):\n",
    "    tlst, clst = integrateLZ(g, a, t0, dt)\n",
    "    plt.ylim(0, 1)\n",
    "    plt.xlim(-t0*au2fs, t0*au2fs)\n",
    "    plt.xlabel('t [fs]', fontsize=16)\n",
    "    plt.ylabel('$p_2(t)\\equiv|c_2(t)|^2$', fontsize=16)\n",
    "    plt.plot(tlst*au2fs, np.abs(clst[:, 1])**2, color='b')\n",
    "    plt.plot(tlst*au2fs, np.ones(len(tlst))*Ptr(a, g/2.0), color='r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAkcAAAG/CAYAAABSanMeAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMywgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/NK7nSAAAACXBIWXMAAA9hAAAPYQGoP6dpAABEgUlEQVR4nO3deXwV1cH/8e8lIQlbgiEIQbYICtEAkUSQsChqg+iPIlULDxYVpRSLUkCfRxFxQRFrW2stIiKLpSpQsIgKrUT2TdlCWC2LgbAkQEASwJCQZH5/nN7cuSSBS3KTm0s+79drXjNz5szMuXei98ssZxyWZVkCAACAJKmGrxsAAABQlRCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACwIRwBAADYEI4AAABs/DocrVq1Sn369FGTJk3kcDj0+eefX3adlStXKi4uTiEhIbr++us1ZcqUim8oAADwG34djs6dO6cOHTpo0qRJHtVPTU3Vvffeq+7duys5OVkvvPCCRowYoc8++6yCWwoAAPyF42p58azD4dCCBQt0//33l1rnueee0xdffKHdu3cXlQ0bNkwpKSlav359JbQSAABUdYG+bkBlWr9+vRITE93KevXqpenTp+vChQuqWbNmievl5uYqNze3aL6wsFCnTp1SgwYN5HA4KrTNAADAOyzL0pkzZ9SkSRPVqFH6xbNqFY4yMjLUqFEjt7JGjRopPz9fmZmZioyMLHG9iRMn6tVXX62MJgIAgAp26NAhNW3atNTl1SocSSp2psd5VfFSZ4DGjBmj0aNHF81nZWWpefPm+vrrQ6pXL1SFhZJlSYWFrsFsU6pRw4wtSzp/XqpZU8rNlYKDzXxp45AQM65Vy6zv3G5BgRlq1HBt2znOz3ffdkiI9NNP7nVL/168u6ws61iWdOaM+ZwhIVJOjilzfhf2aYfDfM6cHPPZQkKkc+ekgADznZ09KwUGSrVrm+mCArMsIMDUDwgw28jKkoKCpHr1pLw8Uzc01NTJzzdDrVpmP2fOmOk6dcz8uXNmv7Vrm+/5p5/Mcuf3/tNPZlnt2mbdM2fMtuvVk06fNoNz/aAgKTvbrHPNNabOuXNmndxcU1anjmlfVpYpa9DA1MvNNd9JTo757kJDzWfPyjL7OH9eCgszQ0CA9OOP0smTpr6zfXXqSBcuSOnprs9Vp47Zh32d48fN9xQaaobwcPM9nj7tWn7smGlH/fqm3bVru45PRoZ09KjZT1nVri21bWuG6GjpxhulVq2kFi3M9wgApcnOzlazZs1Ur169S9arVuGocePGysjIcCs7fvy4AgMD1aBBg1LXCw4OVnBwcLHy224LVWhoqNfbCVzt8vKkw4dN+CsoMGEuJMQVLoODTbg7dEjasUPassUM27aZAOmctwsIkFq2lG64Qbr1VumOO6SuXc22AMDucrfEVKtw1KVLF3355ZduZUuWLFF8fHyp9xsB8L6gIOn66y9dJzRUatZMSkhwleXnS/v2STt3mtC0c6e0Z4+0d68JTfv3m+Hf/5Zee82c2Ro4UBo+3JxpAgBP+PXTamfPntW+ffskSbfccovefvtt9ezZU+Hh4WrevLnGjBmjI0eOaNasWZLMo/wxMTH6zW9+o1//+tdav369hg0bptmzZ+uBBx7weL/Z2dkKCwtTVlYWZ46AKsCyzKW6PXuk77+X1q6VvvnGXMaTzFmlJ54wgenaa33bVgC+4+nvt1+HoxUrVqhnz57Fyh999FF99NFHeuyxx3TgwAGtWLGiaNnKlSs1atQo7dy5U02aNNFzzz2nYcOGXdF+CUdA1VdQIC1dKk2aJDlPGDduLM2ZI91+u2/bBsA3qkU48hXCEeBf1qyRhg0zl+Fq1JD+/GdpxAhftwpAZfP099uve8gGAE906yZt2CA9+qh5ku53v5P++EdftwpAVUU4AlAt1K4tzZwpObss+9//lT75xLdtAlA1EY4AVBsOh/TSS9Izz5j5IUNM9wAAYEc4AlDtvPWWdM89poPMgQNNR5oA4EQ4AlDt1Kgh/f3v5rH+nTvNI/4A4EQ4AlAtRURIkyeb6T/8wXQeCQAS4QhANfaLX0h3321eZzJmjK9bA6CqIBwBqLYcDuntt8143jxuzgZgEI4AVGvt2kkPPWSmX3/dt20BUDUQjgBUey++aMaffSYdOODTpgCoAghHAKq9du3MvUeFhdJf/+rr1gDwNcIRAMi8UkSS/vY3+j0CqjvCEQDIdAp53XXSyZPSwoW+bg0AXyIcAYCkwEDpscfM9Ecf+bIlAHyNcAQA//WrX5lxUpI5gwSgeiIcAcB/tW0rxcZK+fnSP//p69YA8BXCEQDY9O9vxvPn+7YdAHyHcAQANv36mfHy5VJ2tm/bAsA3CEcAYNOmjXTDDdKFC9KSJb5uDQBfIBwBwEV+/nMz/uor37YDgG8QjgDgIomJZrxsmWRZvm0LgMpHOAKAi3TtKtWsKR06JP3wg69bA6CyEY4A4CJ16ki33Wamly3zbVsAVD7CEQCUoGdPMyYcAdUP4QgASnDnnWa8fDn3HQHVDeEIAEpw221SSIh07Ji0e7evWwOgMhGOAKAEwcHmxmzJnD0CUH0QjgCgFM77jlat8m07AFQuwhEAlML5xNrGjb5tB4DKRTgCgFLExZlxaqp04oRv2wKg8hCOAKAU9eubd61J0qZNPm0KgEpEOAKAS7j1VjPesMG37QBQeQhHAHAJnTqZMeEIqD4IRwBwCc4zRxs30hkkUF0QjgDgEmJjpcBAc0P2wYO+bg2AykA4AoBLCAmROnQw0zzSD1QPhCMAuAxuygaqF8IRAFxGx45mnJLi23YAqByEIwC4jPbtzXj7dt+2A0DlIBwBwGXcfLMZZ2TQUzZQHRCOAOAy6taVWrUy05w9Aq5+hCMA8EC7dmZMOAKufoQjAPCA876jbdt82w4AFY9wBAAe4MwRUH0QjgDAA84zRzt2SAUFvm0LgIpFOAIAD7RqJdWqJeXkSD/84OvWAKhIhCMA8EBAgOuRfu47Aq5uhCMA8JDzviPCEXB1IxwBgIdiYsx4927ftgNAxSIcAYCH2rY1Y8IRcHUjHAGAh6KjzXjPHp5YA65mhCMA8FDz5lJIiJSXJx044OvWAKgohCMA8FBAgHTjjWb6++992xYAFYdwBABXwJv3Hb3/vrlUt2xZ+bcFwHsCfd0AAPAnzvuOynvmaMMG6be/NdO9e0v/+Y/UsmX5tgnAOzhzBABXwHnmqLzhaMIE13RenjR9evm2B8B7CEcAcAW8EY4uXJCWLzfTw4eb8TfflK9dALyHcAQAV+DGGyWHQzp5Ujpxomzb2LhROnNGCg+Xnn3WlG3YIJ0+7bVmAigHwhEAXIHataUWLcx0Wc8eLV1qxnfeae4zuvFGqbBQWrHi8uu+95502228wgSoSIQjALhC5b20tmGDGd9+uxl3727Gmzdfer3z56WnnpK++06KjZXS08u2fwCXRjgCgCtU3nDkXM/5rjbnC223b7/0el995Zq2LOnzz0uv+/e/S0OHSmfPlq2NQHXGo/wAcIXK09dRbq70ww/u23GGox07Lr3u7Nnu8+vXS08+WbxeTo70yCNm2uGQPvjgytsJVGecOQKAK1Sevo727TP3F4WGSo0amTJnOPrhB+ncudLXdV52GzPGjL/9tuR6X3/tmv7wQyk1tXidESOkVq2kUaNMey62a5e5cRyojq6KcDR58mRFRUUpJCREcXFxWr169SXrf/LJJ+rQoYNq166tyMhIDR48WCdPnqyk1gLwd84zPgcOmLM0V+I//3Ftw+Ew0w0bmqBkWdLOnSWvd/asdPCgmR482Iz37pUyM4vXnTfPNW1Z0rp17svPnTM3dv/wg/TOO9KmTe7Ld+6Ubr5Z6tZNyshwX/bOO9Jrr5nt2uXmXvl3AVRVfh+O5s6dq5EjR2rs2LFKTk5W9+7d1bt3b6WlpZVYf82aNXrkkUf0xBNPaOfOnZo3b542btyoIUOGVHLLAfirhg2la64xAWHv3itb13m2yRmwnC53ac0ZqiIipBtucK1f0tkd51NvznuaLn6ybcsW97NFW7a4L3/0UTPOy5Ps/9Y8dsycaXrpJWnKFFf5+fPmibuYGNPFgdO5c1JcnNSvn6vMsqQ//Un65z/d97l2bclnuABf8Ptw9Pbbb+uJJ57QkCFDFB0drXfeeUfNmjXT+++/X2L9b7/9Vi1bttSIESMUFRWlbt266Te/+Y02XfxPJwAohcNR9vuOSgtHznlnCLqYcz833WTGzjB18f6zs6WjR830r35lxikp7nW++8593h6OTp1yf2pu7dqS1/u//zOhSJK2bpXS0syZKGenlpI0f77Z9uefS4cPu7bx7LPSAw+4+nXavt2cpbrpJtcZqSNHzGW/p58285YlzZhR/Kb1vDz3+XXrzGdwOnZMys8XcEX8Ohzl5eVp8+bNSkxMdCtPTEzUuovPI/9XQkKCDh8+rMWLF8uyLB07dkzz58/XfffdV+p+cnNzlZ2d7TYAqN6c9x2VNRy1aeNe7pzfs6fk9Zz7ce63tDDlnG/UyNVVwMVnjpwhp1MnM7aHoV273Ova/1dqD0dnz7r2ZV9/7lzTwaXkfnnPGbLs2/jsMzN23iN1/rwrqL30kglbkyZJBQWmR/EnnpDat5eyskydf/9batBAGjnSzH/8sdS1q+sm9W++kZo0MUFOMme1nn/efB8FBeYM208/mWUrVkhBQeYerYtt3Wr6lirlZ6XIqVPmjFpOjgmof/qTmT550nwXBQUmvK5cacJedrbrvrGcnCs/C4kKZPmxI0eOWJKstWvXupVPmDDBuvHGG0tdb968eVbdunWtwMBAS5L185//3MrLyyu1/ssvv2xJKjZkZWV57bMA8C9vvWVZkmX17+/5OoWFllWvnllv5073ZV9/bcqjo0tet18/s/ydd8z8xx+b+R493OvNmmXKb7/dss6csSyHw8wfP+6q06KFKZsxw4xr1rSs3Fyz7IMPTNlNN5lxYKBlnTtnlt11lylzDrNnm/LBg93LN260rKwss66zbPhwU/d//sdV1rOnKRs40FU2bpxlFRRYVtOmrrIdOyxrwgTX/LPPWlZamvs+LcuyatVynw8Ods0XFlpWmzZm+u67Xd9fTIz5nuz7syzLmjnTLPvXvyyrWTNTHhxsWatWWVZsrGUtWmTqzZ9vvs9Vqyyra1dT73e/M3Uky/rNbyyrVy8z/eqrlvXLX5rpDz6wrF/8wkx/+qllPfqomV6+3ByXbt0sKyPDdcyysy3r++/N9KhRlhUZaVmHDllWerplffed+Xznz5vlEyda1n33WdZPP7nWnzDBsgYMsCz7T92MGZY1frxZ92KzZ5vlJZk3z7IWLix5mdPx4+ZvYOVKc/yOHTOfwdeysrI8+v2+KsLRunXr3Mpff/11q02bNiWus3PnTisyMtJ66623rJSUFOvf//631a5dO+vxxx8vdT/nz5+3srKyioZDhw4RjoBq7osvzI9Zhw6er3P0qFknIMD1Q+Z04IArqOTnF1/X+cP+9ddmftMmM9+okXu9F15w/ShblmW1amXmly0z8+fOuULAiROWVb++mU5JMctHjDDzo0dbVkSEmd6yxQSW0FAzHx9vxi+9ZNZp3949qPz975a1bp17mfN7iopylQUHm89qL4uNtaz1693XnTnTsh55xDUfFWVZ06a519m61X1+2zb3+Xnz3OeHDnVNP/OMK7Q6w4pz2hlgnEOfPiXv8557XNPO70lyD2x167qmGzd2TUdHu6Z79HAPgc89Z1k332xZt91mgu6//uVaPmqUK7i1bWtZ4eEmdDuX//WvlnXwoPlbc5bNnWv+Bux/B4mJ5vvNzTWfw96eVavM32ZhoWU9+aRlPfiga9n111tW9+7mb8Ppm28s69//Nm0JCXHVrVfPsuLiTEieMsWyjhzxbljas8cEsCeftKz337eshg1NEA8MNH/LAwda1tq11SQc5ebmWgEBAdY///lPt/IRI0ZYPS7+59R//epXv7IefPBBt7LVq1dbkqyjR496tF9Pv1wAV6+9e83/9ENCSg4zJVm2zKxzww3FlxUUuH5M9u1zX5abawKVZM4WWJb5YXH+8Jw65arr/DF3nmG6914zP2WKmU9JMfPh4Wb+ttvM/D/+YebvvtvMT5/uOhMyZ475kZXMj82bb5rphx6yrJwcV9v+3/8z4xdecJ2VatvWjGvUKH62RyoeogIDLWvyZPey4cMt65ZbXPMOhymz13niCfd5+xkqyXX2xjl06uQe3JyfQXKdNZNcZ9lKCjX24ZprXNN16pRcp7ShZk33EOHpehe37eJ2OIeGDYvvLy6ueL2Lv0PnUKuWCVqlteP22y1r5EjL+uqrK/vcbdua9WbONGF21y7LOnvWhM7jx03QSk21rA0bzN/9/v2WdfiwqbNggfnb+fnPix/rSw3Nmnn2++3XnUAGBQUpLi5OSUlJ6md7HCIpKUl9+/YtcZ2ffvpJgYHuHzsgIECSZFnWlTXg3Dnpv+sCqF5aNpTq15TyzkuHvjfvSLucfSlSbUkdWku6qD+jGpLat5J27JT2b5NaNXYt279bCi6Q6tWVrqtv1q1XQ2odKR1Nl/Zudd0/dHCX2cfNLU29mChphaQDO838/m1meftWZr7d9dK2b6XUHZLuNePaMuUxUVLyWumH7VJELVPe5nop9gYzfWCn2XdwgXRNfalXN2nZV2YbNXJMnd49pNNHpOwz0sJPTVnrVlJIiPmsn80yZTfeYG7CPveTlPS5Kbu2oXT8hJSyzuyrtvMLsVx1nL6a6z6/cLb7/Oqv3ed3bHDN702Rgm3LDuxyLTtx0H297Az3eafcH13l1rmS65TqglTzv5MFZzxf9+K2XdwOp3MnLiq7IO3eXLze7Oml7DtHeu7p0tu1caUZpr5zZZ877Xtpahl7mb+Yp/vNPHSJjsTsPPv3TtU1Z84cq2bNmtb06dOtXbt2WSNHjrTq1KljHThwwLIsy3r++eetQYMGFdWfOXOmFRgYaE2ePNnav3+/tWbNGis+Pt7q1KmTx/ssOnN0JRGZgYGBgYGBwadDljy7Z9ivzxxJUv/+/XXy5EmNHz9e6enpiomJ0eLFi9Xiv6/NTk9Pd+vz6LHHHtOZM2c0adIkPfPMM6pfv77uvPNO/f73v/fVRwAAAFWIw7Isy9eN8DfZ2dkKCwtT1tGjCg0N9XVzAPjIa69Jb/5eeuxR0+P05bRtKx06LH2TJHXpUnz5J59IQ38j3XG7tGiRq3zwYOkf86Txr0rPPOMqnzxZ+t//k37ex7x3bfFi6aFfSu3bmfeuSeZS1Y1tpMAA05t2r17Sdxukv8+SfvEL0+lk59uk+mHS669LTz0t3X2XtHCh6T4g/lZzOe/WW6Vly6Up70uDBkmJidJa26Ptb06UnnpKioqSTth67d69S1qwQHphrKvso5mmE8rHn3CVzZ1jujl4+RVXWeoP0uOPS8tXmPnnnzOvXXFu65r6UlKSaaPToTSpWXPX/A/7petbueYn/dV8Rkn62d3mMfstySXX7XSrtOG/nWxmpEuNI81061bSvv2uek2vkw4fcW1/9GjT7cDYsdKECaYbgunTpU8/lT76SPryS3N8Pv9c+v3vzee+9lrTZcDkyeax/7vuNtv73QjpL++6ptPSzN/GCy+YHtMnTJCaXGeW//ltKTzcbGfOHCk9Q1qzWpo40fR1FRpqejzfuFH6299MD++z/m7W3bHdHLvz56UGEaasR3epVi3pH/+QAgNNdwOxsaZvqawsqXZt89nq1DFdJkjmuI4da7pF+OADc/n0/HmpWTOpeXPTUWjv3lLjxp5divaUZUlLlkjXXSe98orUubPpBuLBB013CaNHm85b27TNVuPrmygrK+vSv99luZRV3XFDNgDLcj3V1K3b5evanw7KzCy5jvMpraZN3cudj4Vf/Pj0xY//O7sXGDDAVaegwLJq1zbl33/vejpt61az/KefXI/7O29sHTXKLDt/3v1GZck88WNZ5nF1e/m//mXKnV0OSObm4oKC4jfqbt9e/Omy/fst6/PPXfPh4eYJqWeecZWtW+e6qV0yT1lZlmW1bOkqsyzzCL59PjHRTLdoYW4gf/NN86j7ihWup/siI03dhx4y8716WdaXX5rP/8EHZtk//2lZYWGmnc7v6vHHzY3DHTpY1rffmnoXLpT+d3C5m/edj9WvXGmOw08/WdZ775mb40+cMI/inz7tvs7f/27aYX8C8sAB84i/fZvnzrnfvG9Z5lhc1BuOtX27eRqyJD/+aB7RP3TIPH15KYcPW9bJk6bejz9eum5lqRZPq/kK4QiAZZlH3CXLatDg8nWTk03diIjS65w86fpRP3vWlBUUuB4H37PHvb798f8LF8wPpGRZr7ziXu/WW035+PFmHBLi3t+N84ky5zBtmmtZt27uy06eNOV/+5t7+cGDpvyPf3SVDRliyg4dcpUFBpqn7woLLeuOO1zlBQXmh7t1a/N9vvmmWXfqVFed/Hyz3uzZpo3Ovpt27jTBZ8IEM799u2U1aWJZf/iD63sdPtwVFuzOnTP7OnzYzJ89a1l/+pPrh7+0oHPmjHnKyt6XEKo+T3+//f6eIwDwFWev1idPmktWERGl1y2tZ2y78HCzjcxM01tybKy5dJKTYy5TREW512/WzFz2yMkx7yVz9qJ98atJunQxl1L+8hczHxsr1azpWn733a72Sa5XlEjmEsiaNWb6rrtMGyXzzjSnDh2kpk3NdNeurnLnKyubNpW++kqaOtUsDwoy5V98YV4P0qmTVKOGeV/dnj2uF/JK0iOPSBs2SHfe6Xo4eMAA9893003mEpFTTIy5nOgUHm4usZSkdm3puedc83XqmEswToGl/ErWrSs99ljJy+D//Pr1IQDgS7VrS/999uOyrxEp7Z1qF3OGJ2d953ZvvLH4D3WNGu4voE3+770zHTq413Pe3+R8Keytt7ovv/tu13R4uPv6vXu7pu1hoG1bKT7eBJFFi0xbJFPWt695ea2zewFJuu8+cx+T81UeklSvnrkH57e/dZXZg5EkBQebV3r8z/8IqDSEIwAoB+e7zr6/TH8tnoajmBgz3rrVjJ3vOrOfzbFznqn54x/Nja8REcXPTiUkuM/Hx7vP33GHa3rsWBP6nGJjpR49zDvNfvELV3lAgDmjk5JiboJ1Cgw0Nxp/9FHxoAP4C8IRAJSDM+xc/MLWizlf0nq5cOS8XLVpkxlf/MLZi911lxk7zxp161Y8lDRr5n5JrkcP9+VhYeaS21NPmctcdg6HeVFqSop7aHIuq8GvCK5C3HMEAOXgPNOzfXvpdQoLrzwcbdlibkN2hp6bby65/u23m5Di7JSle/fidRwOc3/P+vUmGJX0CPWIEZduF1CdEI4AoBzatTPjS4WjQ4fMTdM1a16+b5eYGHPD8o8/moDkvLzWrVvJ9a+5xgSiVavMJa177y19u84gB+DSOCEKAOVw883mzMzx42YoifN+oxtuKP3pJ6egIFfgevNNc0aoTRspMrL0debNMzdFHzhw+TNTAC6PcAQA5VCnjnT99Wa6tLNHnt6M7XTffWY8f74Z9+x56frXXmvOGNlvjAZQdoQjACiny11a86SPI7uhQ93n77mnbO0CUDaEIwAoJ2e/QM77gy7mDE2e3vNz3XWmnyBJev556ec/L1fzAFwhwhEAlJOz3yDn4/d2liVt22am27f3fJsffmhu5J44kf6CgMpGOAKAcnI+fr97t3T2rPuyAwekM2fMjdaeXlaTzJNtzldyAKhchCMAKKfISHMprLCw+KU151mjm25yf58ZgKqLcAQAXuC8tLZxo3t5WS6pAfAtwhEAeEHnzma8erV7+YYNZhwbW6nNAVAOhCMA8II77zTjFSukggIzXVDgCkslvdYDQNVEOAIAL4iLk+rVM6/9SEkxZTt2SFlZUt26nDkC/AnhCAC8IDDQvARWkpYsMeNVq8y4a9fLvzYEQNVBOAIAL+nTx4xnzTL9G332mZm/3Os/AFQthCMA8JIBA6TatU1/Rx9+KK1cKQUESA8/7OuWAbgShCMA8JLQUKl/fzP9m9+YcZ8+dOYI+BvCEQB40cSJ0o03mulrr5XGj/dtewBcOW4RBAAvatRIWrNGWrhQuv9+KSLC1y0CcKUIRwDgZQ0bSkOG+LoVAMqKy2oAAAA2hCMAAAAbwhEAAIAN4QgAAMDmisPRiRMnlJKSorNnz5a4PDMzU7NmzSp3wwAAAHzB43CUn5+vwYMHq3HjxurYsaMaNmyokSNHKicnx63e/v37NXjwYK83FAAAoDJ4HI7effddzZ07V+PHj9eiRYs0atQoTZs2TQkJCTp27FhFthEAAKDSeByOZsyYoXHjxmns2LG655579MYbb2jjxo3KyclRQkKC9u3bV5HtBAAAqBQeh6PU1FQlJCS4lUVHR2vdunWKiIhQ165dtWXLFq83EAAAoDJ5HI4iIiKUnp5erDw8PFzLli1T+/bt1bNnTy1dutSrDQQAAKhMHoejuLg4LViwoMRlderU0aJFi3TXXXfpxRdf9FrjAAAAKpvH4WjgwIFKS0vTyZMnS1weFBSk+fPna+jQoWrevLnXGggAAFCZHJZlWb5uhL/Jzs5WWFiYsrKyFBoa6uvmAAAAD3j6+00P2QAAADaEIwAAABvCEQAAgE1gWVa68847vd0OSZLD4aArAAAA4FNlCkcrVqzwcjMMh8NRIdsFAADwVJnCUWpqqrfbAQAAUCWUKRy1aNHC2+0AAACoErghGwAAwIZwBAAAYEM4AgAAsCnTPUcluXDhgjZu3Kg1a9bo4MGDOnHihHJychQREaGGDRuqY8eO6t69u6677jpv7RIAAMDryh2Oli9frmnTpunzzz/X+fPnJUklva7N+Zh+dHS0Hn/8cT3yyCOKiIgo7+4BAAC8qswvnv3yyy81ZswY7d69W5ZlKTAwUO3atdOtt96qyMhIhYeHq1atWjp16pROnTqlXbt2aePGjTp27JgkKSgoSEOHDtW4cePUsGFDr36oisaLZwEA8D+e/n6XKRz16NFDa9euVa1atdSnTx8NGDBAvXr1UkhIyGXX3b9/v+bMmaPZs2dr165dqlevnmbNmqW+ffteaTN8hnAEAID/8fT3u0w3ZO/YsUPjxo3T4cOHNXv2bPXt29ejYCRJrVq10tixY7Vjxw4tXbpUcXFx2rZtW1maAQAA4HVlOnN05swZ1atXz2uN8Pb2KhpnjgAA8D8VeuaopCCzaNEiHT58uCyb86tgBAAArm5e6+eoT58+evjhh721OQAAAJ/waieQnl6hW7x4sTZs2ODNXQMAAHiFT3rInjhxohISEnyxawAAgEsqcyeQ48eP19/+9jfdcsst6tChgyTTS7YnCgsLPT7LBAAAUJnKHI4cDodSU1OVmpqqBQsWSJI2bNigunXrKiYmRh06dCga2rdvX3TTdV5envbt20fv2AAAoEoqcw/ZknTgwAElJydry5YtmjBhgmrWrKmCggIVFhaajf/3lSGS1KJFC7Vp00apqanau3evevfura+++qr8n8AHeJQfAAD/U6E9ZJekRo0a6tatm5YsWaIdO3YoJSWlaNi2bZuysrKK6kZGRupf//qX2rdv741dVzrCEQAA/sfT3+9yv3jW6eOPP1ZOTo5CQkIUHx+v+Ph4t+UHDx7Unj17VLt2bcXGxqpOnTre2rUmT56sP/zhD0pPT9fNN9+sd955R927dy+1fm5ursaPH6+PP/5YGRkZatq0qcaOHavHH3/ca20CAAD+yWvhaODAgZdc3qJFC7Vo0cJbuysyd+5cjRw5UpMnT1bXrl31wQcfqHfv3tq1a5eaN29e4jq//OUvdezYMU2fPl2tW7fW8ePHlZ+f7/W2AQAA/+O1y2q+0rlzZ3Xs2FHvv/9+UVl0dLTuv/9+TZw4sVj9f//73xowYIB++OEHhYeHl2mfXFYDAMD/VOjrQ37729+W+VUhF5szZ44+/fTTMq2bl5enzZs3KzEx0a08MTFR69atK3GdL774QvHx8Xrrrbd03XXX6cYbb9Szzz6rnJycUveTm5ur7OxstwEAAFydyhSOpk6dqtatW2vIkCFatWrVFa9/4sQJTZo0SdHR0Xr44YeVmppalmYoMzNTBQUFatSokVt5o0aNlJGRUeI6P/zwg9asWaMdO3ZowYIFeueddzR//nwNHz681P1MnDhRYWFhRUOzZs3K1F4AAFD1lemeo61bt+q5557TjBkzNHPmTDVp0kS9e/dWp06dFBcXp8jISIWHhysoKEhZWVk6deqUdu/erU2bNmnNmjVasWKFCgoK1KBBA/35z3/Wk08+Wa4PYe8yQDKvMbm4zKmwsFAOh0OffPKJwsLCJElvv/22HnzwQb333nuqVatWsXXGjBmj0aNHF81nZ2cTkAAAuEqVKRzFxMRo0aJFWr16tSZNmqSFCxdq2rRpmj59+iXXc97e1Lp1a/3617/WsGHDijqHLIuIiAgFBAQUO0t0/PjxYmeTnCIjI3XdddcVBSPJ3KNkWZYOHz6sG264odg6wcHBCg4OLnM7AQCA/yjX02rdu3dX9+7dlZmZqQULFmjVqlVat26dDh065Pb0V2hoqGJjY9WtWzclJiaqR48e5W64JAUFBSkuLk5JSUnq169fUXlSUpL69u1b4jpdu3bVvHnzdPbsWdWtW1eStGfPHtWoUUNNmzb1SrsAAID/qrCn1U6fPq3z588XXV6rKHPnztWgQYM0ZcoUdenSRVOnTtWHH36onTt3qkWLFhozZoyOHDmiWbNmSZLOnj2r6Oho3XbbbXr11VeVmZmpIUOG6Pbbb9eHH37o0T55Wg0AAP9T6Z1AXqx+/foVtWk3/fv318mTJzV+/Hilp6crJiZGixcvLupTKT09XWlpaUX169atq6SkJD399NOKj49XgwYN9Mtf/lKvv/56pbQXAABUbX7fz5EvcOYIAAD/U6H9HAEAAFytKiUcnThxQikpKTp79myJyzMzM4vuCQIAAPClCg1H+fn5Gjx4sBo3bqyOHTuqYcOGGjlyZLHeqPfv36/BgwdXZFMAAAA8UqHh6N1339XcuXM1fvx4LVq0SKNGjdK0adOUkJCgY8eOVeSuAQAAyqRCw9GMGTM0btw4jR07Vvfcc4/eeOMNbdy4UTk5OUpISNC+ffsqcvcAAABXrELDUWpqqhISEtzKoqOjtW7dOkVERKhr167asmVLRTYBAADgilRoOIqIiFB6enqx8vDwcC1btkzt27dXz549tXTp0opsBgAAgMcqNBzFxcVpwYIFJS6rU6eOFi1apLvuuksvvvhiRTYDAADAYxUajgYOHKi0tDSdPHmyxOVBQUGaP3++hg4dqubNm1dkUwAAADxCD9llQA/ZAAD4n0rvIXvOnDnq1q2bpkyZ4q1NAgAAVDqvhqP169df9vJYQUGBt3YJAADgdV4LR1u3btW1116re++995L1xo4dqzZt2uibb77x1q4BAAC8xmvhKCMjQy1atLhsvcGDB2vv3r36+OOPvbVrAAAAr/FaOKpfv75Onz592Xpt2rRRRESEvv32W2/tGgAAwGu8Fo5iY2O1d+9eHTx48LJ1mzdvrqNHj3pr1wAAAF7jtXA0YMAAWZalZ5555rJ1jx07xo3ZAACgSvJaOBo0aJA6duyoBQsW6IEHHlBWVlaJ9datW6cjR46oVatW3to1AACA13gtHAUEBGjhwoVq27atFixYoKioKD3//PNavny5Dhw4oN27d2vGjBl66KGH5HA41K9fP2/tGgAAwGu83kN2dna2nnzySc2ZM6fE5ZZl6eabb9a3336rOnXqeHPXlYYesgEA8D+V3kO2U2hoqD755BNt3rxZI0eOVIcOHdSgQQOFhISodevW+r//+z+tWbPGb4MRAAC4ugVW1IZjY2MVGxtbUZsHAACoEF4/cwQAAODPCEcAAAA2Zbqsduedd3q7HZIkh8OhpUuXVsi2AQAAPFGmcLRixQovN8NwOBwVsl0AAABPlSkcpaamersdAAAAVUKZwlGLFi283Q4AAIAqgRuyAQAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACw8Wo4WrZsmb777rui+e+++07Lli0rdR4AAKCqcViWZXlrYzVq1FB0dLR27twpSYqOjtbevXuVn59f4ry/ys7OVlhYmLKyshQaGurr5gAAAA94+vvt9ctqF2ety80DAABUJdxzBAAAYEM4AgAAsCEcAQAA2BCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACwIRwBAADYBHpzYwMHDtR1111XNN+nTx+lp6eXOg8AAFDVOCzeBHvFPH2rLwAAqDo8/f3mshoAAIAN4QgAAMCGcAQAAGBDOAIAALAhHAEAANgQjgAAAGwIRwAAADZe6wTywoUL2rhxo9asWaODBw/qxIkTysnJUUREhBo2bKiOHTuqe/fubp1EAgAAVDXlDkfLly/XtGnT9Pnnn+v8+fOSpJL6lXQ4HJKk6OhoPf7443rkkUcUERFR3t0DAAB4VZl7yP7yyy81ZswY7d69W5ZlKTAwUO3atdOtt96qyMhIhYeHq1atWjp16pROnTqlXbt2aePGjTp27JgkKSgoSEOHDtW4cePUsGFDr36oikYP2QAA+B9Pf7/LFI569OihtWvXqlatWurTp48GDBigXr16KSQk5LLr7t+/X3PmzNHs2bO1a9cu1atXT7NmzVLfvn2vtBk+QzgCAMD/VOjrQ3bs2KFx48bp8OHDmj17tvr27etRMJKkVq1aaezYsdqxY4eWLl2quLg4bdu2rSzNAAAA8LoyhaODBw/qlVdeUf369cu18549e2rZsmUaOXJkubYzefJkRUVFKSQkRHFxcVq9erVH661du1aBgYGKjY0t1/4BAMDVo0zhqF69esXKFi1apMOHD5epESVtz1Nz587VyJEjNXbsWCUnJ6t79+7q3bu30tLSLrleVlaWHnnkEd11111l3jcAALj6lPmG7IvVqFFD3bt318qVK72xOY917txZHTt21Pvvv19UFh0drfvvv18TJ04sdb0BAwbohhtuUEBAgD7//HNt3brV431yzxEAAP6nQu85Ko2nOWvx4sXasGFDufeXl5enzZs3KzEx0a08MTFR69atK3W9mTNnav/+/Xr55Zc92k9ubq6ys7PdBgAAcHXySQ/ZEydOVEJCQrm3k5mZqYKCAjVq1MitvFGjRsrIyChxnb179+r555/XJ598osBAz7p5mjhxosLCwoqGZs2albvtAACgaipzOBo/frxatWqlBx98UK+99pok00u2JwoLCz0+y+QJZweTTpZlFSuTpIKCAg0cOFCvvvqqbrzxRo+3P2bMGGVlZRUNhw4dKnebAQBA1VTmHrIdDodSU1OVmpqqBQsWSJI2bNigunXrKiYmRh06dCga2rdvX3TTdV5envbt2+eV3rEjIiIUEBBQ7CzR8ePHi51NkqQzZ85o06ZNSk5O1lNPPSXJFdQCAwO1ZMkS3XnnncXWCw4OVnBwcLnbCwAAqr4yh6Nx48Zp0KBBSk5O1pYtWzRhwgQFBgbq/Pnz2rBhgzZs2OB29qZFixZq06aNUlNTlZmZqd69e5e78UFBQYqLi1NSUpL69etXVJ6UlFRip5KhoaHavn27W9nkyZO1bNkyzZ8/X1FRUeVuEwAA8G/lerday5Yt1bJlS/Xr108TJkxQ586dtWTJEu3YsUMpKSlFw7Zt23TgwAEdOHBAkhQZGak33njDG+3X6NGjNWjQIMXHx6tLly6aOnWq0tLSNGzYMEnmktiRI0c0a9Ys1ahRQzExMW7rX3vttQoJCSlWDgAAqqdyv3jW6eOPP1ZOTo5CQkIUHx+v+Ph4t+UHDx7Unj17VLt2bcXGxqpOnTpe2W///v118uRJjR8/Xunp6YqJidHixYvVokULSVJ6evpl+zwCAABw8lo/R9UJ/RwBAOB/fNLPEQAAgL8jHAEAANiUKRz99re/LfN71C42Z84cffrpp17ZFgAAQHmVKRxNnTpVrVu31pAhQ7Rq1aorXv/EiROaNGmSoqOj9fDDDys1NbUszQAAAPC6Mj2ttnXrVj333HOaMWOGZs6cqSZNmqh3797q1KmT4uLiFBkZqfDwcAUFBSkrK0unTp3S7t27tWnTJq1Zs0YrVqxQQUGBGjRooD//+c968sknvf25AAAAyqRcT6utXr1akyZN0sKFC5WXl1fiKzvsnLtq3bq1fv3rX2vYsGFFPWf7E55WAwDA/3j6++2VR/kzMzO1YMECrVq1SuvWrdOhQ4eUn59ftDw0NFSxsbHq1q2bEhMT1aNHj/Lu0qcIRwAA+J9KDUclOX36tM6fP190ee1qQjgCAMD/ePr77bUesi9Wv379ito0AABAhaGfIwAAAJtKCUcnTpxQSkqKzp49W+LyzMxMzZo1qzKaAgAAcEkVGo7y8/M1ePBgNW7cWB07dlTDhg01cuRI5eTkuNXbv3+/Bg8eXJFNAQAA8EiFhqN3331Xc+fO1fjx47Vo0SKNGjVK06ZNU0JCgo4dO1aRuwYAACiTCg1HM2bM0Lhx4zR27Fjdc889euONN7Rx40bl5OQoISFB+/btq8jdAwAAXLEKDUepqalKSEhwK4uOjta6desUERGhrl27asuWLRXZBAAAgCtSoeEoIiJC6enpxcrDw8O1bNkytW/fXj179tTSpUsrshkAAAAeq9BwFBcXpwULFpS4rE6dOlq0aJHuuusuvfjiixXZDAAAAI9VaDgaOHCg0tLSdPLkyRKXBwUFaf78+Ro6dKiaN29ekU0BAADwSIW9PuRqxutDAADwP57+flfImSPLsnT69Gm3l88CAAD4A6+Go/Xr16tXr16qU6eOGjRooODgYEVGRurhhx/Wl19+6c1dAQAAVAivXVZbtmyZevfurfz8fDk36XA43KZjY2M1a9Ys3Xzzzd7Ypc9wWQ0AAP9T6ZfVXnzxRV24cEG33HKLli9frrNnz+rChQvau3evJk+erNjYWCUnJ6tz585atGiRt3YLAADgVV47c1S7dm05HA4dOHBADRs2LLHO1KlT9bvf/U4BAQFKSUlRq1atvLHrSseZIwAA/E+lnzkKCgpSmzZtSg1GkjR06FBNmjRJP/30k15//XVv7RoAAMBrvBaO2rZtq+PHj1+23uDBg9WwYUMtWbLEW7sGAADwGq+Fo4EDB+ro0aP6xz/+cekd1qihZs2a6ccff/TWrgEAALzGa+Gob9++SkxM1NChQ/XNN9+UWi8nJ0f79u1T69atvbVrAAAAr/FaOLr++uv17bffKjs7W7169dKAAQO0bNky5eXlFdU5fvy4Bg0apOzsbI0aNcpbuwYAAPAarz2t1rRpUx09etR94w6HatasqcaNGys/P18ZGRkKDAzUxIkT/Toc8bQaAAD+x9Pfb6++W+3HH3/Utm3blJKSUjTetWuXcnJy3HfqcKhp06Zq166dYmJiioabbrpJQUFB3mpOhSEcAQDgf3wSjkpSWFioPXv2uIWmbdu26dChQ65GOBySpICAALfLcFUV4QgAAP9TZcJRaU6fPu0WmLZu3apdu3bp3LlzvmjOFSEcAQDgf6p8OCqJZVlFZ5GqMsIRAAD+p9J7yPYGfwhGAADg6lalwhEAAICvEY4AAABsCEcAAAA2hCMAAAAbwhEAAIAN4QgAAMCGcAQAAGBDOAIAALAhHAEAANgQjgAAAGwIRwAAADaEIwAAABvCEQAAgA3hCAAAwIZwBAAAYEM4AgAAsCEcAQAA2BCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACwIRwBAADYEI4AAABsCEcAAAA2hCMAAACbqyIcTZ48WVFRUQoJCVFcXJxWr15dat1//vOf+tnPfqaGDRsqNDRUXbp00ddff12JrQUAAFWZ34ejuXPnauTIkRo7dqySk5PVvXt39e7dW2lpaSXWX7VqlX72s59p8eLF2rx5s3r27Kk+ffooOTm5klsOAACqIodlWZavG1EenTt3VseOHfX+++8XlUVHR+v+++/XxIkTPdrGzTffrP79++ull17yqH52drbCwsKUlZWl0NDQMrUbAABULk9/v/36zFFeXp42b96sxMREt/LExEStW7fOo20UFhbqzJkzCg8PL7VObm6usrOz3QYAAHB18utwlJmZqYKCAjVq1MitvFGjRsrIyPBoG3/605907tw5/fKXvyy1zsSJExUWFlY0NGvWrFztBgAAVZdfhyMnh8PhNm9ZVrGyksyePVuvvPKK5s6dq2uvvbbUemPGjFFWVlbRcOjQoXK3GQAAVE2Bvm5AeURERCggIKDYWaLjx48XO5t0sblz5+qJJ57QvHnzdPfdd1+ybnBwsIKDg8vdXgAAUPX59ZmjoKAgxcXFKSkpya08KSlJCQkJpa43e/ZsPfbYY/r000913333VXQzAQCAH/HrM0eSNHr0aA0aNEjx8fHq0qWLpk6dqrS0NA0bNkySuSR25MgRzZo1S5IJRo888oj+8pe/6Lbbbis661SrVi2FhYX57HMAAICqwe/DUf/+/XXy5EmNHz9e6enpiomJ0eLFi9WiRQtJUnp6ulufRx988IHy8/M1fPhwDR8+vKj80Ucf1UcffVTZzQcAAFWM3/dz5Av0cwQAgP+pFv0cAQAAeBvhCAAAwIZwBAAAYEM4AgAAsCEcAQAA2BCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACwIRwBAADYEI4AAABsCEcAAAA2hCMAAAAbwhEAAIAN4QgAAMCGcAQAAGBDOAIAALAhHAEAANgQjgAAAGwIRwAAADaEIwAAABvCEQAAgA3hCAAAwIZwBAAAYEM4AgAAsCEcAQAA2BCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACwIRwBAADYEI4AAABsCEcAAAA2hCMAAAAbwhEAAIAN4QgAAMCGcAQAAGBDOAIAALAhHAEAANgQjgAAAGwIRwAAADaEIwAAABvCEQAAgA3hCAAAwIZwBAAAYEM4AgAAsCEcAQAA2BCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEAABgQzgCAACwIRwBAADYEI4AAABsCEcAAAA2V0U4mjx5sqKiohQSEqK4uDitXr36kvVXrlypuLg4hYSE6Prrr9eUKVMqqaUAAKCq8/twNHfuXI0cOVJjx45VcnKyunfvrt69eystLa3E+qmpqbr33nvVvXt3JScn64UXXtCIESP02WefVXLLAQBAVeSwLMvydSPKo3PnzurYsaPef//9orLo6Gjdf//9mjhxYrH6zz33nL744gvt3r27qGzYsGFKSUnR+vXrPdpndna2wsLClJWVpdDQ0PJ/CAAAUOE8/f0OrMQ2eV1eXp42b96s559/3q08MTFR69atK3Gd9evXKzEx0a2sV69emj59ui5cuKCaNWsWWyc3N1e5ublF81lZWZLMlwwAAPyD83f7cueF/DocZWZmqqCgQI0aNXIrb9SokTIyMkpcJyMjo8T6+fn5yszMVGRkZLF1Jk6cqFdffbVYebNmzcrRegAA4AtnzpxRWFhYqcv9Ohw5ORwOt3nLsoqVXa5+SeVOY8aM0ejRo4vmCwsLderUKTVo0OCS+6nKsrOz1axZMx06dIhLgz7GsahaOB5VB8ei6rhajoVlWTpz5oyaNGlyyXp+HY4iIiIUEBBQ7CzR8ePHi50dcmrcuHGJ9QMDA9WgQYMS1wkODlZwcLBbWf369cve8CokNDTUr//QryYci6qF41F1cCyqjqvhWFzqjJGTXz+tFhQUpLi4OCUlJbmVJyUlKSEhocR1unTpUqz+kiVLFB8fX+L9RgAAoHrx63AkSaNHj9a0adM0Y8YM7d69W6NGjVJaWpqGDRsmyVwSe+SRR4rqDxs2TAcPHtTo0aO1e/duzZgxQ9OnT9ezzz7rq48AAACqEL++rCZJ/fv318mTJzV+/Hilp6crJiZGixcvVosWLSRJ6enpbn0eRUVFafHixRo1apTee+89NWnSRO+++64eeOABX30EnwgODtbLL79c7HIhKh/HomrheFQdHIuqo7odC7/v5wgAAMCb/P6yGgAAgDcRjgAAAGwIRwAAADaEIwAAABvCUTVz4MABPfHEE4qKilKtWrXUqlUrvfzyy8rLy3Orl5aWpj59+qhOnTqKiIjQiBEjitVB+U2YMEEJCQmqXbt2qR2Lciwqz+TJkxUVFaWQkBDFxcVp9erVvm7SVW/VqlXq06ePmjRpIofDoc8//9xtuWVZeuWVV9SkSRPVqlVLd9xxh3bu3Ombxl7lJk6cqFtvvVX16tXTtddeq/vvv1//+c9/3OpUl+NBOKpmvv/+exUWFuqDDz7Qzp079ec//1lTpkzRCy+8UFSnoKBA9913n86dO6c1a9Zozpw5+uyzz/TMM8/4sOVXp7y8PD300EN68sknS1zOsag8c+fO1ciRIzV27FglJyere/fu6t27t1tXIPC+c+fOqUOHDpo0aVKJy9966y29/fbbmjRpkjZu3KjGjRvrZz/7mc6cOVPJLb36rVy5UsOHD9e3336rpKQk5efnKzExUefOnSuqU22Oh4Vq76233rKioqKK5hcvXmzVqFHDOnLkSFHZ7NmzreDgYCsrK8sXTbzqzZw50woLCytWzrGoPJ06dbKGDRvmVta2bVvr+eef91GLqh9J1oIFC4rmCwsLrcaNG1tvvvlmUdn58+etsLAwa8qUKT5oYfVy/PhxS5K1cuVKy7Kq1/HgzBGUlZWl8PDwovn169crJibG7cV8vXr1Um5urjZv3uyLJlZbHIvKkZeXp82bNysxMdGtPDExUevWrfNRq5CamqqMjAy34xIcHKzbb7+d41IJsrKyJKno96E6HQ/CUTW3f/9+/fWvfy163YokZWRkFHtx7zXXXKOgoKBiL+1FxeJYVI7MzEwVFBQU+64bNWrE9+xDzu+e41L5LMvS6NGj1a1bN8XExEiqXseDcHSVeOWVV+RwOC45bNq0yW2do0eP6p577tFDDz2kIUOGuC1zOBzF9mFZVonlcFeWY3EpHIvKc/F3yvdcNXBcKt9TTz2lbdu2afbs2cWWVYfj4ffvVoPx1FNPacCAAZes07Jly6Lpo0ePqmfPnurSpYumTp3qVq9x48b67rvv3Mp+/PFHXbhwodi/GFDclR6LS+FYVI6IiAgFBAQU+9fv8ePH+Z59qHHjxpLMGYvIyMiico5LxXr66af1xRdfaNWqVWratGlReXU6HoSjq0RERIQiIiI8qnvkyBH17NlTcXFxmjlzpmrUcD+B2KVLF02YMEHp6elF/wEsWbJEwcHBiouL83rbrzZXciwuh2NROYKCghQXF6ekpCT169evqDwpKUl9+/b1Ycuqt6ioKDVu3FhJSUm65ZZbJJn7w1auXKnf//73Pm7d1ceyLD399NNasGCBVqxYoaioKLfl1el4EI6qmaNHj+qOO+5Q8+bN9cc//lEnTpwoWub8V0FiYqJuuukmDRo0SH/4wx906tQpPfvss/r1r3+t0NBQXzX9qpSWlqZTp04pLS1NBQUF2rp1qySpdevWqlu3LseiEo0ePVqDBg1SfHx80RnVtLQ0t/vx4H1nz57Vvn37iuZTU1O1detWhYeHq3nz5ho5cqTeeOMN3XDDDbrhhhv0xhtvqHbt2ho4cKAPW311Gj58uD799FMtXLhQ9erVKzqTGhYWplq1asnhcFSf4+HDJ+XgAzNnzrQklTjYHTx40LrvvvusWrVqWeHh4dZTTz1lnT9/3ketvno9+uijJR6L5cuXF9XhWFSe9957z2rRooUVFBRkdezYsegRZlSc5cuXl/jfwKOPPmpZlnl8/OWXX7YaN25sBQcHWz169LC2b9/u20ZfpUr7bZg5c2ZRnepyPByWZVmVGcYAAACqMp5WAwAAsCEcAQAA2BCOAAAAbAhHAAAANoQjAAAAG8IRAACADeEIAADAhnAEoNpYsWJFsZcAf/TRR8XqFRQU6I033lCbNm0UFBQkh8OhO+64o0z7jI2NddtfWbcDoPLw+hAAfuWdd97R6dOnNXLkSNWvX79M2wgNDVW7du0kqcQXZr700kt64403FBQUpJiYGNWuXbuo/pW65ZZbVLduXWVlZWnHjh1l2gaAykUP2QD8SsuWLXXw4EGlpqaqZcuWV7TuihUr1LNnT91+++1asWJFiXUsy1JERIR+/PFHbdiwQfHx8eVvtIf7BlA1cFkNAGxOnDihU6dOqWHDhl4LRgD8C+EIAGxycnIkSbVq1fJxSwD4CuEIgF/46KOP5HA4dPDgQUlSVFSU243O3rhU5XA4ii7VHTx4sMTt5+fn6y9/+Ys6deqkevXqKTg4WE2aNFFCQoJefvllnT59utztAOBb3JANwC80atRIXbt21aZNm5Sbm6v4+HgFBwcXLQ8LCyv3Prp27arc3Fxt2rRJwcHBbpfVnNsfMGCAPvvsM0lSq1atFB4eroyMDG3YsEHr169Xv379FBsbW+62APAdwhEAv9C7d2/17t276IbsefPmXfEN2ZezZs0aHThwQFFRUWrcuLHWrFnjtnzz5s367LPP1KxZM3399deKjo4uWpadna1//OMfatCggVfbBKDyEY4AwEN79+6VJD344INuwUgy3QMMGTLEF80C4GXccwQAHmrWrJkkaenSpTp16pSPWwOgohCOAMBDXbp0UefOnbVt2zY1a9ZM999/v95++21t3rxZdBkHXD0IRwDgoRo1auhf//qXfve736lWrVpauHChnnnmGcXHxysqKqrEV5EA8D+EIwC4Atdcc43eeecdnThxQsnJyfrLX/6inj176uDBgxo8eLDmz5/v6yYCKCfCEQC/4nA4fN0ESaYdsbGxGjFihJYtW6bnn39ekvThhx/6uGUAyotwBMCvOHuudvZkXVXcdtttkqSjR4/6uCUAyotwBMCvXH/99ZKklStXVvq+P/nkE7322ms6cOCAW/nJkyf17rvvSpI6duxY6e0C4F2EIwB+pX///pKkJ598Uu3atdMdd9yhO+64Q1u3bq3wfZ84cUIvvfSSoqKi1LRpU3Xq1Ent2rVTkyZNtGzZMl133XV67bXXKrwdACoWnUAC8CuDBg3Sjz/+qOnTp2vv3r3asWOHJFXKO80eeOAB5eXl6ZtvvtF//vMfbd++XXXq1FFMTIx+8YtfaPjw4apfv36FtwNAxXJYdM4BoJpYsWKFevbsqdtvv90rL6r1l30DuDKcOQJQ7SQnJ6tbt26SpLFjx6p3794Vtq/Bgwdr7969ysrKqrB9APAuwhGAaic7O1tr166VJB07dqxC95WcnKyUlJQK3QcA7+KyGgAAgA1PqwEAANgQjgAAAGwIRwAAADaEIwAAABvCEQAAgA3hCAAAwIZwBAAAYEM4AgAAsCEcAQAA2BCOAAAAbAhHAAAANv8fvn9IFSQ7/GoAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 640x480 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plot(0.3*ev2au, 0.90*ev2au/fs2au, 25*fs2au)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "0dabaa5d4de34356a6daf23f10117d79",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=0.4, description='g', max=0.8, min=0.1), FloatSlider(value=0.5, descri…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@widgets.interact(g=(0.1, 0.8, 0.1), a=(0.1, 1.0, 0.1), t0=(5, 25, 5), dt=(0.05, 0.5, 0.1))\n",
    "def updatePlot(g, a, t0, dt):\n",
    "    _g = g*ev2au\n",
    "    _a = a*ev2au/fs2au\n",
    "    _t0 = t0*fs2au\n",
    "    _dt = dt*fs2au\n",
    "    plot(_g, _a, _t0, _dt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
