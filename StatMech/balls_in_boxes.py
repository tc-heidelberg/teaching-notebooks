# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import numpy as np
import scipy.special as sps
import matplotlib.pyplot as plt

# %%
N = 8   # number of boxes


# %%
def probability_M_balls_indist(M, N, k):
    """
    Probability to find M indistinguishable balls in a box

    M -- int, how many balls in one box
    N -- int, how many boxes in total
    k -- int, how many balls in total
    """
    restBalls = k - M
    restBoxes = N - 1
    totalStates = sps.comb(N + k - 1, k)
    statesMBalls = sps.comb(restBalls + restBoxes - 1, restBalls)
    return statesMBalls/totalStates


# %%
def probability_M_balls_dist(M, N, k):
    """
    Probability to find M distinguishable balls in a box

    M -- int, how many balls in one box
    N -- int, how many boxes in total
    k -- int, how many balls in total
    """
    restBalls = k - M
    restBoxes = N - 1
    pM = (1.0/N)**M*(restBoxes/N)**restBalls*sps.comb(k, restBalls)
    return pM


# %%
def probability_M_balls_dist2(M, N, k):
    """
    Probability to find M distinguishable balls in a box (This is the good one!)

    M -- int, how many balls in one box
    N -- int, how many boxes in total
    k -- int, how many balls in total
    """
    restBalls = k - M
    restBoxes = N - 1
    partitionings = sps.comb(k, M)
    # Ways to distribute k-M balls in N-1 boxes
    distr = (N-1)**(k-M)
    # total states
    S = N**k
    # Probability
    pM = partitionings*distr/S
    return pM


# %%
probability_M_balls_dist2(2, 8, 4)

# %%
probability_M_balls_dist(3, 8, 10)


# %%
def statistics_balls(N, k, probability_function):
    """
    N -- int, how many boxes in total
    k -- int, how many balls in total
    """
    mean = 0.0
    vari = 0.0
    for m in range(k + 1):
        mean = mean + m*probability_function(m, N, k)
        vari = vari + m**2*probability_function(m, N, k)
    vari = vari - mean**2
    return mean, vari, np.sqrt(vari)


# %%
statistics_balls(100, 5000, probability_M_balls_indist)


# %%
def poisson(M, N, k):
    fac1 = (k/N)**M
    fac2 = np.exp(-k/N)
    return fac1*fac2/np.math.factorial(M)


# %%
probability_M_balls_dist2(5, 10, 50)

# %%
poisson(5, 10, 50)


# %%
# Plot the probability to find M balls in a box for N boxes and k total balls
def plot_probability(N, k, xrange=None):
    if not xrange:
        xrange = k
    pM = np.zeros(k + 1, float)
    pO = np.zeros(k + 1, float)
    for m in range(k + 1):
        pM[m] = probability_M_balls_dist2(m, N, k)
        pO[m] = poisson(m, N, k)
        #ipM[m] = probability_M_balls_indist(m, N, k)
    fig, ax = plt.subplots(1, 1)
    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)
    plt.xlabel('$M$ [chewing gum pro tile]', fontsize=14)
    plt.ylabel('$P(M)$', fontsize=14)
    plt.title('N='+str(N)+' tiles, k='+str(k)+' chewing gum', fontsize=14)
    plt.stem(pM[0:xrange], basefmt=' ')
    plt.plot(pO, label='Poisson')
    plt.legend(loc="upper right")
    #plt.savefig('/users/oriol/Downloads/chewinggum_'+str(N)+'_'+str(k)+'.pdf')
    #return np.sum(pM)
    #plt.plot(ipM)


# %%
plot_probability(10, 50)


# %%
def sample_balls_rnd(N, k):
    counter = np.zeros(N, int)
    r = np.random.randint(N, size=k)
    for rn in r:
        counter[rn] += 1
    return counter


# %%
def statistics_balls_rnd(N, k, nsample=1000):
    counter = np.zeros(N, int)
    counter2 = np.zeros(N, int)
    for j in range(nsample):
        sample = sample_balls_rnd(N, k)
        counter = counter + sample
        counter2 = counter2 + sample**2
    mean = counter/nsample
    var = counter2/nsample - mean**2
    std = np.sqrt(var)
    return mean, var, std


# %%
statistics_balls_rnd(100, 5000, nsample=1000)

# %%
