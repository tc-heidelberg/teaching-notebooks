---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

# More on: Lists, tuples, arrays etc. in python

Lists contain several objects that can be anything:

```{code-cell} ipython3
mylist1 = ['a','b','c','d','e','f']
print('A list with strings:',mylist1)
a=5
b=6
c=5682.3
mylist2 = [a,b,c]
print('A list with integers and floats:',mylist2)
mylist3 = ['you','with','May','4th','the','be']
print('Something garbled:',mylist3)
```

We can access the elements of a list using the index operator <code>[]</code> and zero-based indices:

```{code-cell} ipython3
print("mylist1[1]:     ", mylist1[1])     # second element
print("mylist1[-1]:    ", mylist1[-1])    # last element 
print("mylist1[-1]:    ", mylist1[-2])    # second-last element
print("mylist1[0:2]:   ", mylist1[0:2])   # [m:n] slices of lists elements m to n-1
print("mylist1[1:]:    ", mylist1[1:])    # [n:] elements from n and above
print("mylist1[1:5:2]: ", mylist1[1:5:2]) # [m:n:l] elements from m to n-1 step l
print("mylist1[::2]:   ", mylist1[::2])   # [::l] all elements step l
```

We cannot perform arithmetic operations on lists with floats:

```{code-cell} ipython3
5.8*mylist2
```

But we can perform operations on lists with integers <code>n</code> which will
return a new list with <code>n</code> times as many elements: The operation acts
on the list object, not on its elements. 

```{code-cell} ipython3
5*mylist2
```

We can perform arithmetic operations on single the elements of a list:

```{code-cell} ipython3
5.8*mylist2[1]
```

Lists themselves can also contain lists (and are called nested lists):

```{code-cell} ipython3
mylist4 = [mylist1,mylist2,mylist3]
print("mylist4: ", mylist4)
mylist5 = [[1,2,3],['f','d','s','a']]
print("mylist5: ", mylist5)
```

Elements in nested lists are accessed with consecutive index operators. The
first index-operator acts on the outer list and returns an inner list. We can
operate on the inner list immediately with a second index operator (or any other
operation supported by the object returned by the first index operator).

```{code-cell} ipython3
print("mylist5[0][0]:  ", mylist5[0][0])
print("mylist5[1][-1]: ", mylist5[1][-1])
print("mylist5[1][2]:  ", mylist5[1][2])
```

One can even put a function into the list. This can be useful to pass
functionality around between different parts of code. 

```{code-cell} ipython3
def myfunc():
    print("I am a function in a list")

myflist = [myfunc,]

print("mylist[0]: ", myflist[0])

# We can access the function with the index 
# operator. The index-operator returns the function object. 
# To execute the function we just need to add round brackets:
# one also says 'calling the function'. 
# Round brackets 'call' the function. 

myflist[0]()
# ------^    returns function
# ---------^ calls the function
```

Elements in lists can be changed:

```{code-cell} ipython3
print("mylist2: ", mylist2)
mylist2[1]=83.5
print("mylist2: ", mylist2)
```

Lists can be extended using append():

```{code-cell} ipython3
mylist1.append('d')
print("mylist1: ", mylist1)
mylist6=[]
mylist6.append(mylist1)
mylist6.append(mylist2)
print("mylist6: ", mylist6)
```

Lists can be extended without generating nested lists using extend:

```{code-cell} ipython3
print("mylist1: ", mylist1)
mylist1.extend('e') # single element
print("mylist1: ", mylist1)

mylist6=[]
print("mylist6: ", mylist6)
mylist6.extend(mylist1) # append elements from list
mylist6.extend(mylist2)
print("mylist6: ", mylist6)
```

We can also insert items:

```{code-cell} ipython3
# assignment: the same list is not known
# by twe different names
mylist7 = mylist1
print("mylist7: ", mylist7)

# insert an element at position 1. 
mylist7.insert(1,'x')
print("mylist7: ", mylist7)
# insert an element at position 1. 
mylist7.insert(1,'x')


print("mylist7: ", mylist7)
print("mylist1: ", mylist1)

mylist1 is mylist7
# note these point to the same memory address because 
# we used the '=' operation above
```

```{code-cell} ipython3
# create an empty list

mylist7 = []

# extend the list with mylist1
mylist7.extend(mylist1)

# they are not the same object, but have the same contents
print("mylist1 is mylist7: ", mylist1 is mylist7)

print("mylist7: ", mylist7)
mylist7.insert(1,'x')
print("mylist7: ", mylist7)
mylist7.insert(1,'x')
print("mylist7: ", mylist7)
print("mylist1: ", mylist1)

# adding elements to mylist7 does not change mylist1
```

Deleting items from lists:

```{code-cell} ipython3
print("mylist7: ", mylist7)

# delete an element using the index operator and the del command
del mylist7[0]
print("mylist7: ", mylist7)

# delete the first matching element
# (results in error if the element does not exist):
mylist7.remove('x')
print("mylist7: ", mylist7)

# remove an element using its position 
# and assign it to a variable:
lastelem = mylist7.pop(-1) # remove last element
print("mylist7: ", mylist7)
print("lastelem: ", lastelem)
```

Python List methods:

<code>append() </code>  - Add an element to the end of the list\
<code>clear()  </code>  - Removes all items from the list\
<code>copy()   </code>  - Returns a shallow copy of the list\
<code>count()  </code>  - Returns the count of number of items passed as an argument\
<code>extend() </code>  - Add all elements of a list to the another list\
<code>index()  </code>  - Returns the index of the first matched item\
<code>insert() </code>  - Insert an item at the defined index\
<code>pop()    </code>  - Removes and returns an element at the given index\
<code>remove() </code>  - Removes an item from the list\
<code>reverse()</code>  - Reverse the order of items in the list\
<code>sort()   </code>  - Sort items in a list in ascending order\



To find out about attributes (functions and variables) defined for an 
object or module use the <code>dir()</code> command:

+++

## Looping over lists

There are two kinds of loops in python: while-loop and for-loop.

### while-loop

Syntax: 

<code>
while condition:
    indented_code_block
</code>

The indented code block is executed over and over as long as the condition evaluates to <code>True</code>. One needs to make sure that the condition evaluates to <code>False</code> at some point, otherwise the loop will be infinite. 
    

```{code-cell} ipython3
mylist1 = ['a','b','c','d','e','f'] # mylist got a bit long now so start over

j=0 # needs to be initialized
while j < len(mylist1): # as long as condition is fulfilled, loop is executed
    print(mylist1[j])
    j = j+1
```

### for-loop

Syntax: 

<code>
for element in sequence:
    indented_code_block_using_element
</code>



The <code>range</code> function returns a sequence of integers and is often used in for-loops.

Syntax: <code>range(start=0, stop, step=1)</code> 

<code>start</code>  and <code>step</code>  are optional parameters. Their default value is indicated with the <code>=</code> in the syntax above.

```{code-cell} ipython3
# return a sequence of integers starting at 0, ending at 5 (not including 5)

for j in range(5):
    print(j)
```

```{code-cell} ipython3
# return a sequence of integers starting at 2, ending at 5 (not including 5)

for j in range(2,5):
    print(j)
```

```{code-cell} ipython3
# return a sequence integers starting at 1, ending at 5 (not including 5) in steps of 2

for j in range(1,5,2):
    print(j)
```

```{code-cell} ipython3
# we can feed the result of some other function directly into range(),
# by putting it into argument list of range: here the len() function.
# The output of the call to len() is then then input of range(), here the
# number of elements in mylist1. 

for j in range(len(mylist1)):
    print(j,mylist1[j])
```

```{code-cell} ipython3
# we can loop directly over list elements because lists are 'iterable'

for m in mylist1:
    print(m, mylist1.index(m))
```

```{code-cell} ipython3
# the enumerate function returns a sequence of index-element pairs.
# (it is actually a tuple of index and element
# which is unpacked in two variables - see below)
# Here the index is assigned to the variable j and the element to 
# variable mm. 

for j,mm in enumerate(mylist1):
    print(j,mm) # mm = mylist1[j]
    
```

## Arrays

- Arrays are used for mathematical operations
- Requires numpy - python itself has only lists
- <b>All elements in an array have the same data type</b>

```{code-cell} ipython3
# import everything from numpy (done above)
from numpy import *

length = 10

# create an array of length 10 filled with zeros (default data type: 64-bit float)
array1 = zeros(length)
print("array1 (zeros): ", array1)

# create an array of length 10 filled with ones
array1 = ones(length)
print("array1 (ones): ", array1)

# create a 2D-array (matrix) of size 3x3 but do not  
# initialize it. The numbers in the array are random 
# and can be anything. Use with care!
length1 = 3
array1 = empty([length1,length1])
print("array1 (empty): ", array1)

```

```{code-cell} ipython3
# we can convert a list to an array
myarray1 = array([1,2,3,4,5,6,7,8,9,10])
print("myarray1: ", myarray1)
print("type(myarray1): ", type(myarray1))

# the dtype attribute holds information on the kind of 
# data stored in the array.
# numpy will try to guess the correct data type. 
# we passed a list of integers, so it returned an integer array.
# try changing one element to a float and repeat. 
print("myarray1.dtype: ", myarray1.dtype)

# the data type can be specified explicitly:
myarray1 = array([1,2,3,4,5,6,7,8,9,10],dtype=complex)
print("myarray1: ", myarray1)
print("myarray1.dtype: ", myarray1.dtype)
```

Arrays can contain different types of data. The type is determined automatically unless it is explicitly declared.

```{code-cell} ipython3
myarray2 = zeros(length,dtype=ndarray)
print("myarray2: ", myarray2)
print("myarray2.dtype: ", myarray2.dtype)
myarray2[0]=myarray1
print("myarray2: ", myarray2)
```

The elements of an array can themselves be arrays (or other objects).

+++

Array indexing is similar to list indexing: <code>x[start:stop:step]</code>

```{code-cell} ipython3
print("myarray1[0:length:2]: ", myarray1[0:length:2])

# this is the same as

print("myarray1[::2]: ", myarray1[::2])

# if start and stop coincide with first and last element of the array, they need not to be given

print("myarray1[::-2]: ", myarray1[::-2])

# a negative step reverses the order.
```

And arrays can be reshaped:

```{code-cell} ipython3
# 9-element vector
myarray2 = array([1,2,3,4,5,6,7,8,9])
print("myarray2: ", myarray2)

# 3x3 matrix
myarray3 = myarray2.reshape(3,3)
print("myarray3: ", myarray3)

# the arrays are not the same object
print("myarray2 is myarray3: ", myarray2 is myarray3)

# the reshape operation returns a different 'view'
# on the array. 
# but they still share the same data buffer
myarray2[0] = -10
print("myarray3: ", myarray3)
```

We can access single elements of the matrix with the index operator. Note that the matrix is organized as an array of arrays:

```{code-cell} ipython3
# we can access sub-arrays 
myarray3[1] # returns a row of the matrix
```

```{code-cell} ipython3
myarray3[1][2]
```

```{code-cell} ipython3
# the index operator can take two parameters 
myarray3[1,2]
```

Very useful functions are <code>arange</code> and <code>linspace</code>.
</ode>arange</code> returns values between <code>start</code>, <code>stop</code>
with a <code>dx</code> where the last point (<code>stop</code> ) is not
included; whereas <code>linspace</code> returns values between
<code>start</code>, <code>stop</code>  with number of grid points where the last
point (<code>stop</code>) is included.

```{code-cell} ipython3
myarray2 = arange(1,10)
print("myarray2: ", myarray2)
print("arange(9): ", arange(9))
print("arange(0,9,0.5): ", arange(0,9,0.5))

myarray3 = linspace(0,11,12)
print("myarray3: ", myarray3)
myarray4 = myarray3.reshape(3,4)
print("myarray4: ", myarray4)
print("linspace(2,5,4): ", linspace(2,5,4))

# Note that arange defaults to dtype=int for integer input. 
# One can give an explicit dtype in both, arange and linspace
```

Arrays can be concatenated:

```{code-cell} ipython3
# arrays can be concatenated:
print("concatenate([myarray2,myarray2]): ", concatenate([myarray2,myarray2]))

# But note that other than for lists the + operation
# results in element-wise + and not in concatenation.
print("myarray2 + myarray2: ", myarray2 + myarray2)
print("myarray2 * myarray2: ", myarray2 * myarray2)
```

There is a large number of functions available for operations on arrays. See \
https://docs.scipy.org/doc/numpy/reference/routines.array-manipulation.html \
https://docs.scipy.org/doc/numpy/reference/routines.sort.html \
https://docs.scipy.org/doc/numpy/reference/routines.statistics.html 

For an overview see https://docs.scipy.org/doc/numpy/reference/routines.html.

+++

# Task 

Create an array with 256 values between zero and one, including one. Reshape this array into a 2D-matrix with equal number of rows and columns (square matrix). Multiply this matrix with the identity matrix __I__.

Remember: The identity matrix is a square matrix with ones on the diagonal and zero entries everywhere else.

\begin{align}
\mathbf{I} = 
\begin{pmatrix} 
1 & 0 & 0 & \ldots & 0 \\
0 & 1 & 0 & \ldots & 0 \\
\vdots & \vdots & \vdots & \ddots & \vdots \\
0 & 0 & 0 & \ldots & 1 \\
 \end{pmatrix} 
\end{align}

```{code-cell} ipython3

```
