# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Mean energy of the harmonic oscillator at a finite temperature 

# %% [markdown]
# Copyright (C) 2021, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>
# <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# %%
import numpy as np

import sympy as sp
from sympy import init_printing; init_printing(use_latex='mathjax')

import matplotlib.pyplot as plt
from matplotlib import rc
plt.rcParams['figure.figsize'] = [10, 6]
plt.rcParams['figure.dpi'] = 100
MEDIUM_SIZE = 10
plt.rc('axes', labelsize=MEDIUM_SIZE)

# %% [markdown]
# ### Classical harmonic oscillator

# %% [markdown]
# The classical harmonic oscillator is described by the Hamiltonian function (total energy)
#
# \begin{align}
#     H(p,x) & = T(p) + V(x) \\
#            & = \frac{p^2}{2m} + \frac{1}{2}k x^2,
# \end{align}
#
# where $m$ is the mass of the particle and $k$ is the force constant. Upon [solution of the classical equations of motion](https://en.wikipedia.org/wiki/Harmonic_oscillator),
# one finds that $k = m\omega^2$, where $\omega$ is the angular frequency of the oscillator. The Hamiltonian is hence more commonly written as
#
# \begin{align}
#     H(p,x) = \frac{p^2}{2m} + \frac{1}{2}m \omega^2 x^2.
# \end{align}
#
# The mean energy of the harmonic oscillator in thermal equilibrium at inverse temperature $\beta = 1/k_B T$ *(do not confuse the temperature $T$ with the kinetic energy function $T(p)$)*
# is obtained by averaging the energy of the system over coordinate space weighted by the probability that the system has that specific energy, namely:
#
# \begin{align}
#     \bar{E}(\beta) = \int_{-\infty}^{+\infty} \int_{-\infty}^{+\infty} H(p,x)\, P(H, \beta)\, dp\; dx.
# \end{align}
#
# $P(H, \beta)$ is the probability density at total energy $H$,
#
# \begin{align}
#     P(H, \beta) & = \frac{e^{-\beta H}}{\int_{-\infty}^{+\infty} \int_{-\infty}^{+\infty} e^{-\beta H}  dp\; dx } \\
#                 & = \frac{e^{-\beta H}}{Z(\beta) }
# \end{align}
#
# and clearly $\int_{-\infty}^{+\infty} \int_{-\infty}^{+\infty} P(H, \beta)  dp\; dx = 1$, as expected from a probability density.
#
# Now, the mean energy of the kinetic and potential energies can be calculated separately (*show this yourself*) as
#
# \begin{align}
#     \bar{T}(\beta) & = \int_{-\infty}^{+\infty} T(p) P(T, \beta)\, dp \\
#     \bar{V}(\beta) & = \int_{-\infty}^{+\infty} V(x) P(V, \beta)\, dx.
# \end{align}

# %%
x = sp.Symbol('x', real=True)
p = sp.Symbol('p', real=True)

m = sp.Symbol('m', real=True, positive=True)
w = sp.Symbol('omega', real=True, positive=True)

b = sp.Symbol('beta', real=True, positive=True)

n = sp.Symbol('n', integer=True)

# %%
# Define kinetic and potential energies
T = p**2/2
V = m*w**2*x**2/2

# Calculate partition functions
ZT = sp.integrate( sp.exp(-b*T), (p, -sp.oo, sp.oo) )
ZV = sp.integrate( sp.exp(-b*V), (x, -sp.oo, sp.oo) )

# Calculate mean energies
Tmean = sp.integrate( T*sp.exp(-b*T), (p, -sp.oo, sp.oo) ) / ZT
Vmean = sp.integrate( V*sp.exp(-b*V), (x, -sp.oo, sp.oo) ) / ZV

print(Tmean, Vmean, Tmean + Vmean)

# %% [markdown]
# Finally, the mean energy of the oscillator is
#
# \begin{align}
#     \bar{E}(\beta) = \bar{T}(\beta) + \bar{V}(\beta) = \frac{1}{\beta} = k_B T
# \end{align}
#

# %% [markdown]
# ### Quantum mechanical oscillator
#
# The energy of the quantum mechanical oscillator is quantized according to $E_n = (n+\frac{1}{2})\hbar\omega$. The mean energy at inverse temperature $\beta$ is not calculated, similarly as before, as
#
# \begin{align}
#     \bar{E}(\beta) = \sum_{n=0}^{\infty} E_n P(E_n, \beta)
# \end{align}
#
# with
#
# \begin{align}
#     P(E_m, \beta) & = \frac{ e^{-\beta E_m}}{ \sum_{n=0}^{\infty} e^{-\beta E_n} } \\
#                   & = \frac{ e^{-\beta E_m}}{ Z(\beta) }
# \end{align}
#
# Let us first substract the zero point energy, $E_0=\frac{1}{2}\hbar\omega$, such that $E_n = \hbar\omega\, n$.
#
# Making use of the known result
#
# \begin{align}
#     \sum_{n=0}^{\infty} e^{-a n} = \frac{1}{1-e^{-a}}
# \end{align}
#
# the partition function reads ($a\to\beta \hbar\omega$)
#
# \begin{align}
#    Z(\beta) =  \sum_{n=0}^{\infty} e^{-\beta\,\hbar\omega\, n} = \frac{1}{1-e^{-\beta\,\hbar\omega}}.
# \end{align}
#
# For the mean energy we still need to evaluate a sum of the form
#
# \begin{align}
#     S = \sum_{n=0}^{\infty} n e^{-a n}.
# \end{align}
#
# This can be done realizing that
#
# \begin{align}
#     S = - \frac{d}{da} \Big( \sum_{n=0}^{\infty} e^{-a n}\Big) =
#         - \frac{d}{da} \Big( \frac{1}{1-e^{-a}} \Big) = \frac{e^{-a}}{(1-e^{-a})^2}.
# \end{align}
#
# Combining these intermediate results one arrives at the mean energy
#
# \begin{align}
#     \bar{E}(\beta) & = \hbar\omega \frac{e^{-\beta \hbar\omega}}{(1-e^{-\beta \hbar\omega})^2} (1-e^{-\beta\,\hbar\omega}) \\
#                    & = \frac{ \hbar\omega} {(e^{\beta \hbar\omega} - 1)}
# \end{align}
