# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Particle in a box with step potential & Time propagation

# %% [markdown]
# Copyright (C) 2022, Oriol Vendrell <oriol.vendrell@uni-heidelberg.de>
# <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

# %% [markdown]
# ### Imports, initialization.

# %%
# For completeness, in fact not necessary because automatically imported by ipython.
import numpy as np
import scipy as sp
import scipy.integrate as spi
import matplotlib.pyplot as plt

# %% [markdown]
# ### Input

# %%
L = 2.0        # Box length
m = 1.0        # particle's mass
Q = -1.0       # particles's charge
Np = 60       # Number of real space points in segment
Nb = 10        # Number of basis functions (pbox solutions)
step = 0.0   # Height of step function (Hartree)
istep = 0.3    # Start of step (position)
fstep = 0.4    # End of step (position)
A0 = 0.0

# %%
X = np.linspace(0,L,Np)


# %% [markdown]
# Particle in a box energy $$E_n = \frac{\hbar^2 \pi^2}{2 m L^2} n^2$$

# %% [markdown]
# ### Particle in a box energies and eigenstates

# %%
# particle in a box analytical energy for level n (n>=1)
def pbox_a_energy(n): # particle in a box analytical energy for level n (n>=1)
    return np.pi**2/(2*m*L**2)*n**2


# %%
# particle in a box eigenstates on the real space grid;
# n>=1 !!
def pbox_basis(n):
    """
    n: int. quantum number
    """
    y = np.zeros(Np)
    for i, xx in enumerate(X):
        if xx < L or xx > 0.0:
            y[i] = np.sqrt(2.0/L)*np.sin(n*np.pi*xx/L)
    return y


# %%
Blist = []
for i in range(Nb):
    Blist.append(pbox_basis(i+1))
B = np.array(Blist)


# %% [markdown]
# ### Hamiltonian and potential energy operator on the real space grid

# %% [markdown]
# $$H = H_0 - q_e (x-\frac{L}{2})E$$ 

# %%
# Particle (electron) in a box with step function potential and constant electric field
def Vfunc():
    v = np.zeros(Np)
    for i,xx in enumerate(X):
        if xx > istep and xx < fstep:
            v[i] = step
        v[i] = v[i] - Q*(xx - 0.5*L)*A0
    return v


# %%
V = Vfunc()

# %%
plt.ylim((-1, max(1, step+step*0.1)))
plt.plot(X, V, marker='x', linestyle='--', color='b')


# %% [markdown]
# ### Matrix elements of the Hamiltonian operator

# %% [markdown]
# $$V_{ij}=\int_0^L \phi_i^*(x) V(x) \phi_j dx$$

# %%
def matrix_element_V(i,j):
    """
    i,j: quantum numbers
    """
    return spi.simpson(B[i-1]*V*B[j-1], x=X)


# %%
Vop = np.zeros((Nb,Nb),float)
for i in range(Nb):
    for j in range(Nb):
        Vop[i,j] = matrix_element_V(i+1,j+1)


# %% [markdown]
# $$ T_{ij} = \frac{-\hbar^2}{2m} \int_0^L \phi_i(x) \frac{d^2}{dx^2} \phi_j(x) dx $$

# %%
def pbox_basis2(n):
    y = np.zeros(Np)
    for i,xx in enumerate(X):
        if xx < L or xx > 0.0:
            y[i] = (0.5/m)*np.sqrt(2.0/L)*(n*np.pi/L)**2*np.sin(n*np.pi*xx/L)
    return y
    


# %%
def matrix_element_T(i,j):
    return spi.simpson(B[i-1]*pbox_basis2(j), x=X)


# %%
Top = np.zeros((Nb,Nb),float)
for i in range(Nb):
    for j in range(Nb):
        Top[i,j] = matrix_element_T(i+1,j+1)


# %% [markdown]
# $$ P_{ij} = i\hbar \int_0^L \phi_i(x) \frac{d}{dx} \phi_j(x) dx $$

# %%
def pbox_basis_D1(n):
    y = np.zeros(Np)
    for i,xx in enumerate(X):
        if xx < L or xx > 0.0:
            y[i] = np.sqrt(2.0/L)*n*np.pi/L*np.cos(n*np.pi*xx/L)
    return y
    


# %%
def matrix_element_P(i,j):
    return 1j*spi.simpson(B[i-1]*pbox_basis_D1(j), x=X)


# %%
Pop = np.zeros((Nb,Nb),complex)
for i in range(Nb):
    for j in range(Nb):
        Pop[i,j] = matrix_element_P(i+1,j+1)


# %% [markdown]
# $$ X_{ij} = \int_0^L \phi_i(x) x \phi_j(x) dx $$

# %%
def matrix_element_X(i,j):
    """
    i,j: quantum numbers
    """
    return spi.simpson(B[i-1]*X*B[j-1], x=X)


# %%
Xop = np.zeros((Nb,Nb),float)
for i in range(Nb):
    for j in range(Nb):
        Xop[i,j] = matrix_element_X(i+1,j+1)

# %%
#s1 = np.zeros(Nb,float)
#s1[3]=1.0
#comm = np.dot(Pop,Xop) - np.dot(Xop,Pop)
#acomm = np.dot(Xop,Pop) + np.dot(Pop,Xop)
#q = acomm/(2.0j)

# %% [markdown]
# $$H_{ij} = T_{ij} + V_{ij}$$

# %% [markdown]
# ### Build and diagonalize Hamiltonian matrix

# %%
H = Top + Vop

# %%
evals,evecs = np.linalg.eig(H)

# %%
evecs[:] = evecs.take(evals.argsort(),axis=1) # axis=1: eigenvectors are expected in columns
evals[:] = evals.take(evals.argsort())


# %%
#plot(evals[:10],marker='s', linestyle='--', color='m')

# %% [markdown]
# ### Eigenfunctions in the perturbed box

# %%
#ylim((0,1))
#plot(range(0,Nb),np.log(np.abs(evecs[:,3])**2),marker='o', linestyle='--', color='r')

# %% [markdown]
# The eigenvectors evecs[:,k] contain the expansion coefficients $c_j^{(k)}$ of the $k$-th state in the perturbed box (with the step potential) in terms of the orthogonal basis we have chosen, namely the eigenstates of the ``flat" particle in a box $\phi_j(x)$:
# $$ \psi_k(x) = \sum_j^{Nb} c_j^{(k)} \phi_j(x) $$
# The plot below shows on a log scale $\log(|c_j^{(k)}|^2)$ for selected eigenstates $k$.

# %%
# Coefficients to spatial representation for arbitrary coefficients vector 
def psi_coeff2grid(c):
    """
    v: array, vector with expansion coefficients
    """
    psi = np.zeros(Np,complex)
    for i in range(Nb):
        psi = psi + c[i]*np.dot(evecs[:,i],B)
    return psi


# %%
# Spatial representation to coefficients
def psi_grid2coeff(fx):
    """
    fx: array, real space representation of function f
    
    returns: fc, array, coefficient space representation of function f
    """
    fc = np.dot(B,fx)
    return fc


# %%
p = psi_coeff2grid(evecs[:,3])
p2 = np.abs(p)**2

# %%
vmax = max(V)
p2max = max(p2)
fact = p2max/(vmax+0.01)
#ylim((-2,4))
#plot(X,p2)
#plot(X,3*V*fact,marker='x', linestyle='--', color='r')

# %% [markdown]
# ### Dipole operator
# Here we build the dipole operator in the particle-in-a-box basis
# $$
# \mu_{i,j} = \int_0^L \Phi_i(x) \mu(x) \Phi_j(x) dx 
# $$
# where
# $$
# \mu(x) = -\,Q\, x
# $$

# %%
# Dipole operator in real space
def mu():
    m = np.zeros(Np)
    for i,xx in enumerate(X):
        m[i] = m[i] - Q*(xx - 0.5*L)
    return m
D = mu()


# %%
# Dipole matrix element between states m and n
def matrix_element_mu(m,n):
    itgr = 0.0
    return spi.simpson(B[m-1]*D*B[n-1], x=X)


# %%
Dop = np.zeros((Nb,Nb),float)
for i in range(Nb):
    for j in range(Nb):
        Dop[i,j] = matrix_element_mu(i+1,j+1)

# %%
Dop[0,1]

# %% [markdown]
# ### Electric (Laser) field
# Electric field $\epsilon(t)=A\cos(\omega t + \frac{3\pi}{2})$
#
# Uncomment only one of the two following boxes and study the evolution of the populations of the energy levels of the box below.
#
# The spectra of the pulses is the same (see also below). The only difference is in their intensity and sequence. In the sequential population the first pulse induces the $1\to 2$ transition followed by the second pulse inducing the $2\to 3$ transition.
#
# In the non-squential case, the sequence of the $1\to 2$ and $2\to 3$ pulses is inverted. For this scheme to work, the intensities have to become larger. As the $2\to 3$ is already on, the $1\to 2$ starts pumping up population which becomes immediately transferred to $|3\rangle$. This is simple example of the process called *Stimulated Raman Adiabatic Passage* (STIRAP).

# %% jupyter={"outputs_hidden": true}
# Sequential population of the levels 1 -> 2 -> 3
A1 = 0.55
A2 = 0.5

fwhm1 = 15.0
fwhm2 = 15.0

tc1 = 20.0   # 1 -> 2 pulse
tc2 = 30.0   # 2 -> 3 pulse

# %%
# Direct population 1 -> 3 (STIRAP)
#A1 = 2.0
#A2 = 2.0

#fwhm1 = 15.0
#fwhm2 = 15.0

#tc1 = 30.0   # 1 -> 2 pulse
#tc2 = 20.0   # 2 -> 3 pulse

# %%
# Pulse normalization constants
s1 = fwhm1/(2.0*np.sqrt(2.0*np.log(2)))
s2 = fwhm2/(2.0*np.sqrt(2.0*np.log(2)))
ph = 1.5*np.pi

# %% [markdown]
# $|1\rangle\rightarrow|2\rangle\rightarrow|3\rangle$

# %%
omega12 = pbox_a_energy(2) - pbox_a_energy(1) # resonant 1 <-> 2 levels
omega23 = pbox_a_energy(3) - pbox_a_energy(2) # resonant 2 <-> 3 levels
def Efield(t):
    f1 = A1*np.exp(-0.5*(t-tc1)**2/s1**2)*np.cos(omega12*t - ph)
    f2 = A2*np.exp(-0.5*(t-tc2)**2/s2**2)*np.cos(omega23*t - ph)
    return f1+f2


# %% [markdown]
# Chirped pulse

# %%
omega12 = pbox_a_energy(2) - pbox_a_energy(1) # resonant 1 <-> 2 levels
omega23 = pbox_a_energy(3) - pbox_a_energy(2)
omega34 = pbox_a_energy(4) - pbox_a_energy(3)
def chirp(t):
    f1 = 2.0*np.exp(-0.5*(t-tc1)**2/s1**2)*np.cos(omega12*t + ph)
    f2 = 2.0*np.exp(-0.5*(t-tc2)**2/s2**2)*np.cos(omega23*t - ph)
    f3 = 2.0*np.exp(-0.5*(t-tc3)**2/s3**2)*np.cos(omega34*t + ph)
    return f1+f2+f3


# %%
tf = 50.0
EF = Efield
tgrid = np.linspace(0,tf,1500)
efg = EF(tgrid)

# %%
plt.plot(tgrid, efg)


# %% [markdown]
# Here we calculate the Fourier transform of the $\epsilon(t)$ pulse. This yields its frequency representation $\sigma(\omega)$:
#
# \begin{align}
#     \sigma(\omega) = \int_{-\infty}^{+\infty} \epsilon(t) \exp(i \omega t) dt
# \end{align}

# %%
def sigma(w):
    return spi.simpson(efg*np.exp(1j*w*(tgrid)), x=tgrid)


# %%
fgrid = np.linspace(0,10,200)
spec = list(map(sigma,fgrid))

# %%
plt.plot(fgrid,np.abs(spec))


# %% [markdown]
# ### Time propagation

# %% [markdown]
# Time proagation is performed here by numerically solving the time dependent Schrödinger equation. The main ingredients for such a strategy are a function that returns the action of the Hamiltonian on a state vector and a general integration routine:
# $$
# \dot{\mathbf{\Psi}} = -i \mathbf{H}\mathbf{\Psi}
# $$
# This strategy can then be used trivially for time-dependent Hamiltonians

# %%
def dtpsi(t,p):
    Ht = H - Dop * EF(t)
    return -1j*np.dot(Ht,p)


# %%
t0 = 0.0
dt = 0.1
tf = 50.0
y0 = np.zeros(Nb,complex)
y0[0]=1.0
r = spi.ode(dtpsi)
r.set_integrator('zvode',nsteps=100000)
r.set_initial_value(y0, t0)
tlst = []
ylst= []
while r.successful() and r.t < tf:
    r.integrate(r.t+dt)
    tlst.append(r.t)
    ylst.append(r.y.copy())

# %%
tlst = np.array(tlst)
ylst = np.array(ylst)

# %% [markdown]
# Eigenstate populations $|c_j|^2$

# %%
plt.ylim((0,1))
plt.xlim((0,tf))
plt.plot(tlst,np.abs(ylst[:,0])**2,color='b')
plt.plot(tlst,np.abs(ylst[:,1])**2,color='g')
plt.plot(tlst,np.abs(ylst[:,2])**2,color='r')
plt.plot(tlst,np.abs(ylst[:,3])**2,color='m')

# %% [markdown]
# $$\Psi(x,t)$$

# %%
t = 5.0
it = int(t/dt)
p = psi_coeff2grid(ylst[it,:])
p2 = np.abs(p)**2
plt.ylim((0,5))
plt.title('t=%s  i=%s'%(tlst[it],it))
plt.plot(X,p2)

# %%
