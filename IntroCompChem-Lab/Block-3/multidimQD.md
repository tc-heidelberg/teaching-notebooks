---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

+++

# Multidimensional Quantum Dynamics
For a comprehensive discussion of the topic and detailed derivations visit https://www.pci.uni-heidelberg.de/tc/usr/mctdh/lit/intro_MCTDH.pdf

## Standard approach wavefunction ansatz and its limitations
How can we represent a multidimensional wavefunction on a computer? A very useful starting point is to define sets of basis functions for each coordinate.  Let us consider a wavefunction that depends on $F$ coordinates, and for each coordinate $q_\kappa$ we define a number of basis functions $\left\{\chi_{j_\kappa}^{(\kappa)}(q_\kappa)\right\}$. Then we can define the projector on the so-called *primitive basis* for coordinate $q_\kappa$ as

\begin{align}
P_\kappa = \sum_{j_\kappa} \left| \chi_{j_\kappa}^{(\kappa)}(q_\kappa)\right\rangle\left\langle \chi_{j_\kappa}^{(\kappa)}(q_\kappa)\right|
\end{align}

The projection of a wavefunction $\psi(q_1, q_2, \cdots, q_f)$ into the space of all primitive basis functions for all coordinates is then given as


\begin{align}
P_1 P_2 \cdots\ P_F \left| \psi(q_1, q_2, \cdots, q_F)\right\rangle & = \prod_\kappa P_\kappa \left| \psi(q_1, q_2, \cdots, q_F)\right\rangle \\[.4cm]
& = \sum_{j_1}   \sum_{j_2} \cdots  \sum_{j_F}   C_{j_1, j_2, \cdots j_F} \left| \chi_{j_1}^{(1)}(q_1)\right\rangle \left| \chi_{j_2}^{(2)}(q_2)\right\rangle \cdots \left| \chi_{j_F}^{(F)}(q_F)\right\rangle
\end{align}

where 

\begin{align}
C_{j_1, j_2, \cdots j_F} = \left\langle \phi_{j_1}^{(1)}(q_1)\right|  \left\langle \phi_{j_2}^{(2)}(q_2)\right| \cdots \left\langle \phi_{j_F}^{(F)}(q_F)\right| \left. \psi(q_1, q_2, \cdots, q_f) \vphantom{\phi_{j_f}^{(f)}(q_F)} \right\rangle
\end{align}.

From previous lectures we know that it is advantageous to use DVR functions for $\left\{\chi_{j_\kappa}^{(\kappa)}(q_\kappa)\right\}$. In this case $j_1,\dots,j_F$ run over DVR grid points and

\begin{align}
    C_{j_1, j_2, \cdots j_F} = \psi(q_{1,j_1}, q_{2,j_2}, \cdots, q_{F,j_F}).
\end{align}

In any case, we only store the coefficients  $C_{j_1, j_2, \cdots j_F}$ in memory and keep a record of which basis functions they belong to. Instead of storing a vector with a single index we now need to store a multi-dimensional array with as many indices as there are coordinates in the system.

### Curse of dimensionality
We can easily see that this multi-dimensional array grows exponentially with the number of coordinates: if $N_\kappa$ is the number of basis functions for one coordinate, then the total number $N$ of coefficients grows as
\begin{align}
N &= \prod_\kappa N_\kappa
\end{align}

This is called the curse of dimensionality. It gets even worse when looking at operators which need to be projected from both, left and right:


\begin{align}
\prod_\kappa P_\kappa \hat{O}\prod_\kappa P_\kappa =   \sum_{i_1}  \cdots  \sum_{i_f}   \sum_{j_1}   \cdots  \sum_{j_F}  O_{i_1\cdots i_F, j_1\cdots j_F}    \prod_\kappa  \left| \phi_{j_\kappa}^{(\kappa)}(q_\kappa)\right\rangle   \left\langle \phi_{i_\kappa}^{(\kappa)}(q_\kappa)\right| 
\end{align}

with 


\begin{align}
O_{i_1\cdots i_f, j_1\cdots j_F}  =    \prod_\kappa  \left\langle \phi_{i_\kappa}^{(\kappa)}(q_\kappa)\right| \hat{O} \prod_\kappa  \left| \phi_{i_\kappa}^{(\kappa)}(q_\kappa)\right\rangle
\end{align}

The size of the matrix representation scales as 

\begin{align}
N^2 &= \prod_\kappa N_\kappa^2
\end{align}

So one usually does not want to build the complete matrix as it will likely be very large.

+++

## 2D example: the Henon-Heiles potential

We want to compare various ways of treating multidimensional quantum system, in particular the numerically exact **standard approach** and the time-dependent Hartree method (**TDH**). Although the interest lies in treating polyatomic molecules with $3N-6$ internal degrees of freedom (typically $N \geq 3$), we focus here on two-dimensional systems where the **standard approach** can still be readily employed. For this, we consider the so-called Henon-Heiles potential. It is very famous and often used for benchmarking because it looks particularly simple and is easy to implement, yet exhibits chaotic behavior. 

The potential has the form

\begin{align}
 V(x,y) = \frac12 \left( x^2 + y^2\right) + \frac{\alpha^2}{16}\left( x^2 + y^2\right)^2    + \alpha  \left( xy^2 -\frac{x^3}{3}  \right)
\end{align}

with parameter $\alpha$ that determines the deviation from the two-dimensional harmonic oscillator. 

The Hamiltonian of the system reads

\begin{align}
H = -\frac{1}{2m_x} \frac{\partial^2}{\partial x^2} -\frac{1}{2m_y} \frac{\partial^2}{\partial y^2} + V(x,y) 
\end{align}

+++

To model the a system we need to set up a 2D grid of points. Conveniently we can use the points of two DVR grids and combine them into 2D meshgrids. Keep in mind that the potential energy matrix $\mathbf{V}$ is diagonal in the DVR basis!

```{code-cell} ipython3
import numpy as np

from scipy.sparse.linalg import LinearOperator
from scipy.linalg import eigh_tridiagonal, eigh
from scipy.integrate import ode

from numpy.linalg import svd

import matplotlib.pyplot as plt
from ipywidgets import interact

from dvr import *

plt.rcParams["figure.figsize"] = (8,5)
```

```{code-cell} ipython3
N=70

massX=1
massY=1
omegaX=1
omegaY=1

hoX = hoDVR(N, mass=massX, eq=0, freq=omegaX)
hoY = hoDVR(N, mass=massY, eq=0, freq=omegaY)


# define small helper routines  to remove and apply HO dvr weights
def remove_HO_weights2d(psi):
    
    psitmp = psi.reshape(hoX.N, hoY.N) 
    weights = np.outer(hoX.weights,hoY.weights) 
    return psitmp/weights
    
def apply_HO_weights2d(psi):
    
    psitmp = psi.reshape(hoX.N, hoY.N) 
    weights = np.outer(hoX.weights,hoY.weights) 
    return psitmp*weights
```

```{code-cell} ipython3
def HenonHeilesPotential(x,y):
    alpha = 0.1
    
    pot = 0.5*(x**2 + y**2) + alpha*(x*y**2-x**3/3) + alpha**2/16*(x**2 + y**2)**2
    #pot = 0.5*(x**2 + y**2) 
    return pot
    
X,Y = np.meshgrid(hoX.grid,hoY.grid,indexing="ij")    
Vhh = HenonHeilesPotential(X,Y)
```

```{code-cell} ipython3
mf = 16
fig = plt.figure(figsize=(5,5))
ax = plt.axes() 
ax.contour(X, Y, Vhh, 55,cmap="inferno", linestyles="solid",  origin='lower')
plt.xticks(fontsize=mf)
plt.yticks(fontsize=mf)
ax.set_xlabel('x',fontsize=mf)
ax.set_ylabel('y',fontsize=mf)
ax.set_title('Henon-Heiles potential',fontsize=mf, weight='bold')
plt.show()
```

```{code-cell} ipython3
def HHHamiltonian(psi):

    psitemp = psi.reshape(hoX.N,hoY.N)
    
    hpsi = Vhh*psitemp
    hpsi += -np.einsum('ij,jk->ik', hoX.dif2, psitemp)/(2*massX)
    hpsi += -np.einsum('ij,kj->ki', hoY.dif2, psitemp)/(2*massY)
    
    return hpsi.flatten()
```

```{code-cell} ipython3
%%time 

NHH = hoX.N * hoY.N

H = np.zeros((NHH,NHH), dtype=np.double)

for i in range(NHH):
    phi = np.zeros(NHH, dtype=np.double)
    phi[i] = 1.0
    H[:,i] = HHHamiltonian(phi)
    

Eval, Evec = eigh(H,eigvals_only=False)
print(Eval[0:10])
```

```{code-cell} ipython3
# dynamically create figure
def plot_Evec(n=0):

    mf = 16
    fig, ax = plt.subplots(1, figsize=(5,5))
    #ax = plt.axes(projection='3d')
    
    psiv = Evec[:,n].copy()
    
    psiv = remove_HO_weights2d(psiv)
    
    #ax.matshow(psi, interpolation='nearest',cmap=cm.inferno, origin='lower')
    plt.pcolor(X,Y,psiv**2, cmap='hot')
    
    ax.set_title('', fontsize=mf)
   

    ax.set_xlabel(r'x [a$_0$]', fontsize=mf)
    ax.set_ylabel(r'y [a$_0$]', fontsize=mf)
  
    plt.show()
    
    
interact(plot_Evec, n=(0, 50, 1))
```

```{code-cell} ipython3
%%time 

# propagator
t0 = 0.0 
dt = 0.1
tsteps = 200

propagator = np.diag(np.exp(-1j * Eval * dt ) )


# trafo to grid basis
propagator2 = Evec @ (propagator @ Evec.T)


# store intermediate results
psigrid = np.zeros((tsteps+1,NHH), dtype=complex)
tgrid = np.zeros(tsteps+1)
autocorr_sm = np.zeros((tsteps+1), dtype=complex)

# initial state
x0 = 1.5
y0 = 1.0
psi0hh = np.exp(-(X-x0)**2/4)* np.exp(-(Y-y0)**2/5) 
psi0hh = apply_HO_weights2d(psi0hh)
norm = np.vdot(psi0hh,psi0hh)
psi0hh /= np.sqrt(norm)

psit = psi0hh.flatten().copy()

psigrid[0,:] = psit
tgrid[0] = 0.0


# propagation
for i in range(1,tsteps+1):
    psit = propagator2 @ psit
    psigrid[i,:] = psit
    tgrid[i] = tgrid[i-1] + dt
    # check autocorrelation
    autocorr_sm[i-1] = psigrid[0,:].conj().T @ psit
```

```{code-cell} ipython3
# dynamically create figure
def plot_propagation(n=0):

    mf = 16
    #fig = plt.figure(figsize=(10,8))
    #ax = fig.gca(projection='3d')
    
    
    psihh = psigrid[n,:].copy()
    psihh = remove_HO_weights2d(psihh)
    
    
    #ax.plot_surface(X, Y, abs(psihh)**2, cmap = cm.viridis,rstride=1, cstride=1,edgecolor='none')

    #plt.xticks(fontsize=mf)
    #plt.yticks(fontsize=mf)
    #ax.zaxis.set_tick_params(labelsize=mf)
    #ax.set_xlabel('x',fontsize=mf)
    #ax.set_ylabel('y',fontsize=mf)
    #ax.set_zlabel('V',fontsize=mf)
    
    fig, ax = plt.subplots(figsize=(5,5))
    plt.pcolor(X,Y,abs(psihh)**2, cmap='hot')
    ax.set_title(r'$\psi(t)$, $t=${:.1f}'.format(tgrid[n]),fontsize=mf)
    ax.set_xlabel(r'x [au]',fontsize=mf)
    ax.set_ylabel(r'y [au]',fontsize=mf)
    plt.show()
    
    
interact(plot_propagation, n=(0, tsteps-1, 1))
```

```{code-cell} ipython3
plt.plot(autocorr_sm.real,label='Standard Re a(t)')
plt.plot(autocorr_sm.imag,label='Standard Im a(t)')
plt.xlabel('t (au)',fontsize=mf)
plt.ylabel('a(t)',fontsize=mf)
plt.show()
```

## Mitigating the curse of dimensionality
We can see above that the wavefunction and the potential are really stored as $F$-dimensional arrays with a fixed number of indices (dimensions) and a fixed number of elements per index which gives rise to the exponential scaling law. This kind of object is often called a tensor in information-science. Unlike in physics or mathematics where a tensor is describing algebraic relations between other objects (like a stress tensor, susceptibility and alike), here we really treat it as a multi-dimensional object with the mentioned index-structure. 

We can write the Standard method wavefunction in diagrammatic tensor notation as follows:
<center><img src="standard_method.png" style="width:40%" ></center>

This ansatz is *numerically exact*, i.e. its accuracy is only limited by the underlying basis (typically DVR grid) in which each degree of freedom is represented. In MCTDH, we refer to this underlying basis as the primitive basis.

+++

### Tensor decomposition
A *tensor decomposition* tries to "fragment" such a high-dimensional object into low-dimensional objects that can be handled much easier. The curse of dimensionality is mitigated. 

Consider a two-dimensional wavefunction $\psi(x,y)$ represented on DVR grids $\lbrace x_{j_1}\rbrace$ and $\lbrace y_{j_2}\rbrace$  at first. We can try and write the wavefunction as a weighted sum of two one-dimensional wavefunctions
\begin{align}
    \psi(x_{j_1},y_{j_2}) \approx \sum_{r=1}^R \sigma_r u_r(x_{j_1})v_r(y_{j_2}).
\end{align}
In tensor notation we can write this approximation as 
<center><img src="svd.png" style="width:30%"></center>

The exact representation in this format is given by the Singular Value Decomposition (**SVD**) of a matrix,
\begin{align}
    C_{j_1j_2} = \sum_{r=1}^{R'} \sigma_r U_{j_1r}V_{j_2r},
\end{align}
where the singular values $\sigma_r$ are the square-roots of the non-zero eigenvalues of $\mathbf{C}\mathbf{C}^{\dagger}$ (or equivalently $\mathbf{C}^{\dagger}\mathbf{C}$). If we only have to sum over a few terms (small *rank* $R$ < $R'$) to obtain a reasonably accurate representation of $\psi(x_{j_1},y_{j_2})$, we have found a more compact (less memory consuming) representation of $\psi(x_{j_1},y_{j_2})$! 
Note that setting $R=1$ corresponds to a *Hartree product ansatz* for the 2D system.

+++

Let us investigate such tensor decompositions for 2D systems. A grey-scale picture serves as a example 2D tensor $C_{j_1j_2}$. The index $j_1,j_2$ identifies the position of a pixel with corresponding numerical value $C_{j_1j_2}$ between 0 (black) ad 1 (white).

```{code-cell} ipython3
# cat picture from 
# https://stackabuse.com/introduction-to-image-processing-in-python-with-opencv

img = plt.imread('cat.png')


print(type(img))
print(img.dtype)
print(img.shape)


plt.rcParams["figure.figsize"] = (9.2,14.4)
plt.imshow(img, cmap='gray')
plt.show()
```

```{code-cell} ipython3
# singular value decomposition with numpy.linalg.svd

U, S, Vh = svd(img, full_matrices=True)
print(U.shape)
print(S.shape)
print(Vh.shape)
```

```{code-cell} ipython3
from ipywidgets import interact
mf = 16

def svd_reconstruct(R=1):
    rec = np.matmul(U[:,:R]*S[:R], Vh[:R,:])
    err = np.sqrt(sum(S[R:]**2))
    N1=U.shape[0]
    N2=Vh.shape[1]
    perc = R*(N1+N2)/N1/N2*100
    plt.imshow(rec, cmap='gray')
    plt.text(1,-10, 'R={},  Err={:6.3}, storage={:10.3}%'.format(R,err,perc), fontsize=mf)
    plt.show()
    
interact(svd_reconstruct, R=(1, 400, 1))    
```

### Questions
* What can you observe when you increase the rank $R$?
* How does the memory usage of the SVD scale in comparison with the full tensor $C_{j_1j_2}$?
* What is the maximum rank for a $N\times M$ matrix?

+++

## Tensor decomposition for higher-order tensors

There is no unique extension of the SVD to higher-order tensor with $F>2$ indices. Here, we introduce the so-called **Tucker decomposition**, 
\begin{align}
    C_{j_1j_2\dots j_F} = \sum_{r_1,r_2,\dots,r_F=1}^{R_1,R_2,\dots,R_F} A_{r_1,r_2,\dots,r_F} C^{(1)}_{j_1r_1}C^{(2)}_{j_2r_2}\dots C^{(F)}_{j_Fr_F},
\end{align}
which can be represented graphically as:
<center><img src="tucker.png" style="width:25%"></center>

This is equivalent to the **MCTDH** (multiconfiguration time-dependent Hartree) wavefunction ansatz. 

The time-dependent Hartree (**TDH**) approach is a special case of the Tucker decomposition, in which all $R_i$ are set to 1 (rank-1 decomposition). This reduces the so-called core tensor $A_{r_1,r_2,\dots,r_F}$ to a scalar $A_{1,1,\dots,1}$.

<center><img src="tdh.png" style="width:25%"></center>
\begin{align}
    C_{j_1j_2\dots j_F} = A_{1,1,\dots,1} C^{(1)}_{j_11}C^{(2)}_{j_21}\dots C^{(F)}_{j_F1}
\end{align}

+++

# Correlation

A function or tensor is said to be **uncorrelated** if an exact  Rank-1 decomposition exists (rank-1: all $R_i=1$ in the various contexts). Note that all decompositions above are the same in this case and are just a single outer product of vectors (if the method to create the decomposition can calculate those vectors is a different question). 

A function or tensor is said to be **correlated** if an exact Rank-1 decomposition does not exist. By dropping terms one hence loses correlation. Correlation gives rise to many quantum effects (entanglement, dephasing, etc.).

For $F=2$, we can always decompose the numerically exact wavefunction -represented by the full-rank tensor $C_{j_1j_2}(t)$- exactly in SVD format. In MCTDH terms, the SVD corresponds to an exact multiconfigurational expansion of the wavefunction where each configuration is represented by the outer product $U_{j_1r}V_{j_2r}$ and diagonal A-vector. The contribution of each configuration is simply given by $\sigma_r$, the populations by $\sigma_r^2$. A low-rank approximation (which would choose $R$ smaller than the full rank $R'$) is only accurate if the populations of higher configurations are sufficiently small.

+++

Let us assess the correlation of the Standard approach wavefunction in the 2D Henon-Heiles system.

```{code-cell} ipython3
svals = []
for i,psi in enumerate(psigrid):
    psitmp = psi.reshape((hoX.N,hoY.N))
    U, S, Vh = svd(psitmp, full_matrices=False)
    svals.append(S)
```

```{code-cell} ipython3
svalt = np.array(svals)

for i in range(min(hoX.N,hoY.N)):
    plt.plot(svalt[:,i]**2)
    
plt.rcParams["figure.figsize"] = (6,5)
plt.xlabel('t (au)',fontsize=mf)
plt.ylabel(r'$\sigma_r^2(t)$',fontsize=mf)
plt.show()
```

### Questions
* How does the behavior of $\sigma_r^2$ change for a Henon-Heiles system with $\alpha=0$? What changes for larger values of $\alpha$? Explain.
* In which regime do you expect accurate results from the TDH (Time-dependent Hartree) method? When will it fail?
* How does the memory consumption scale for the TDH ansatz and MCTDH (Tucker) ansatz?

+++

## Time-dependent Hartree

With the Hartree product wavefunction ansatz at hand, we can derive equations of motion for the constituents of the ansatz. To this end we use a variational principle, more precisely the Dirac-Frenkel variational principle:

\begin{align}
 \left\langle \delta \psi\right|H -  i\frac{\partial}{\partial t}\left| \psi \right\rangle = 0 
\end{align}

Plugging in the Hartree product ansatz 
\begin{align}
  \psi(q_1,q_2,\dots,q_F,t) = A(t)\phi_1(q_1,t)\phi_2(q_1,t)\dots\phi_F(q_f,t),
\end{align}
we can derive the equations of motion for $A(t)$ and the $\phi_j(q_j,t)$'s.
Note that this wavefunction ansatz is not unique, since we can shift complex factors among $A(t)$ and the *single particle functions* $\phi_j(q_j,t)$. To remove this non-uniqueness we impose the constraint 
\begin{align}
    i\langle \phi_j(t)|\dot\phi_j(t)\rangle = 0.
\end{align}
This constraint effectively minimizes the time derivative of single particle functions, moving fast oscillations to $A(t)$.

We obtain the EOMs:
\begin{align}
    i\dot A &= \langle H \rangle A \\
    i\dot\phi_j &= \left(\langle H \rangle^{j} - \langle H \rangle\right)\phi_j\\
                &= \left(1 - \left| \phi_j \right\rangle\left\langle\phi_j\right|\right) \left\langle H \right\rangle^{j}\phi_j         
\end{align}
with (time-dependent) **mean-fields**
\begin{align}
    \langle H \rangle^{j} = \langle \phi_1\dots\phi_{j-1} \phi_{j+1}\dots\phi_F|\hat H|\phi_1\dots\phi_{j-1} \phi_{j+1}\dots\phi_F\rangle.
\end{align}
In principle, evaluation of $\langle H \rangle^{j}$ involves the computation of high-dimensional ($F-1$ dimensions) integrals. This challenge can be overcome if the Hamiltonian is in **sum-of-products** (SOP) form, turning a single high-dimensional into many 1D integrals.


Here we use a human-readable tableau-style input format to setup the Henon-Heiles Hamiltonian (as used previously) in SOP form which is similar to the format used in the MCTDH Heidelberg package.

```{code-cell} ipython3
from sopoper import *
```

```{code-cell} ipython3
opertable = """
# coeff       | x      | y
# --------------------------------------------
-0.5/massX     | dq^2    | 1
-0.5/massY     | 1       | dq^2
 0.5           | q^2     | 1
 0.5           | 1       | q^2
 alpha**2/16   | q^2*q^2 | 1
 alpha**2/16   | 1       | q^2*q^2
 alpha**2/8    | q^2     | q^2
 alpha         | q       | q^2
 -alpha/3      | q^2*q   | 1
"""

N=70

massX=1
massY=1
omegaX=1
omegaY=1

alpha = 0.1

hoX = hoDVR(N, mass=massX, eq=0, freq=omegaX)
hoY = hoDVR(N, mass=massY, eq=0, freq=omegaY)

# store the constants in a parameters dictionary
parameters ={}
parameters["alpha"] = alpha
parameters["massX"] = massX
parameters["massY"] = massY
# create the numerical operator table 
hhsop = txt2oper(opertable, (hoX,hoY), parameters )
```

```{code-cell} ipython3
def matvec(psi, hsop):
    psitmp = psi.reshape((hoX.N,hoY.N))
    
    # space for the result of the matrix 
    # vector multiplication
    hpsi = np.zeros(((hoX.N,hoY.N)), dtype=complex)
    # loop through all lines in the operator table
    # which now holds coefficients and op1d instances
    for line in hsop:
        
        # operate coefficient
        # new h1psi for each line
        h1psi = line[0] * psitmp
        
        # operate 1d operators
        # loop through the indices of psi
        for f, term in enumerate(line[1:]):
            
            # do a re-shape of psi such that
            # the operator always operates on the 
            # middle index. this way one does not 
            # have to fiddle around with what operator
            # is operating where. 
            
            #bef = int(np.product(psitmp.shape[:f]))
            #aft = int(np.product(psitmp.shape[f+1:]))
            #gdm =  int(psitmp.shape[f])
               
            #h1psi =  h1psi.reshape(bef, gdm)
            h1psi = np.einsum("ij,kj -> ik",term.hterm, h1psi)
            #h1psi =  h1psi.reshape((hoX.N,hoY.N))
            
        # one line is done, add result to hpsi
        hpsi +=  h1psi
        
    # finally apply the potential
    # hpsi += V*psitmp
        
    return hpsi.flatten()
```

What follows is an implementation of the TDH EOMs in python:

```{code-cell} ipython3
# some helper routines: 

# The constituents of the wavefunction will be mapped into
# one single 1D array. This is because the scipy ODE integrator 
# expects a single 1D array. 

# FORMAT:
# psi[0]      : a(t)
# psi[1:N1+1] : phi_1 etc. 


def norm_tdh(psi, dims):
    "norm of a TDH wavevunction"
    
    norm2 = np.conj(psi[0])*psi[0]
    
    gi = 1 
    for f,n in enumerate(dims):
        gf = gi + n
        norm2 *= np.vdot(psi[gi:gf],psi[gi:gf])
        gi += n
        
    return sqrt(norm2)
    

def normalize_tdh(psi, dims):
    "return normalized TDH wavevunction" 
    
    norm2 = np.conj(psi[0])*psi[0]
    psi[0] /= np.sqrt(norm2)
    
    gi = 1 
    for f,n in enumerate(dims):
        gf = gi + n
        norm2 = np.vdot(psi[gi:gf],psi[gi:gf])
        psi[gi:gf] /= np.sqrt(norm2)
        gi += n
            
    return psi
    


def meanfields(psi, dims, hamilton):
    "TDH mean fiels <H> and <H>^(i)"
    
    nterms = len(hamilton) # number of operator lines 
    ndims  = len(dims)     # number of coordinates
   
    # for speed just map the fractions of 
    # psi that correspond to a phi into 
    # an array phi and store them in a list
    phis = []
    gi = 1
    for f,n in enumerate(dims):
        gf = gi + n
        phis.append( psi[gi:gf])
        gi += n
    
    # this is space for all <phi_j|h_j^r|phi_j>
    # also the coefficients of H do in there in column 0
    expects = np.zeros((nterms,ndims+1), dtype=np.double)
    
    # now loop through the operator table and 
    # calculate all <phi_j|h_j^r|phi_j>
    for k, term in enumerate(hamilton):
        
        expects[k,0] = term[0]
        
        for f,phi in enumerate(phis):
            z = f+1
            expects[k,z] = term[z].expect(phi)

    # calculates <H>: this multiplies
    # all rows in expects which gives 
    # prod_j <phi_j|h_j^r|phi_j>
    # and then sums over r 
    phihphi = expects.prod(axis=1).sum()
       
    # now for the mean fields. The 
    # mean fields of coordinates f
    # are a single 1D operator
    # 
    # we need to evaluate  
    # \prod_{j!=f} <phi_j|h_j^r|phi_j>  h_f^r
    # and then sum over r
    # 

    fops = []
    
    for f in range(len(dims)):
        
        # so first do \prod_{j!=f} <phi_j|h_j^r|phi_j> 
        # this is just the full product with element f set to 1
        
        phihphi_f = expects.copy()    # copy, dont want to change the original
        phihphi_f[:,f+1] = 1          # set f'th element to one
        phihphi_f = phihphi_f.prod(axis=1) # product
        
        fop = np.zeros((dims[f],dims[f]), dtype=np.double)
        
        # now the r - summation. Just add matrices 
        # along the operator table
        
        z=f+1
        for k,term in enumerate(hamilton):
            
            fop += term[z].hterm*phihphi_f[k]
            
        # create a 1d operator in the end
        fops.append(op1d(dims[f], fop))
        
    
    return phihphi, fops
```

```{code-cell} ipython3
def psidot_tdh(t, psi, dims, hamilton, update=False):
    

    phihphi, mfields = meanfields(psi, dims, hamilton)
        
        
    psidot = np.zeros(len(psi), dtype=complex)
    psidot[0] = -1j * phihphi * psi[0]
    
    gi = 1 
    for f,n in enumerate(dims):
        gf = gi + n
        psidot[gi:gf] =   -1j * (mfields[f].operate(psi[gi:gf]) - phihphi*psi[gi:gf])
        gi += n
    
    return psidot
    
    
def autocorr(psi0,psit, dims):
    
    auto = np.conj(psi0[0])*psit[0] # = 1
    gi = 1 
    for f,n in enumerate(dims):
        gf = gi + n
        auto *= np.vdot(psi0[gi:gf],psit[gi:gf])
        gi += n
        
    return auto
    
```

We set up the initial WF as a Hartree product (use the same initial state in TDH and standard approach runs).

```{code-cell} ipython3
# initial wf
x0 = 1.5
y0 = 1.0
psiX = np.exp(-(hoX.grid-x0)**2/4)
psiY = np.exp(-(hoY.grid-y0)**2/4)
```

```{code-cell} ipython3
# calculate the length of the psi-arary 

psidim = 1 + hoX.N + hoY.N

psi_tdh = np.zeros(psidim, dtype=complex)

# a(t)
psi_tdh[0] = 1.0

# phi_1(t)
gi = 1
gf = gi + len(hoY.grid)
psi_tdh[gi:gf] = psiX
gi = gi + len(hoX.grid)

# phi_2(t)
gf = gi + len(hoY.grid)
psi_tdh[gi:gf] = psiY
gi = gi + len(hoY.grid)


dims = (hoX.N, hoY.N)

psi_tdh = normalize_tdh(psi_tdh, dims)
```

```{code-cell} ipython3
%%time
# set up the integrator - it needs the routine to calculate 
# the derivative, the ODE type (zvode=complex-valued 
# ordinary differential equation) and the integrator 'adams'
# in this case (uses information from previous propagation steps)

t0 = 0
tfinal = 20
tsteps_tdh = 200
dt = tfinal/tsteps_tdh


# store intermediate results
psitmp_tdh = np.zeros((tsteps_tdh+1,psidim), dtype=complex)
tgrid_tdh = np.zeros(tsteps_tdh+1)


psitmp_tdh[0] = psi_tdh # t=0 
tgrid_tdh[0] = t0

auto_tdh = np.zeros(tsteps_tdh+1, dtype=complex)
auto_tdh[0] = autocorr(psi_tdh,psi_tdh, dims)


r = ode(psidot_tdh).set_integrator('zvode', method='adams')
r.set_initial_value(psi_tdh, t0).set_f_params(dims,hhsop)

i=1
# loop through time steps
while r.successful() and r.t < tfinal and i <= tsteps_tdh:
    psitmp_tdh[i]= r.integrate(r.t+dt)
    auto_tdh[i] =  autocorr(psitmp_tdh[0],psitmp_tdh[i], dims)
    tgrid_tdh[i]= r.t
    i+=1
```

```{code-cell} ipython3
plt.plot(tgrid_tdh,auto_tdh.real,label='TDH Re a(t)')
plt.plot(tgrid,autocorr_sm.real,label='Standard Re a(t)')

plt.plot(tgrid_tdh,auto_tdh.imag,ls='--',label='TDH Im a(t)')
plt.plot(tgrid,autocorr_sm.imag,ls='--',label='Standard Im a(t)')

plt.xlabel('t (au)',fontsize=mf)
plt.ylabel('a(t)',fontsize=mf)
plt.legend(loc='best')

plt.rcParams["figure.figsize"] = (8,5)
plt.show()
```

## Questions
* Validate your predictions on the accuracy of TDH which you made in the previous section.

```{code-cell} ipython3

```
