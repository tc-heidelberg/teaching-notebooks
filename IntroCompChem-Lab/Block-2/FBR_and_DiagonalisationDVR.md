---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

+++

# Diagonalisation DVR
https://doi.org/10.1016/S0370-1573(99)00047-2 (Appendix B)

```{code-cell} ipython3
import numpy as np
import scipy.integrate as spi
from scipy import linalg as la
from scipy.special import hermite
from scipy import interpolate

import matplotlib.pyplot as plt

plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["xtick.labelsize"] = 14
plt.rcParams["ytick.labelsize"] = 14
```

## From VBR to FBR

+++

In the variational basis representation (**VBR**) the Hamiltonian is projected onto a truncated set of basis functions $\lbrace \phi_j(x) \rbrace_{j=1}^{N}$. 

\begin{align}
 \hat{H} \rightarrow P\hat{H}P &=  P\hat{T}P +  P\hat{V}P \\
  &= \sum_{jk}^{N}  \sum_{jk}^{N}\left|\phi_j\right\rangle T_{jk}^{\mathrm{VBR}} \left\langle\phi_k\right| +   \sum_{jk}^{N}  \sum_{jk}^{N}\left|\phi_j\right\rangle V_{jk}^{\mathrm{VBR}} \left\langle\phi_k\right|
\end{align}

Evaluation of the kinetic energy operator matrix elements $T_{jk}^{\mathrm{VBR}}$ is not problematic for we require the matrix elements $\langle \phi_j|\hat T|\phi_k\rangle$ to be known *analytically*. Far more problematic is the potential energy operator matrix elements $V_{jk}^{\mathrm{VBR}}$, since it involves evaluation of (typically) high-dimensional integrals.
In fact, the computation of these matrix elements by accurate numerical integration may take considerable more computational time than the full diagonalisation of the Hamilton matrix. In the following, we assume a one-dimensional coordinate $x$ for clarity. Note however that the interest lies in treating multi-dimensional systems.\
\
To circumvent this problem we introduce the finite basis representation (**FBR**). In FBR, we approximate the potential as a function of the position operator matrix $\mathbf{Q}$,
\begin{align} 
     \mathbf{V}^{\mathrm{FBR}} = V(\mathbf{Q}) = V(P\hat x P).
\end{align}
with $Q_{jk}=\langle \phi_j|\hat x|\phi_k\rangle$.\
Since the truncated basis is not complete ($P\neq \mathbf 1$) this is only exact for linear potential energy functions, see e.g.
\begin{gather}
    \hat V  = x^2 \\
    \hat V^{\mathrm{VBR}} = P\hat VP = P\hat x^2P \\
    \hat V^{\mathrm{FBR}} = V(P \hat x P) = P \hat x P \hat x.
\end{gather}

Why is this useful? To evaluate $V(\mathbf{Q})$ we diagonalize $\mathbf{Q}$,
\begin{align}
    \mathbf{Q} = \mathbf{U}\mathbf{X}\mathbf{U}^{\dagger},
\end{align}
where $\mathbf{U}$ denotes the eigenvector matrix and $\mathbf{X}$ is the diagonal matrix of eigenvalues $x_{\alpha}$, i.e. $X_{\alpha\beta}=x_{\alpha}\delta_{\alpha\beta}$. 
With this at hand, we can simply evaluate $\mathbf{V}^{\mathrm{FBR}}$ through
\begin{align}
    V_{jk}^{\mathrm{FBR}} = \sum_{\alpha=1}^N U_{j\alpha}V(x_\alpha)U_{k\alpha}^{*}.
\end{align}
We have turned the potentially troublesome numerical integration in  $\langle \phi_j|\hat V|\phi_k\rangle$ into a simple sum, involving the potential evaluated at a $N$ grid points $\lbrace x_{\alpha}\rbrace$! These grid points are obtained as the eigenvalues of the position matrix $V(\mathbf{Q})$. 
In fact, if $\mathbf{Q}$ is a tridiagonal matrix, we can rewrite 
\begin{align}
    V_{jk}^{\mathrm{FBR}} = \sum_{\alpha=1}^N w_{\alpha}\phi_{j}(x_\alpha)V(x_\alpha)\phi_{k}^{*}(x_\alpha),
\end{align}
which is a *Gaussian quadrature* rule with weights $w_\alpha^{1/2}= \phi_{k}^{*}(x_\alpha)/U_{k\alpha}^{*}$. If $V(x)$ is of polynomial degree $l$, this ensures numerically exact integrals over $\phi_j(j)\hat{V}(x) \phi_k(x)$ if $j+k+l \leq 2N+1$. For higher-order polynomials an error is made.


To summarize what we have found so far:
### VBR (Variational Basis representation)

* $\left\langle\phi_j\right| \hat{T} \left|\phi_k\right\rangle$ known analytically
* $\left\langle\phi_j\right| \hat{V} \left|\phi_k\right\rangle$ numerically exact
* Basis truncation error
* Description of system is improved with lager number of basis functions (guaranteed)

### FBR  (Finite Basis representation)

* $\left\langle\phi_j\right| \hat{T} \left|\phi_k\right\rangle$ known analytically
* $\left\langle\phi_j\right| \hat{V} \left|\phi_k\right\rangle$ by $N$-point quadrature where $N$ is the number of basis functions.
* the quadrature points are obtained from diagonalizing the position operator matrix
* Representation of operators and wavefunction in $ \left|\phi_k\right\rangle$-basis
* Basis truncation error
* Quadrature error

+++

Let's investigate this in practice. We pick the first $N$ harmonic oscillator (HO) eigenfunctions as our $\lbrace \phi_j(x) \rbrace_{j=1}^{N}$ basis.

```{code-cell} ipython3
# atomic units
hbar = 1.0
m = 5.0
omega = 2.0

# set number of points for numerical approximation
steps = 250
L = 20

dx = L/(steps + 1)
# grid along x
L2 = L/2-dx
xgrid = np.linspace(-L2,L2,steps)

# number of basis functions
Nb = 10
```

```{code-cell} ipython3
def evec_ho(xgrid,L, m, omega,N=20):
    "create the analytical HO functions"
    steps=len(xgrid)          # number of points
    momega = m*omega
    ww = np.empty((steps,N))
    for n in range(min(N, steps)):
        ww[:,n] = np.exp(-momega*xgrid**2/2)*hermite(n)(np.sqrt(momega)*xgrid)
        ww[:,n] = ww[:,n]/ np.sqrt((ww[:,n]**2).sum())
    return ww
```

And define a function to evaluate matrix elements $\int dx \phi_j(x)f(x)\phi_k(x)$.

```{code-cell} ipython3
def f_jk(j,k,f):
    return spi.simpson(pbasis[:,j]*f*pbasis[:,k])
```

Build the position operator matrix $\mathbf{Q}$ in this basis. Numerical integration is not needed, as the exact analytical expressions $\langle\phi_j|\hat x|\phi_k\rangle$ are known.

```{code-cell} ipython3
pbasis = evec_ho(xgrid,L,m,omega,N=Nb)
Q    = np.zeros((Nb,Nb))

for j in np.arange(0,Nb-1):
    Q[j,j+1] = np.sqrt((j+1)/(2*m*omega))
for j in np.arange(1,Nb):
    Q[j,j-1] = np.sqrt(j/(2*m*omega))

plt.matshow(Q)
plt.show()
```

For HO eigenfunctions, the position operator matrix $\mathbf{Q}$ is **tridiagonal** which guarantees several nice properties (not going into details). \
Let's diagonalize $\mathbf{Q}$ and inspect the eigenvalues, i.e. grid points $x_\alpha$.

```{code-cell} ipython3
X, U = la.eigh(Q)
weights = np.zeros(Nb)

# fix sign of eigenvectors
xx = pbasis @ U 
for i in range(Nb):
    a = 0
    # chi_alpha(x_alpha)
    chiii = interpolate.interp1d(xgrid,xx[:,i])
    if chiii(X[i]) < 0:
        U[:,i] = -1.0*U[:,i]

# determine weights w_alpha^0.5 for quadrature
for a in range(Nb):
    i = 0
    phii = interpolate.interp1d(xgrid,pbasis[:,i])
    wa = U[i,a]/phii(X[a]) 
    weights[a] = wa
```

```{code-cell} ipython3
mf = 14
fig, ax = plt.subplots(figsize=(5,3))
ax.scatter(X,np.zeros(Nb))

#for i in range(3):
#   ax.plot(xgrid,pbasis[:,i],ls='--')

ax.set_xlim(-2.5,2.5)
ax.set_xlabel('x',fontsize=mf)
plt.show()
```

### Questions
* What happens if you increase the number of basis functions $N$?
* What can you say about the grid points $x_\alpha$? Compare with the grid-based approach which would employ the linspace `xgrid`.


Let's test the accuracy of matrix elements of various potential energy functions $V(x)$.

```{code-cell} ipython3
def V0(x):
    return x

def V1(x):
    return 0.5*m*omega**2*x**2

def V2(x):
    return  x**3

def V3(x):
    return 0.5*m*omega**2*x**2 + 150*np.exp(-x**2)

plt.plot(xgrid,V1(xgrid))
plt.plot(xgrid,V2(xgrid))
plt.plot(xgrid,V3(xgrid))
plt.show()
```

```{code-cell} ipython3
V_VBR = np.zeros((Nb,Nb))
V_FBR = np.zeros((Nb,Nb))

for j in range(Nb):
    for k in range(Nb):
        V_VBR[j,k] = f_jk(j,k,V0(xgrid))

V_FBR = U @ np.diag(V0(X)) @ U.T
```

```{code-cell} ipython3
fig, ax = plt.subplots(1,3,figsize=(3*5,5))
ax[0].matshow(V_VBR)
ax[1].matshow(V_FBR)
cs = ax[2].matshow(abs(V_FBR-V_VBR))

ax[0].set_title(r'$V^{\mathrm{VBR}}$',fontsize=mf+2)
ax[1].set_title(r'$V^{\mathrm{FBR}}$',fontsize=mf+2)
ax[2].set_title(r'$|V^{\mathrm{VBR}}-V^{\mathrm{FBR}|}$',fontsize=mf+2)
fig.colorbar(cs)
plt.show()
```

### Questions
* What general trend can you observe for the accuracy of $V^{\mathrm{FBR}}$? 
* How does this depend on the polynomial order of $\hat V(x)$?

+++

## From FBR to DVR

Let us define a new basis by unitarily transforming the FBR basis $\lbrace \phi_j(x) \rbrace_{j=1}^{N}$ with the matrix $\mathbf U$,
\begin{align}
    \chi_{\alpha}(x) = \sum_{j=1}^N \phi_j(x)U_{j\alpha}.
\end{align}

These functions are orthonormal, $\langle\chi_{\alpha}|\chi_{\beta}\rangle$ and diagonalise -by construction- the position operator $\langle\chi_{\alpha}|\hat x|\chi_{\beta}\rangle=x_{\alpha}\delta_{\alpha\beta}$.

How do the new functions $ \chi_{\alpha}(x)$ look like? Let's check.

```{code-cell} ipython3
chis =  pbasis @ U
fig, ax = plt.subplots(figsize=(8,5))

ax.scatter(X,np.zeros(Nb),c='black')

for i in range(Nb):
    ax.plot(xgrid, chis[:,i])
    ax.vlines(X[i],-2,2,ls='--')
    
ax.set_xlim(-2.5,2.5)
ax.set_ylim(-1.0,1.0)
ax.set_xlabel('x',fontsize=mf)   
plt.show()  
```

## Questions

* How does a so-called **DVR** (discrete variable representation) function $\chi_\alpha(x)$ behave at $x=x_\alpha$? How does it behave at every other grid point of the basis $\lbrace x_\alpha \rbrace$? We refer to this property as *discrete $\delta$ property*.
* Use the definition of $\chi_\alpha(x)$ and the FBR approximation, 
\begin{align}
    \langle \phi_j|\hat V|\phi_k\rangle \approx V_{jk}^{\mathrm{FBR}} = \sum_{\alpha=1}^N U_{j\alpha}V(x_\alpha)U_{k\alpha}^{*},
\end{align}
to prove that
\begin{align}
    \langle \chi_\alpha|\hat V|\chi_\beta\rangle \approx V_{\alpha\beta}^{\mathrm{DVR}}=V(x_\alpha)\delta_{\alpha\beta}.
\end{align}

+++

What about the DVR representation of the wavefunction? We start from the FBR representation of the wavefunction,
\begin{align}
  \Psi(x) = \sum_{j=1}^N c_j \phi_j(x)\\
  \Psi(x) \rightarrow \vec {\Psi}^{\mathrm{FBR}} = \vec c,
\end{align}
and transform with $\mathbf{U}$ to obtain  $\vec{\Psi}^{\mathrm{DVR}}$,
\begin{gather}
    \vec{\Psi}^{\mathrm{DVR}} = \mathbf{U}^{\dagger}\vec {\Psi}^{\mathrm{FBR}}.
\end{gather}

```{code-cell} ipython3
psi_FBR = np.zeros(Nb)
psi_FBR[0] = 1.0 # HO ground state
psi_x = pbasis @ psi_FBR 
psi_DVR = U.T @ psi_FBR

plt.plot(xgrid,psi_x)
plt.scatter(X,psi_DVR,label='w/ weights')
plt.scatter(X,psi_DVR/weights,label='w/o weights')
plt.xlim(-2.5,2.5)
plt.xlabel('x',fontsize=mf)
plt.ylabel(r'$\Psi$',fontsize=mf)
plt.legend(fontsize=mf)
plt.show()
```

In the DVR basis, the wavefunction is represented by its values on grid points $\lbrace x_\alpha \rbrace$ weighted by $w_\alpha^{1/2}$,
\begin{align}
     \Psi(x) \rightarrow   \vec{\Psi}^{\mathrm{DVR}} =\left(w_1^{1/2}\Psi(x_1),w_2^{1/2}\Psi(x_2),\dots,w_N^{1/2}\Psi(x_N)\right).
\end{align}
This holds only if $\Psi(x)$ lies entirely within the basis set, i.e. $P\Psi = \Psi$.

+++

FBR and DVR are connected through the unitary transformation $\mathbf{U}$. Thus, $T_{\alpha\beta}^{\mathrm{DVR}}$ are known analytically and can be obtained from $\mathbf{U}\mathbf{T}\mathbf{U}^{\dagger}$ (with $ T_{jk}=\langle\phi_j|\hat T|\phi_k\rangle$. This makes the use of a DVR basis particularly attractive. It combines the "best of both worlds", i.e. analytical expressions for the kinetic energy operator matrix elements (cf. VBR) and a diagonal potential energy matrix (cf. grid method).

+++

### DVR  (Discrete variable representation)

* $\left\langle\phi_j\right| \hat{T} \left|\phi_k\right\rangle$ known analytically
* $\left\langle\chi_\alpha\right| \hat{V} \left|\chi_\beta\right\rangle$ is evaluated in the grid basis, i.e., $V_{\alpha\beta} = V(x_\alpha)\delta_{\alpha\beta}$ is diagonal
* Representation of operators and wavefunction in $ \left|\chi_\alpha\right\rangle$-basis
* Basis truncation error
* Quadrature error 
* FBR and DVR are connected by unitary transformation.
