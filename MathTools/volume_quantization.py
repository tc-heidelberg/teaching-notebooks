# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import numpy as np
import scipy as sc
import sympy as sy

from sympy import init_printing; init_printing(use_latex='mathjax')
from sympy import I, pi

# %%
x = sy.Symbol('x', real=True)
k = sy.Symbol('k', real=True)
L = sy.Symbol('L', real=True, positive=True)


# %%
def get_psi_n(n):
    return 1/sy.sqrt(L)*sy.exp(2*I*pi*x*n/L)


# %%
psi_3 = get_psi_n(3)
psi_4 = get_psi_n(4)

# %%
sy.integrate( np.conj(psi_3)*psi_3, (x, 0, L) )

# %%
sy.integrate( np.conj(psi_4)*psi_3, (x, 0, L) )
