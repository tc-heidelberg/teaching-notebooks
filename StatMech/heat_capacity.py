# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
import numpy as np
import matplotlib.pyplot as plt


# %%
# We assume a 2-particle system with states at energy 0 and \epsilon
# Make epsilon = 1 

# %%
def Z(b):
    f = np.exp(-b)
    return 1 + 2*f + f**2


# %%
def U(b):
    f = np.exp(-b)
    z = Z(b)
    return (2*f + 2*f**2)/z


# %%
def Cv(b, ufunc, dx=0.005):
    # Numerical evaluation of Cv using U
    # Take numerical derivative of ufunc(beta) with respect to beta
    dx2 = dx/2
    d = (ufunc(b+dx2) - ufunc(b-dx2))/dx
    # Multiply with dbeta/dT = -beta**2
    C = -d*b**2
    return C


# %%
Cv(1/0.1, U)

# %%
Trange = np.linspace(0.01, 2, 100)
brange = 1/Trange

# %%
zr = Z(brange)

# %%
plt.plot(Trange, Z(brange))

# %%
plt.xlabel('$T/\epsilon_1$', fontsize=16)
plt.ylabel('$U/\epsilon_1$', fontsize=16)
plt.title('Interne Energie', fontsize=16)
plt.plot(Trange, U(brange))
plt.savefig('/users/oriol/Downloads/U.pdf')

# %%
plt.xlabel('$T/\epsilon_1$', fontsize=16)
plt.ylabel('$C_v$', fontsize=16)
plt.title('Wärmekapazität', fontsize=16)
plt.plot(Trange, Cv(brange, U))
plt.savefig('/users/oriol/Downloads/Cv.pdf')

# %% [markdown]
# ## Debye and Einstein at low temperature

# %%
Trange = np.linspace(0.0001, 0.15, 100)
def cv_debye_lT(x):
    cv = 12*np.pi**4*x**3
    return cv
def cv_einst_lT(x):
    cv = 3*(1.0/x)**2*np.exp(-1.0/x)
    return cv


# %%
cvd = cv_debye_lT(Trange)
cve = cv_einst_lT(Trange)

# %%
plt.xlabel('$T/\Theta$', fontsize=16)
plt.ylabel('$C_v/Nk$', fontsize=16)
plt.title('Heat capacity, Einstein vs Debye at low T', fontsize=16)
plt.ylim(-0.01, 0.2)
plt.plot(Trange, cvd, label='Debye')
plt.plot(Trange, cve, label='Einstein')
plt.legend()
plt.savefig('/users/oriol/Downloads/Cv_DvsE.pdf')

# %%

# %%
