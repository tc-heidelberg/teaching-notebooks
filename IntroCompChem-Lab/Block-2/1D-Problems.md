---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import eigh
import scipy.integrate as spi
from ipywidgets import interact

# put the file 'dvr.py' into the same directory as the notebook
# for this to work:
from dvr import *

plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["xtick.labelsize"] = 14
plt.rcParams["ytick.labelsize"] = 14
```

# Vibrational Structure of HCl

+++

## Morse potential

Realistic systems usually have some smooth potential rather than a box potential, for instance the Morse oscillator

\begin{align}
    V(R) = D_{e} \left[ \exp\left(-2\beta(R-R_0)\right) -2  \exp\left(-\beta(R-R_0)\right) \right] + D_e
\end{align}

Here, $D_e$ is the potential depth ($7.41\cdot10^{-19}$ J $\approx 37302$ cm$^{-1}$ for HCl) and $\beta=\sqrt{k/2D_e}$ is something like a "stiffness" of the oscillator. The constant $k=\mu\omega_0^2$ is the force constant of the oscillator in harmonic approximation with harmonic frequency $\omega_0$.

The kinetic energy is in this case given as 
\begin{align}
T & = -\frac{\hbar^2}{2\mu} \frac{\partial^2}{\partial R^2} 
\end{align}
with $R$ the distance between the two atoms and $\mu=\frac{m_1 m_2}{m_1 + m_2}$ the reduced mass of the two particles with mass $m_1$ and $m_2$, respectively 


## Morse parameters for HCl
For HCl the following are used:

\begin{align}
 D_e & = 7.41\cdot10^{-19} \textrm{  J}\\
 \omega_0 & = 2990 \textrm{  cm}^{-1}\\
 m_1 & = 1 \textrm{  u}\\
 m_2 & = 35 \textrm{  u}\\
 R_0 & = 2.9 \textrm{  a}_0\\
\end{align}




The eigenenergies of the bound states are known analytically:
\begin{align}
   E_n = \omega_0\left(n+\frac12\right) - \frac{\omega_0^2}{4D_e} \left(n+\frac12\right)^2
\end{align}

Up to now we have done everything genuinely in atomic units, but for realistic systems we need to do a number of conversions from the SI system to atomic units. Scipy has a comprehensive list of physical constants: https://docs.scipy.org/doc/scipy/reference/constants.html

```{code-cell} ipython3
# get us some physical constants
from scipy.constants import *

# we do everything in atomic units so get some conversion parameters

au2ic = physical_constants["hartree-inverse meter relationship"][0]/100  # 219474.63137   # Hartree to cm-1
E_h = physical_constants['Hartree energy'][0]        # atomic unit of energy  in J (=1 Hartree)
amu = physical_constants['atomic mass constant'][0]  # atomic mass unit 'AMU' in kg (not atomic units)
me = physical_constants['electron mass'][0]          # electron mass in kg (this is one atomic unit)
amu2au = amu/me                                      # amu in electron masses

De = 7.41E-19 / E_h # De in atomic units

omega0 = 2990          # vibrational frequency in cm^-1 (harmonic approximation)
omega0 = omega0/au2ic   # vibrational frequency in Hartree (harmonic approximation)

# reduced mass
def get_mu(m1,m2):
    mu = m1*m2/(m1+m2)
    return mu

# force constant
def get_k(mu,omega0):
    k = mu*omega0**2
    return k

m1 = 1    # H-mass in AMU
m2 = 35   # Cl-mass in AMU
mu = get_mu(m1,m2)*amu2au # reduced mass in atomic units

R0 = 2.9  # equilibrium distance

# we need the force constant k=mu*omega^2
k = get_k(mu,omega0)
beta = np.sqrt(k/(2*De))

# Morse potential
def Morse(x,De,beta,R0):
    xtmp = x - R0
    pot= De*(np.exp(-2.0*beta*xtmp)-2.0*np.exp(-beta*xtmp)) + De
    return pot
```

```{code-cell} ipython3
#overview of the potential
xmin = 1.9
xmax = 10
nsteps = 100
tempx = np.linspace(xmin,xmax,nsteps)
tempV = Morse(tempx,De,beta,R0)

mf = 16
fig, ax = plt.subplots(figsize=(8,5))
ax.plot(tempx,tempV*au2ic)
ax.set_xlabel("x (a$_0$)",fontsize = mf)
ax.set_ylabel("Energy (cm$^{-1}$)",fontsize = mf)
ax.set_ylim(bottom=0,top=100000)
ax.xaxis.set_tick_params(labelsize=mf)
ax.yaxis.set_tick_params(labelsize=mf)
ax.hlines(De*au2ic,tempx[0],tempx[-1],linestyle=':')
hl=5000
ax.arrow(R0,0,0,De*au2ic-hl,head_width=0.2, head_length=hl, fc='k', ec='k')
ax.text(R0, 40000, r"$D_e$", fontsize=mf)
plt.show()
```

We are now able to set up our system on a sin DVR grid. A sin DVR is a diagonalization DVR which employs a set of particle in a box eigenfunctions.

```{code-cell} ipython3
N = 250 # some large number of points for a nice plot
xmin = 1.9
xmax = 10
x0 = (xmin+xmax)/2 # mid point
L = xmax - xmin
dvr = sinDVR(N,L,x0)


V = Morse(dvr.grid,De,beta,R0 )

Hdvr = -dvr.dif2/2/mu + np.diag(V)

Edvr, eigenvecdvr= eigh(Hdvr,eigvals_only=False)

# first 10 analytic results
Eana = np.zeros(10)
for n in range(10):
    Eana[n] = omega0*(n+1/2) - omega0**2/4/De*(n+1/2)**2
    
# in cm-1:
Eana *= au2ic
```

```{code-cell} ipython3
# dynamically create figure
def plot_psi_dvr(n=1):

    # we have eigenstates starting at n=1
    # but the numpy arrays start at index zero
    # so we just rename the n to n-1 in 
    # order to get the correct vectors and energies
    nm1 = n-1
    
    fig, ax = plt.subplots(figsize=(8,5))
    
    #plot 1
    #Column-major order from Fortran leads to the unexpected indexing of the eigenvectors
    #we could also transpose the eigenvectors using transpose() or .T
    ax.plot(dvr.grid, np.abs(eigenvecdvr[:,nm1]+Edvr[nm1])**2)
    ax.plot(dvr.grid, Morse(dvr.grid,De,beta,R0 ))
    ax.set_ylabel(r'$|\Psi|^2$, V [au]',fontsize=mf)
    ax.set_ylim(-.1,.5)
    ax.set_xlabel(r'$x$',fontsize=mf)
    #plt.subplots_adjust(hspace = 0.0)
    plt.show()
    

# interactive plotting. There seems to be a 
# bug in recent ipywidgets versions where this crashes 
# if one moves the slider to fast. 
# (slider can be moved with arrow keys after clicking it)
interact(plot_psi_dvr, n=(1, 35, 1))
```

In the plot above the wavefunction in shifted by the state energy. We can see, that about 23 bound states exist, then the state energy is reaching the dissociation threshold. 

__Note, however, that dissociation is not in our model: the Morse potential is still embedded in our original box we used to define the basis functions.__

## HCl using a VBR basis

Let us build the Hamiltonian matrix using the VBR approach. We know the matrix of the kinetic part analytically, so we only need to build the matrix elements $V_{jk}$. We do this by defining a function that just builds the product $\phi_j(q)V(q)\phi_k(q)$ and feed this function into a scipy integrator.

```{code-cell} ipython3
# set up a function that just multiplies 
# two basis functions and the potential
# this function we will feed into a scipy 
# integrator
def v_phij_phik(x,L,j,k,x0,De,beta,R0):
    
    # numpy arrays start at 0
    # but basis functions start at 1
    j1 = j+1
    k1 = k+1
        
    phij = np.sqrt(2/L)*np.sin(np.pi*j1*(x-x0)/L)
    phik = np.sqrt(2/L)*np.sin(np.pi*k1*(x-x0)/L)
    V = Morse(x,De,beta,R0)
    
    return phij*V*phik
```

```{code-cell} ipython3
from scipy.integrate import quad

Nvbr = 80

# space for <phi_j| V | phi_k> matrix
Vjk = np.zeros((Nvbr,Nvbr), dtype=np.double)

for j in range(Nvbr):
    for k in range(j,Nvbr): # run from j and use symmetry of Vjk
        a = xmin
        b = xmin + L
        
        # calculate the integral: 
        # <phi_j| V | phi_k> = \int_{-L/2}^{L/2} phi_j(x) V(x) phi_k(x) dx
        # I use adaptive quadrature with fixed error (defaults)
        # (maxiter needs to be large for big N)
        val, err = quad(v_phij_phik, a, b, args=(L,j,k,xmin,De,beta,R0))
        Vjk[j,k] = val
        Vjk[k,j] = val  # the matrix is symmetric

# kinetic energy matrix
Tjk = np.zeros((Nvbr,Nvbr), dtype=np.double)

# T is diagonal in the VBR frame and
# the diagonals are just the 
# particle in a box energies
for j in range(Nvbr):
    Tjk[j,j] = ((j+1)*np.pi/L)**2/2/mu
    
 # complete hamiltonian   
Hvbr = Tjk + Vjk
```

Let us diagonalize the Hamiltonian in the VBR basis.

```{code-cell} ipython3
Evbr = np.zeros((10,Nvbr))

for M in range(10,Nvbr):
    H = Hvbr[0:M, 0:M]
    E = eigh(H,eigvals_only=True)
    # store eigenvalues as a function of M
    Evbr[0:10,M] = E[0:10]
```

```{code-cell} ipython3
mf = 16
fig, ax = plt.subplots(figsize=(8,5))
ax.set_xlabel("Number of basis functions",fontsize = mf)
ax.set_ylabel("Eigenstate Energy (cm$^{-1}$)",fontsize = mf)
#ax.set_ylim(bottom=0,top=30000)
ax.xaxis.set_tick_params(labelsize=mf)
ax.yaxis.set_tick_params(labelsize=mf)
for m in range(10):
    ax.plot(range(10,Nvbr), Evbr[m,10:]*au2ic)
    ax.plot(range(10,Nvbr), Eana[m]*np.ones(Nvbr-10), linestyle=':')


plt.show()
```

The solid lines show the calculated state energies as a function of the number of basis functions used to build the Hamiltonian matrix. The dashed lines are the analytic results. 

## Questions
* How do the VBR eigenstate energies behave with growing number of basis functions? 

# HCl using a DVR basis
## Task
* Create the same plot as above with a DVR basis.
* How do the DVR eigenstate energies behave with growing number of basis functions? Compare to the VBR approach.

```{code-cell} ipython3
# set up and diagonalize H for various numbers of DVR basis functions
```

```{code-cell} ipython3
# plot 
```

# Photodissociation dynamics of diatoms and spectroscopy

+++

Typically, diatomic molecules dissociative behavior upon photoexcitation. Using the DVR approach, we can efficiently model such one-dimensional dissociation quantum dynamics.
The diatomic molecule is initially in its vibronic ground state $|0\rangle$. Assuming a delta-shaped laser pulse in the time-domain and employing the Condon approximation, the ground state wavepacket is promoted on the excited surface, $\hat \mu|0\rangle$. The dipole-operated initial state is not an eigenstate and will therefore undergo dynamics on the excited state surface.\
<img src="diatom_diss.png" style="width:40%">

Non-adiabatic effects are neglected by making the Born-Oppenheimer approximation. This fully decouples the dynamics on the excited state potential energy surface $V_2(R)$ from the ground state potential energy surface $V_1(R)$.
Hence, it is sufficient to solve the nuclear time-dependent Schrödinger equation,
\begin{align}
    i|\dot{\psi}_2(t)\rangle = (\hat T + \hat V_2(R))|\psi_2(t)\rangle,
\end{align}
where $|\psi_2(t)\rangle$ is the nuclear wavefunction evolving on the excited electronic state surface. The initial state is chosen as $|\psi_2(t)\rangle=|0\rangle$, i.e. the vibrational ground state wavefunction in the electronic ground state. Wavefunction and Hamilton operator are represented on a sin-DVR grid.

```{code-cell} ipython3
# Morse potential
def V1(x):
    m1 = 1    # H-mass in AMU
    m2 = 35   # Cl-mass in AMU
    mu = get_mu(m1,m2)*amu2au # reduced mass in atomic units
    R0 = 2.9
    De = 7.41E-19 / E_h
    # we need the force constant k=mu*omega^2
    k = get_k(mu,omega0)
    beta = np.sqrt(k/(2*De))
    
    xtmp = x - R0
    pot= De*(np.exp(-2.0*beta*xtmp)-2.0*np.exp(-beta*xtmp)) + De
    return pot

def V2bound(x):
    m1 = 1    # H-mass in AMU
    m2 = 35   # Cl-mass in AMU
    mu = get_mu(m1,m2)*amu2au # reduced mass in atomic units
    R0 = 3.05
    De = 7.41E-19 / E_h
    # we need the force constant k=mu*omega^2
    k = get_k(mu,omega0)
    beta = np.sqrt(k/(2*De))
    
    xtmp = x - R0
    pot= De*(np.exp(-2.0*beta*xtmp)-2.0*np.exp(-beta*xtmp)) + 2*De
    return pot

def V2diss(x):
    beta = 0.6
    R0 = 3.8
    xtmp = x - R0
    De = 7.41E-19 / E_h
    pot= 0.5*De*(np.exp(-2.0*beta*xtmp)-1.5*np.exp(-0.5*beta*xtmp)) + 1.5*De
    return pot
```

```{code-cell} ipython3
#overview of the potential
xmin = 1.9
xmax = 10
nsteps = 100

tempx = np.linspace(xmin,xmax,nsteps)
#tempV1 = Morse(tempx,De,beta,R0)
tempV1 = V1(tempx)
tempV2 = V2diss(tempx)

mf = 16
fig, ax = plt.subplots(figsize=(8,5))
ax.plot(tempx,tempV1*au2ic)
ax.plot(tempx,tempV2*au2ic)
ax.set_xlabel("x (a$_0$)",fontsize = mf)
ax.set_ylabel("Energy (cm$^{-1}$)",fontsize = mf)
ax.set_ylim(bottom=0,top=100000)
ax.xaxis.set_tick_params(labelsize=mf)
ax.yaxis.set_tick_params(labelsize=mf)
plt.show()
```

First, diagonalize the Hamiltonian for the electronic ground state ($\hat H = \hat T + \hat V_1(R)$) in the DVR basis to obtain the vibronic ground state $|0\rangle$.

```{code-cell} ipython3
N = 200
xmin = 1.9
xmax = 10
x0 = (xmin+xmax)/2 # mid point
L = xmax - xmin

m1 = 1    # H-mass in AMU
m2 = 35   # Cl-mass in AMU
mu = get_mu(m1,m2)*amu2au # reduced mass in atomic units
```

```{code-cell} ipython3
dvr = sinDVR(N,L,x0)
H0 = -dvr.dif2/2/mu + np.diag(V1(dvr.grid))

Eval0, evec0 = eigh(H0,eigvals_only=False)
```

Solving the time-dependent Schrödinger equation $i|\dot \psi(t)\rangle =\hat H|\psi(t)\rangle$ for a given initial state $|\psi(0)\rangle$ consitutes an initial value problem with the formal solution
\begin{align}
     |\psi(t)\rangle = \mathrm{e}^{-i \hat{H} t} |\psi(0)\rangle.
\end{align}
The propagator $\hat U(t) = \mathrm{e}^{-i \hat{H} t}$ reduces to a diagonal matrix in the eigenbasis of $\hat H$,
\begin{align}
    \langle \varphi_n|\hat U(t)|\varphi_m\rangle = \delta_{nm} \mathrm{e}^{-i \omega_n t},
\end{align}
for $\hat H|\varphi_n\rangle = \omega_n|\varphi_n\rangle$. As we have already seen, diagonalization of $\hat H$ in the DVR basis is cheap for a one-dimensional system. To evolve the wavefunction in DVR grid representation, we have to transform the propagator back to the DVR basis using the eigenvectors of $\hat H$ (which we get for free when diagonalizing $\hat H$).

```{code-cell} ipython3
# set up excited state Hamiltonian
H1 = -dvr.dif2/2/mu + np.diag(V2diss(dvr.grid))
Eval1, evec1 = eigh(H1,eigvals_only=False)
```

```{code-cell} ipython3
# propagator
t0 = 0.0 
dt = 1.0
tsteps = 400

propagator = np.diag(np.exp(-1.0j * (Eval1-Eval1[0]) * dt ))
# we remove the lowest eigenvalue from diagonal
# to remove highly oscillatory components

# store intermediate results
psigrid = np.zeros((tsteps+1,N), dtype=complex)
tgrid = np.zeros(tsteps+1)
autocorr = np.zeros(tsteps+1, dtype = complex)

# trafo to grid basis
propagator2 = evec1 @ (propagator @ evec1.T)

# initial state
psi0 = evec0[:,0] # GS wavefunction on V0
#norm = np.dot(psi0,psi0)
#psi0 /= np.sqrt(norm)

psit = psi0.copy()

psigrid[0,:] = psit
tgrid[0] = 0.0

# propagation
for i in range(1,tsteps+1):
    psit = propagator2 @ psit
    psigrid[i,:] = psit
    tgrid[i] = tgrid[i-1] + dt
    autocorr[i-1] = psi0.T.conj() @ psit
```

```{code-cell} ipython3
def plot_psit(t=1):    
    fig, ax = plt.subplots(figsize=(8,5))
    ax.plot(dvr.grid, np.abs(psigrid[int(t)])**2)
    # plot PEC 
    ax.plot(dvr.grid, V2diss(dvr.grid)-0.1)
    ax.set_ylabel(r'$|\Psi|^2$, V [au]',fontsize=mf)
    ax.set_ylim(0,.5)
    ax.set_xlabel('$x$',fontsize=mf)
    #plt.subplots_adjust(hspace = 0.0)
    plt.show()
interact(plot_psit, t=(t0, tsteps, 10))
```

Additionally, we can record the autocorrelation function 
\begin{align}
    a(t) = \langle \psi(0)|\psi(t)\rangle
\end{align}
and use it to obtain the linear absorption spectrum,
\begin{align}
 I(\omega) \propto \frac{1}{\pi}\Re\{ \int_0^\infty a(t) \exp{(i\,\omega \, t)}\; dt \}
\end{align}

```{code-cell} ipython3
plt.plot(tgrid,autocorr.real)
plt.plot(tgrid,autocorr.imag)
plt.xlabel('t (au)',fontsize=mf)
plt.ylabel('$a(t)$ (au)',fontsize=mf)
plt.show()
```

```{code-cell} ipython3
def sigma(omg, a, t, dt):
    s = spi.trapezoid(a*np.exp(1j*omg*t), t, dt)
    return np.real(s)/np.pi

def power_spectrum(auto, t, w0, wf, dw):
    dt = t[1] - t[0]
    w = np.arange(w0, wf, dw)    # Generate frequency grid
    s = np.zeros(len(w), float)  # sigmas
    for l, iw in enumerate(w):
        s[l] = sigma(iw, auto, t, dt)
    return w, s

au2ev = 27.2 
```

```{code-cell} ipython3
# apply damping function g(t)
autocorr2 = autocorr * np.cos(tgrid*np.pi/(2*tgrid[-1]))**2
w, s = power_spectrum(autocorr2, tgrid, -2, 1, 0.001)
```

```{code-cell} ipython3
energies = (w+Eval1[0])*au2ev
plt.plot(energies,s)
plt.xlim(2,15)
plt.xlabel(r'$\omega$ (eV)',fontsize=mf)
plt.ylabel(r'$I(\omega)$ (arb. u.)',fontsize=mf)
plt.tight_layout()
```

## Questions
* What happens to the wavepacket if you propagate for longer times? Can we accurately capture the dissociation limit with the current DVR grid?
* What can you say about the behavior of the autocorrelation function and the spectrum?
* Besides the potential energy curve `V2diss(x)` a bound potential energy function `V2bound(x)` is provided above. Repeat the calculation using `V2bound(x)`. Which differences do you notice?
* Try vibrationally excited states as initial states. What can you observe?

```{code-cell} ipython3
# your code
```

```{code-cell} ipython3

```
