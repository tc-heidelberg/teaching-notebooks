---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

# About jupyter notebooks

The notebooks are organized in cells, where the cells an be of type "markdown" like this one, or of type "code" (there is also a "raw" format that we will not use). If you click into a cell, the cell is selected. A double click into a markdown cell will activate the edit mode where you can edit the text. Typing `esc` will get you back in the so-called command mode, `shift`+`enter` pressed simultaneously will display the formatted text.

(In some browsers some key combinations do not work because the browser interprets them as commands for itself. Click the keyboard icon above to see keyboard shortcuts and execute commands.)

You can also select a cell by clicking on the left of the cells content. By holding down shift while clicking multiple cells you can select multiple cells. 

## Execution of cells

Any cell can be executed: Select the cell (or multiple cells) you want to execute  and press `shift` and `enter` simultaneously.  

For markdown cells this will exit the edit mode and display the formatted text. 

Execution of code cells will run the code inside that cell. Any output the code generates will be displayed directly below the respective code cell.

## Adding cells

To add a cell select an existing cell and change to the command mode (hit esc, alternatively click to the left of an existing cell). Hit 'a' to add a cell <b>a</b>bove the selected cell or 'b' to add a cell <b>b</b>elow the selected cell. 

Alteratively you can use the control elements: for instance 'Insert' -> 'Insert Cell Above'.

## Changing the cell type

Select an existing cell and change to command mode. Hit 'm' to make it a markdown cell. Or hit 'y' to make it a code cell. 

Alteratively you can use the control elements: for instance 'Cell' -> 'Cell type' -> 'Markdown'.

## Read more

https://jupyter-notebook.readthedocs.io/en/latest/user-documentation.html


# Python basics

In the following we will explore some simple Python commands. We will start with introducing the basic data types in Python and perform some basic operations. Let us start with so-called "literals" 

## Literals

Literals are direct representations of data, such as numbers, strings, Booleans etc. Think of it as data that you type literally into the code: The integer number `42`, or the text `"Hello world"`, or the Boolean values `True` or `False`. 

### Integers

Integer numbers, or short 'integers', are numbers without a fractional component like `-6`, `0`, `1` or `789827`. 

Below is a code cell with a number. Select the code cell and press shift+enter simultaneously.

```{code-cell} ipython3
10
```

Nothing spectacular happened apparently. The number is just repeated below the cell. It is the result of the code we executed in the cell (which was just a number we typed in this case so we just got the number back).

Let us perform an operation using two integer literals:

```{code-cell} ipython3
10 + 1 
```

Now below the code cell we obtain the result of the operation. 

One can apply arithmetic operations between two integers:


| Operation | Operator | Result |
| --- | --- | --- |  
| Addition | `+` | integer |
| Subtraction | `-` | integer |
| Multiplication | `*` | integer |
| Floor division | `//` | integer   |
| True division | `/` | float   |
| Power | `**` | integer   |

The usual rules for arithmetic operations apply:

```{code-cell} ipython3
10 + 3 * 4
```

```{code-cell} ipython3
3*4 + 10
```

Powers are expressed with `**`

```{code-cell} ipython3
2**10
```

Division is a bit special. In the table above you see two division operations, `//` and `/`.

`//` is the so-called 'floor division'. It only results in the integer part of a division. The result of the operation `3/2` is '1.5' which is not an integer but  a float (see below) The operation `3/2` is therefore called 'true division'. The result of `3//2` is the integer part of '1.5' which is the largest integer less or equal to the result of the true division. The integer part of `1.5` which is `1`.  

Floor division:

```{code-cell} ipython3
3//2
```

True division:

```{code-cell} ipython3
3/2
```

### Floats

A different type of literals are floating point numbers. Floating point numbers (short "floats") are representations of real numbers. 

Floating point numbers contain a decimal point "`.`".

```{code-cell} ipython3
1.
```

```{code-cell} ipython3
1.0002
```

One can also use scientific notation

```{code-cell} ipython3
1.e-8
```

```{code-cell} ipython3
1.0 + 1.e-12
```

Try the above arithmetic operations for integers with floats. Also try and mix integers and floats and pay attention if the result is integer or float. What is the result of 'floor div' for floats?  

+++

#### Finite precision issues 

Floating point numbers (usually 64 bit numbers) are stored in a similar format as used in scientific notation: sign (1 bit), exponent (11 bits) mantissa (52 bits). 
If you are interested in details see https://en.wikipedia.org/wiki/Double-precision_floating-point_format.


Floating point numbers therefore have a finite precision or 'granularity' of the mantissa of $\approx2\cdot10^{-16}$ (for 64 bit floats). Also the representation of a float is not unique: `1.e-2` is the same as `100.e-4`. To add or subtract floats the exponents must first be made equal which means the mantissa must be zero-padded which in turn leaves fewer bits for its numerical value. This has consequences for arithmetic operations: 


The number $1\cdot 10^0 +1\cdot 10^{-15} = 1\cdot 10^0 +0.000000000000001\cdot 10^{0}$ can be represented:

```{code-cell} ipython3
1 + 1.e-15
```

The number 1+$10^{-16}$ is below the "resolution" of the finite precision and cannot be represented:

```{code-cell} ipython3
1 + 1.e-16
```

We obtain as a result the number `1.0`. Let us test if the result is really `1.0` and the output  not just a printing issue:

```{code-cell} ipython3
1 + 1.e-16 == 1.0
```

Expression above is evaluated from left to right. Here we first add 1 and $10^{-16}$ and then use the comparison operator `==` to compare the result of $1+10^{-16}$ to `1.0`. The result `True` tells us that the value on both sides of `==` is the same.  $1+10^{-16}$ really results in 1. We therefore lose some precision here. 

Adding $10^{-16}$ twice makes no difference because the expression is evaluated from left to right:

```{code-cell} ipython3
1 + 1.e-16 + 1.e-16
```

It does, however, make a difference if we first add the small numbers and then add it to the big number (remember: the expression is evaluated left to right). This is because floats are stored as a mantissa and an exponent. For adding, the exponents need to be the same which is the case for the first two numbers. Adding 1 then does not lead to a loss of precision because both numbers can be represented with loss of precision with the same exponent. 

```{code-cell} ipython3
1.e-16 + 1.e-16 + 1
```

The order in which the operations are executed obviously matters even if it should not. __This is a difference to exact math especially when dealing with numbers on very different scales.__  We will see examples later in  the lecture where this plays a role. 

### Complex numbers:

Complex numbers are represented with a `j` (not `i` as you may be used to from math class).  `j` is conventionally used in engineering and computer science to represent the imaginary unit while `i` is conventionally used in math and physics.

```{code-cell} ipython3
1.0j
```

```{code-cell} ipython3
1.0 + 1.0j
```

Try the arithmetic operations for complex numbers. Try also to mix integers, floats and complex numbers. 

+++

### Strings

Strings are sequences of characters. A string literal is represented as a quoted sequence of characters.

```{code-cell} ipython3
"Hello world"
```

The type of quotes, double quote `"` or single quote `'` is not important. Just be consistent:

```{code-cell} ipython3
'Hello World'
```

Quotes inside quotes are interpreted as part of the string if the inner quotes are different from the outer quotes:

```{code-cell} ipython3
"Hello 'world'"
```

One can also 'escape' the quote by using a backslash `\`:

```{code-cell} ipython3
"Hello \"world\""
```

We can do operations on strings

```{code-cell} ipython3
"Hello world " * 3
```

```{code-cell} ipython3
"I just wrote " + "'hello world'!"
```

Note the nested quotes. 

```{code-cell} ipython3
'I just wrote ' + '"hello world"!'
```

Triple quotes can hold strings that extend over multiple lines

```{code-cell} ipython3
"""Hello
world"""
```

```{code-cell} ipython3
'''Hello
World'''
```

the `\n` encodes a newline character. The `print()` command translates `\n` into a line break.

```{code-cell} ipython3
print('Hello\nworld')
```

Try the `print()` command on numbers. 

```{code-cell} ipython3
print(3/2)
```

### Booleans 

Boolean data types can only be `True` or `False`

```{code-cell} ipython3
True
```

```{code-cell} ipython3
False
```

The result of a comparison is Boolean

Test if two objects have an equal value:

```{code-cell} ipython3
1 == 2
```

```{code-cell} ipython3
1 == 1
```

| Comparison operator | Meaning |
| :-: | :-: |
| == | equal to |
| != | not equal to |
| < | less than |
| > | larger than |
| <= | less than or equal to |
| >= | larger than or equal to |


Try a number of other comparisons. Also try and compare strings, floats and complex numbers. Try what happens of you use the `<`, `>`, `<=`, or `>=`  on complex numbers. Why does it result in an error for  the `<`, `>`, `<=`, or `>=` operators but not for the `==`, and `!=` operators?

```{code-cell} ipython3
1 + 1j == 1.0 + 1j
```

Let us compare strings:

```{code-cell} ipython3
"Hello World" == "Good morning"
```

```{code-cell} ipython3
"Hello World" > "Good morning"
```

The  `<`, `>`, `<=`, and `>=` actually compare the lexical order of two strings. So "a" is considered less than "b". Also it holds "A" < "a" < "aa" < "b".

```{code-cell} ipython3
"a" < "b"
```

```{code-cell} ipython3
"a" > "A"
```

```{code-cell} ipython3
"a" < "aa"
```

### None

A special literal is `None`. It can be used to "nullify" a variable such that it is defined but has only the value `None`. None is also often used as a default value of optional parameters of functions. 

An operation that returns no data really returns `None`.

```{code-cell} ipython3
None
```

# Comments

Comments are pieces of text inside the code section that are ignored during execution (you may not always run Jupyter notebooks with markup cells). Comments are very useful to add extra explanations to the code or to exclude certain sections of code from execution without deleting the source code so one can easily bring back the out-commented commands.

It is good practice to use a lot of comments in the code especially when coding complicated operations. Comments are very helpful for instance if you need to change something in your project years after you first wrote it and do not know the details any more. It will also help co-workers on the same project who may need to change something.

In Python, everything that follows a `#` in a line is a comment

```{code-cell} ipython3
# this is a comment and will not be executed 
# use comments to explain what you are doing:

3*7   # multiply 3 and 7
```

# Variables

Variables are symbolic names for pieces of data. They are really aliases for memory addresses that point to a location in memory where the actual data is stored. 

Variable names can consist of any combinations of letters (A-Z, a-z), numbers (0-9) and underscores `_` but they must not start with a number. 

Python will also let you use a number of Unicode symbols like Greek letters for variable names. This, however, is considered bad practice according to many coding guidelines and should be avoided.  

In Python, variables pop into existence the moment they are assigned.

Assignment is done using the `=` operator (not to be confused with the comparison operator `==`): 

```{code-cell} ipython3
x = 5

# from here on in the variable x exists 
# and points to a memory location holding 
# the integer value 5 
```

We can now access the data using the name of the variable.

```{code-cell} ipython3
x
```

```{code-cell} ipython3
# create another variable 
y = 3
```

We can use variables instead of literals to perform operations, for instance comparisons and arithmetic expressions

```{code-cell} ipython3
x == y
```

```{code-cell} ipython3
x != y
```

```{code-cell} ipython3
x > y
```

```{code-cell} ipython3
x < y
```

Arithmetic operations work just as with literals

```{code-cell} ipython3
x * y
```

```{code-cell} ipython3
x + y
```

Of course, you can assign the result of any operation to a new variable. Everything right of the assigment is evaluated first and the result is stored in the variable on the left. 

```{code-cell} ipython3
z = x + y
```

```{code-cell} ipython3
z
```

```{code-cell} ipython3
# compare x and y and assign the result to b
b = x == y
```

```{code-cell} ipython3
b
```

If many variables should be initialized with the same value one can use a little shurtcut:

```{code-cell} ipython3
# assign all variables, v1, v2, v3, and v4 the same value
v1 = v2 = v3 = v4 = 42
print(v1, v2, v3, v4)
```

(Note that the `print` command can take any number of objects).

Variable names are case sensitive. Trying to access a variable that has not been assigned will result in an error:

```{code-cell} ipython3
# the varible capital Z does not exist

Z
```

__It is good practice to rarely use literals and use variables instead__

Suppose, for instance, you have to write a lab report and you use Python to make a number of plots and set the font size using a literal within the plot command. If you now want to change the font size to a different value you would have to go through all plot commands and change the font size. This can be a lot of work and is also very error prone. Better assign the font size to a variable and call the plot command with the variable. Then you only need to change the variable in one place and all font sizes are changed simultaneously. 

## Lists and tuples

Lists and tuples are ordered collections of things. One can group very different types of data  together.

### Lists

A list is created using square brackets enclosing comma separated elements.

```{code-cell} ipython3
# create a list of literals and assign in to the variable l

l = ["x", 7, 1.2]
```

```{code-cell} ipython3
l
```

```{code-cell} ipython3
# create another list with exactly the same content

k = ["x", 7, 1.2]

# and one more list with the same content but one element is different 
k1 = ["y", 7, 1.2] 
```

We can now compare the lists. The comparison is made element wise and evaluates to <code>True</code> if all elements are equal. 

```{code-cell} ipython3
l == k # all elements are equal
```

```{code-cell} ipython3
l == k1 # one element is different
```

However, the two lists are not the same object. The two variables <code>l</code>  and <code>k</code>  are really different lists that are stored in different locations in memory which just happen to hold the same content. We can check if the two variables point to the same memory locations with the <code>is</code> operator: 

```{code-cell} ipython3
l is k
```

The result `False` tells us `l`  and `k` are __not the same__ objects. The just have equal contents.  If we do an assignment <code>k = l</code> now, the two variables <code>l</code>  and <code>k</code>  are actually pointing to the same memory location. They now represent the same object.

<b>Python by default does not make a copy of the objects upon assignments.</b> It just assigns the memory address such that now the two variables `l` and `k` are two different names for the same list. 

```{code-cell} ipython3
k = l # assign the memory address of l to k
```

```{code-cell} ipython3
l is k
```

`l`  and `k` are now __the same__ objects. 

#### The index operator

We can access the elements of a list  with the index operator <code>[]</code> and an integer index, starting  from 0 for the first element, 1 for the second, ect.:

```{code-cell} ipython3
l[1] # second element of l
```

Accessing a non-existent element results in an error

```{code-cell} ipython3
l[3] # does not exist
```

A very nice feature is that we can access the list in reverse order with negative indices, for instance -1 for  the last element, -2 for the second last, etc. 

```{code-cell} ipython3
l[-1] # last element of l
```

Remember that we said that <code>l</code>  and <code>k</code> represent now the same object? Let's change an element of <code>l</code> using the index operator  <code>[]</code>  and see how this influences <code>k</code>

```{code-cell} ipython3
print("l before changing an element of l:", l)
print("k before changing an element of l:", l)
l[1] = "Hello world" # change an element of l 
print("l after changing an element:", l)
print("k after changing an element:", l)
```

`k` has also changed! This is because `l` and `k` are really the same object due to the assignment `k = l`. The same list just goes by two different names, `l` and `k`.


One can also create lists of variables

```{code-cell} ipython3
a = 1
b = "b"
c = 3.0

l2 = [a,b,c]

print(l2)
```

We can add lists and multiply with integers

```{code-cell} ipython3
l + l2
```

```{code-cell} ipython3
l*3
```

We can even create lists containing lists

```{code-cell} ipython3
l3 = [1,l2, c]
```

```{code-cell} ipython3
l3
```

A list does not store the data of its elements but only the memory addresses of the data. l3 therefore stores the address of l2 and not a copy of l2. l2 itself also only stores addresses of its elements. If we change an element of l2 we also change the contents of l3:

```{code-cell} ipython3
# change an element of l2

l2[1] = "coherent superposition"
```

```{code-cell} ipython3
l3
```

A list may even contain itself (probably not many use cases here, though...).

```{code-cell} ipython3
l4 = [] # an empty list
```

```{code-cell} ipython3
l4
```

```{code-cell} ipython3
l4.append(l4) # append l4 to itself
```

```{code-cell} ipython3
l4
```

### Tuples

Tuples are created like lists but with round brackets. The difference to lists is that they are immutable and only allow read access. Once a tuple is created it cannot be changed. Tuples are very practical to pass collections of data around between different parts of code. Let us create a tuple:

```{code-cell} ipython3
t = ("y", 10, 1+1j)
```

```{code-cell} ipython3
t
```

We have read access with the index operator <code>[]</code> as for lists

```{code-cell} ipython3
t[2]
```

But no write access: trying to change an element results in an error

```{code-cell} ipython3
t[0] = "Hello world"
```

Note that also tuples only store addresses. So we observe the same behavior as for lists:

```{code-cell} ipython3
a = 1
b = "b"
c = 3.0

l2 = [a,b,c]

# put l2 into a tuple 
t2 = (1,l2,3)

print("tuple t2 before changing l2:", t2)

# and now change an element in l2 
l2[1] = "Hello world"

print("tuple t2 after changing l2:", t2)
```

The tuple `t2` has only stored the address of the list `l2`, not the list data itself. If we change the contents of `l2` then this change is reflected in `t2`: the print command recursively loops through all the memory addresses stored in `t2` and prints whatever it finds there. When it finds `l2`, which itself is a collection of addresses, it also loops over `l2` and prints whatever it finds there. 

+++

# Built-in functions

Python provides a number of built-in functions. Functions are pre-defined pieces of code that perform a certain, usually very limited, task. Executing a function is also refered to as 'calling' a function.  

All functions have a function name and may take a number of arguments as input data. Calling a function is performed by stating the function name and providing the arguments in round brackets. If no arguments are given, one uses empty brackets. The arguments contain data that is passed to the function. This can be literals or variables. We have already seen a function call:

### <code>print()</code>

The print command can be used to print something to the output. <code>print()</code> will try to convert any number of objects passed to it to a string representation and output it to the standard output channel.

```{code-cell} ipython3
print("Hello world")
```

```{code-cell} ipython3
print(3, True, "some string")
```

### <code>str()</code>

What <code>print()</code> internally calls is another function <code>str()</code> that returns an objects string representation. We will see later in the course how this works in more detail. We can use the <code>str()</code> function to obtain a string representation of any object. 

```{code-cell} ipython3
str(1+3j)
```

```{code-cell} ipython3
str(l)
```

Note the quotes around the output, indicating that the output is a string. 

+++

### <code>help()</code>

Python has a built-in interactive help. Just call <code>help()</code> with no arguments and type a command you need help on into the search field. To exit the help type "quit" in the search field

```{code-cell} ipython3
help()
```

One can get help for a command directly by calling help with the command. 

```{code-cell} ipython3
help(print)
```

### <code>type()</code>

Python allows you to assign anything to a variable. Therefore the type of a variable can change depending of what has been assigned to it. One can check the type with the <code>type()</code> function.

```{code-cell} ipython3
type(l)
```

```{code-cell} ipython3
type(3)
```

Types of variables change depending on the last assigment:

```{code-cell} ipython3
# type of x is tuple
x = (1, 3, 5)
print("type of x is:", type(x))

# type of x is float
x = 1.0
print("type of x is:", type(x))

# type of x is string
x = "Hello world"
print("type of x is:", type(x))
```

### <code>id()</code>

Every object in Python has a unique ID. You can access the ID with the function <code>id()</code>. 

```{code-cell} ipython3
id(x)
```

```{code-cell} ipython3
# the two lists l and k which are the same object
id(l)
```

```{code-cell} ipython3
id(k)
```

Note that the IDs of `k` and `l` are identical. `k` and `l` are really the same object. 

+++

# Self defined functions

Besides the built-in functions one can define additional functions.

Functions can be defined using the <code>def</code> keyword followed by the function name and a list of arguments in round brackets - for no arguments just empty round brackets.  Finally a <code>:</code> concludes the function declaration. 

The function body starts with an indented block of code after the declaration. This indentation is actually a syntax element. It means that all following lines with the same indentation are part of the function body. The function body ends where the indentation ends. The function is only executed when called and not when defined. 

The <code>return</code> statement specifies what the function passes back to the caller. A function without a `return` statement returns <code>None</code> on exit. 
The function will terminate once it reaches a `return` statement. Any code in the function body that follows `return` will never be executed. 

For instance a function that calculates the power $x^n$ would be defined like this:

```{code-cell} ipython3
def power(x, n):
    # function body

    # nothing after 'return' is executed in the function body
    return x**n

print("Hello world") 
# The line above is not indented and therefore not part of the function body. 

```

Note that the function has not been executed. Only the `print("Hello world")` has been axecuted as it is not part of the function. 

The function `power(x, n)` has now been defined which means it can be used from now on. 

Try putting the line <code>print("Hello world")</code> into the function body before and after the return statement and observe the differences when executing the cell above followed by the cell below.

Once defined we can use the function:

```{code-cell} ipython3
power(2,10) # 2^10
```

Of course we can assign the output of `power` to a variable:

```{code-cell} ipython3
two_power_10 = power(2,10)
print(two_power_10)
```

However, there is no help for our function yet.

```{code-cell} ipython3
help(power)
```

A help text can be implemented by just adding a string right below the function definition line. If a longer text is required one can use triple quotes to use multiple lines.   

```{code-cell} ipython3
def power(x, n):
    "Calculates x^n"
    return x**n
```

```{code-cell} ipython3
help(power)
```

# Control structures

### Case selection (branching)

Often different operations need to performed depending in on the input data. This can be realized with an 'if' or 'if-elif-else' statement. 

The syntax is (note the <code>:</code> after the condition)

>~~~~
> if condition1:
>    indented block
> elif condition2:
>    indented block
> else:
>    indented block 
>    (if none of the above applies) 

In the example above the first block that matches the condition will be executed. All other blocks are ignored. 


Play with the variables `x` and `a` below: change their values and see what conditions evaluate to `True`.

```{code-cell} ipython3
x = 3
a = 1

# simple if

if type(a) is int:
    # if condition "type(a) is int" evaluates
    # to True do this, otherwise ignore
    print("a is int")


# if - else

if a == 0:
    # if condition "a == 0" evaluates to True do this
    print("a is zero")
else:
    # otherwise do this 
    print("a is not zero")


# if - elif - else
# One can add as many elif as needed.
# Only the block followed by the first
# condition that evaluates to True will be
# executed
 
if x < a:
    # if condition "x < a" evaluates to True do this
    print("x is smaller than",a)
elif x == a:
    # Otherwise if the second condition ("x == a") 
    # evaluates to True do this
    print("x is equal to",a)   
else:
    # If all conditions above evaluate to False do this
    print("x is greater than",a)
```

One can combine several conditions with the `and` and `or` operators. Negation of a condition can be done with the `not` operator. 

```{code-cell} ipython3

a = 0
b = 10
x = 4

if x > a and x < b:
    print("x is larger than a and x is smaller than b")

if x > a and not x < 3:
     print("x is larger than a and x is not smaller than 3")

if x < b or x < a:
     print("x is smaller than b or x is smaller than a")
```

Note that the `or` operation is _inclusive_ in the sense of 'condition A or condition B or both must match'. We can build a matrix or outcomes depending on the results of the conditions:

```{code-cell} ipython3
print( "condA    condB   result")
print( "------------------------")
print( "True  or True:  ", True or True)
print( "False or True:  ", False or True)
print( "True  or False: ", True or False)
print( "False or False: ", False or False) 
# this is text --^                 ^
# this evaluates before printing --^ 
```

Same for `and`:

```{code-cell} ipython3
print( "condA     condB   result")
print( "-------------------------")
print( "True  and True:  ", True and True)
print( "False and True:  ", False and True)
print( "True  and False: ", True and False)
print( "False and False: ", False and False)
```

### Loops

We can loop over so-called itarables. We have already seen two types of iterables: lists and tuples. We will see more later in the course. The keyword <code>for</code> initializes a loop. Note again the <code>:</code> and indentation:

>~~~
>for element in iterable:
>    do stuff with element in indented block



```{code-cell} ipython3
# create a tuple
# tuples are iterable
t3 = (1,3,2,4,5)

# loop over elements in tuple t3
for elem in t3:
    print(elem)
```

We do not need to define a tuple or list every time we want to use a loop. The `range` command is often used to create a sequence of integers that serve as a loop counter: 

```{code-cell} ipython3
for i in range(5):
    print(i)
```

`range(5)` simply returns the first 5 integers starting from 0. One can also define a starting integer, a last integer (which is not included) and, optionally, a step size: 

```{code-cell} ipython3
# i will take all values from 10 to 18 (but excluding 18) in steps of 2.  
for i in range(10,18,2):
    print(i)
    
# see what happens if you set the step size to 1.
```

# Import

Often one needs additional modules and packages which contain specialized code and functionality that is not loaded by default. 

In Python a module is code that is contained in a single source file, while a package is a collection of source modules that are bundled together so that they can be loaded with a single command. 

The Python base installation comes with a number of modules and packages which are just not loaded on startup, other modules or packages are provided by third parties and need to be installed before they can be used.  

Those modules or packages can be graphics libraries, numerical routines, databases, etc. They can be loaded with the <code>import</code> statement. There are several ways to do this as we will see later, the most simple one is by using the <code>import</code> statement followed by the package or module name.

```{code-cell} ipython3
# load the numpy package 
# (numerical python) which should be 
# installed by default if you installed Anaconda 
# if numpy is not installed please install it with the package manager
import numpy 
```

After we have loaded the `numpy` package we can use it.

We can look at the contents of the package with the <code>dir()</code>function

```{code-cell} ipython3
dir(numpy)
```

This also applies for all other objects like lists. Note that this does not print the contents of the list but functions and variables which are members of the list type (we will see what a member is later). 

```{code-cell} ipython3
dir(l)
```

Any objects defined in the package are accessible by the package name followed by a dot and the object name. 

For instance the function <code>zeros(n)</code> provided by numpy returns an array object filled with zeros of length<code>n</code>.

```{code-cell} ipython3
a = numpy.zeros(3)
```

```{code-cell} ipython3
a
```

An array is similar to a list but with some important differences:
* all elements in an array are of the same type (for instance all floats)
* the elements are stored consecutively in memory. This has performance reasons but will also allow some other nice features. 
* once created, the number of elements in an array cannot be changed. 
* arithmic operations (`+`,`-`,`*`, etc.) and comparisons (`==`,`<`, etc) are performed element wise and the result is an array and not a single value. 

```{code-cell} ipython3
b = numpy.ones(3)
print("add:      a + b:  ", a + b)
print("compare:  a == b: ", a == b)
print("multiply: b*4:    ", b*4)
print("power:    10**b:  ", 10**b)
```

We will later see much more of Python. But this should be enough to get you started. 

```{code-cell} ipython3

```
