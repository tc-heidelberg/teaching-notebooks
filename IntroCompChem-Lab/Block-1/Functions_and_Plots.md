---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

Einführung in die Computergestützte Chemie, WiSe2024/25

```{code-cell} ipython3
# import the libraries that are needed

from numpy import *                # import everything from numpy
import matplotlib.pyplot as plt    # import pyplot from matplotlib and name it plt
```

```{code-cell} ipython3
# this is a function that, when called, will return the area and circumference of a circle
# it requires a vector (1-D array) of radii as input variable and will return two 1-D arrays
# "zeros" and "pi" are imported from numpy 

def area_circ(r):
    # create two arrays filled with zeros
    a = zeros(len(r))
    c = zeros(len(r))
    
    # loop over the input vector.
    # enumerate returns an element-index and the element
    # itself  
    for i,myr in enumerate(r):
        a[i] = pi * myr ** 2
        c[i] = 2.0 * pi * myr

    # return a and c to caller
    return a, c 
```

```{code-cell} ipython3
# this generates a set of values for the radius on a grid
# linspace and double are imported from numpy

radius = linspace(0,10,20, dtype=double)

# this computes the corresponding area and circumference for each radius
area, circ = area_circ(radius)
```

```{code-cell} ipython3
#this plots the result

mf=18 # font size

# create a figure and axis object
fig, ax = plt.subplots(figsize=(8,5))

# plot the data
ax.plot(radius,area,marker='x',label='area')
ax.plot(radius,circ,marker='o',label='circumference')

# set some properties of the axis ticks
ax.xaxis.set_tick_params(labelsize=mf)
ax.yaxis.set_tick_params(labelsize=mf)
ax.set_xlabel('radius',fontsize=mf)
ax.set_ylabel('area/circumference',fontsize=mf)

# add a legend
legend = ax.legend(loc='best', shadow=False,fontsize=mf,borderpad = 0.1, labelspacing = 0, handlelength = 0.8)

# display the plot
# always use plt.show() to prompt output of the figure
plt.show()
```

## Vectorization

```{code-cell} ipython3
def area_circ2(r):
    # create two arrays filled with zeros  
    a = pi * r ** 2
    c = 2.0 * pi * r

    # return a and c to caller
    return a, c 
```

```{code-cell} ipython3
radius = linspace(0,10,20, dtype=double)
```

Since <code>radius</code> is a 2D numpy array arithmetic operations are
performed element wise. In <code>area_circ2</code> the square and multiplication
operations are hence performed on all elements of <code>radius</code> and the
result is also an array holding the function values in the respective index
<code>[i]</code>. So we can evaluate all elements with one single call to the
function. This is possible because all array elements have the same data type.
This is also referred to as <i>vectorization</i>.

```{code-cell} ipython3
# this computes the corresponding area and circumference for each radius through a single call of area_circ
# this is more efficient than explicit looping (as in area_circ)
area, circ = area_circ2(radius)
print(area)
print(circ)
```

# Plotting functions

```{code-cell} ipython3
# need additional modules
from matplotlib import cm  # color map
```

```{code-cell} ipython3
# some functions to plot
def my_func1(x):
    return x**2+5

def my_func2(x,y):
    z = sin(sqrt(5)+x)*y
    return z
```

## Plot 1D function

```{code-cell} ipython3
# grid on which to evaluate 1d function
xgrid = arange(-2,2.1,.1)
f1 = my_func1(xgrid)

# plot the result
mf=18 # font size

# create a figure and axis object
fig, ax = plt.subplots(figsize=(8,5))

# plot the data
ax.plot(xgrid,f1)

# set some properties of the axis ticks
ax.xaxis.set_tick_params(labelsize=mf)
ax.yaxis.set_tick_params(labelsize=mf)
ax.set_xlabel('y',fontsize=mf)
ax.set_ylabel('x',fontsize=mf)

# display the plot
plt.show()
```

## Plot 2D function

+++

When we want to plot 2D functions $f(x,y)$ we typically set up a grid for all
$x$-values and for all $y$-values, say, 10 points with indices $i$ and  $j$ for
each variable. We then have to loop through all $10\times 10 = 100$ combinations
of the different $x$- and $y$-values to evaluate the function at the respective
$x$ and $y$ values. 

What <code>X, Y = meshgrid(xgrid, ygrid) </code> returns are all these
combinations in two arrays such that we can access them as
$x_i$=<code>X[j,i]</code> and $y_j$=<code>Y[j,i]</code>.  (See the help of
<code>meshgrid()</code>, especially the <code>indexing</code> parameter for some
subtleties about $i$ and  $j$ )


```{code-cell} ipython3
# 2d grid (meshgrid) on which to evaluate 2d function
xgrid = arange(-2,2.1,.1)
ygrid = arange(-2,2.1,.1)
X, Y = meshgrid(xgrid, ygrid)

f2 = my_func2(X,Y)

# plot the result
mf = 14
# create 3d axes object
fig = plt.figure(figsize=(7,7))
ax = plt.axes(projection='3d')

# cm: color map 
ax.plot_surface(X, Y, f2, cmap = cm.viridis, rstride=1, cstride=1, edgecolor='None')

ax.tick_params(axis='x', labelsize=mf)  # Set x-axis tick label size
ax.tick_params(axis='y', labelsize=mf)  # Set y-axis tick label size
ax.tick_params(axis='z', labelsize=mf)  # Set z-axis tick label size

ax.set_xlabel('x value', fontsize=mf, labelpad=10)
ax.set_ylabel('y value', fontsize=mf, labelpad=10)
ax.set_zlabel('z value', fontsize=mf, labelpad=2)

#ax.view_init(elev=30,azim=30)
plt.tight_layout()
plt.show()
```

# The square

You have several square samples of different sizes and would like to know the
area and the perimeter of the samples.

Generate a python script (in particular, jupyter notebook) to solve your problem
and visualize your result. Use the above example as a basis. Put in comments as
appropriate.

```{code-cell} ipython3
# your code
```
